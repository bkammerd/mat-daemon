#!/usr/bin/env bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${script_dir}

if [ -f build.options ]; then
    source ${script_dir}/build.options
fi

# Make sure we have defaults that work.
DEF_CLANG="clang"
DEF_LLVM_STRIP="llvm-strip"
DEF_BCC_SRC_PATH="/home/bkammerd/memory-ef/env/src/bcc"
DEF_LIBBPF_SRC_PATH="${DEF_BCC_SRC_PATH}/src/cc/libbpf/src"
DEF_BPFTOOL="$(realpath ${script_dir}/bpftool)"
DEF_BKMALLOC_INC_PATH="/home/bkammerd/memory-ef/env/include"

CLANG=${CLANG:-${DEF_CLANG}}
LLVM_STRIP=${LLVM_STRIP:-${DEF_LLVM_STRIP}}
BCC_SRC_PATH=${BCC_SRC_PATH:-${DEF_BCC_SRC_PATH}}
LIBBPF_SRC_PATH=${LIBBPF_SRC_PATH:-${BCC_SRC_PATH}/src/cc/libbpf/src}
BPFTOOL=${BPFTOOL:-${DEF_BPFTOOL}}
BKMALLOC_INC_PATH=${BKMALLOC_INC_PATH:-${DEF_BKMALLOC_INC_PATH}}

ARCH=$(uname -m | sed 's/x86_64/x86/' | sed 's/aarch64/arm64/' | sed 's/ppc64le/powerpc/')
# OPT="-O0"
OPT="-O3 -march=native -mtune=native"
LTO="-flto"
PREPROCESSOR="-D LINUX_VERSION=$(uname -r | sed 's/[^0-9]/ /g' | awk -F ' ' '{ print $1" "$2" "$3; }' | perl -e 'my $input = <>; my @mysplit = split / /,$input; print ((@mysplit[0]<<16)+(@mysplit[1]<<8)+@mysplit[2]);')"

if ! [ -z "${LTO}" ]; then
    # can we actually use LTO?
    echo 'int main(){}' > /tmp/llvm_lto_test_$$.c
    if ! ${CLANG} -flto /tmp/llvm_lto_test_$$.c -o /tmp/llvm_test_$$ >/dev/null 2>&1; then
        LTO=""
    fi
fi

CFLAGS="${OPT} ${LTO} ${PREPROCESSOR} -Wall -g -fno-omit-frame-pointer -Wno-deprecated-declarations -Isrc/bpf"
LDFLAGS="${LTO} -fno-omit-frame-pointer -rdynamic -lelf -lz -lm -lpthread -ldl -lpfm -lnuma -lrt"
BPF_CFLAGS="-g -O3 -Wall ${PREPROCESSOR}"
POL_CFLAGS="-g ${OPT} -Wall"

S=$(realpath src)
B=$(realpath build)
rm    -rf ${B}
mkdir -p  ${B}
BBPF=$(realpath build/libbpf)
mkdir -p ${BBPF}
BOBJ=$(realpath build/obj)
mkdir -p ${BOBJ}
BOBP=$(realpath build/obj/bpf)
mkdir -p ${BOBP}
BOMD=$(realpath build/obj/md)
mkdir -p ${BOMD}
BPOL=$(realpath build/policies)
mkdir -p ${BPOL}
BSKL=$(realpath build/skel)
mkdir -p ${BSKL}
BBIN=$(realpath build/bin)
mkdir -p ${BBIN}

echo "Building libbpf.."
cd ${LIBBPF_SRC_PATH}
make -j BUILD_STATIC_ONLY=1 \
        OBJDIR=${BOBJ}      \
        DESTDIR=${BBPF}     \
        INCLUDEDIR=         \
        LIBDIR=             \
        UAPIDIR=            \
        install || exit $?
cd ${script_dir}

echo "Generating vmlinux.h.."
mkdir -p src/x86
${BPFTOOL} btf dump file /sys/kernel/btf/vmlinux format c > src/x86/vmlinux.h

echo "Generating BPF skeleton.."
echo "  CC       mat_daemon.bpf.o"
${CLANG} -o ${BOBP}/mat_daemon.bpf.o -c                                                     \
            ${BPF_CFLAGS} -Wno-unknown-attributes -Wno-unused-variable -Wno-unused-function \
            -target bpf                                                                     \
            -D__TARGET_ARCH_${ARCH}                                                         \
            -I${B} -I${BBPF} -I${S}/${ARCH} -I${S}                                          \
            src/bpf/mat_daemon.bpf.c || exit $?
${LLVM_STRIP} -g ${BOBP}/mat_daemon.bpf.o || exit $?
${BPFTOOL} gen skeleton ${BOBP}/mat_daemon.bpf.o > ${BSKL}/mat_daemon.skel.h || exit $?

echo "Compiling mat-daemon.."

pids=()
GLOBIGNORE="src/bpf/*.bpf.c"
s1=(src/*.c)
s2=(src/bpf/*.c)
source_files="${s1[@]} ${s2[@]}"
unset GLOBIGNORE
for f in ${source_files}; do
    echo "  CC       $(basename ${f} ".c").o"
    ${CLANG} -o ${BOMD}/$(basename ${f} ".c").o \
        -c ${CFLAGS}                            \
        -I${BBPF} -I${BSKL} -I${S}/${ARCH}      \
        -Wno-unknown-attributes                 \
        ${f} &
    pids+=($!)
done

for p in ${pids[@]}; do
    wait $p || exit $?
done

echo "Linking mat-daemon.."
echo "  LD       mat-daemon"

s1=(${BOMD}/*.o)
obj_files="${s1[@]} ${BOBJ}/libbpf.a"

${CLANG} -o ${BBIN}/mat-daemon ${LDFLAGS} ${obj_files} || exit $?


echo "Building policies.."

pids=()
for f in $(find policies -name "*.c"); do
    dir=$(dirname ${f})
    out_dir=${BPOL}/${dir#*/}
    mkdir -p ${out_dir}
    echo "  CC    ${dir}/$(basename ${f} ".c").o"
    ${CLANG} -o ${out_dir}/$(basename ${f} ".c").so \
        -shared -fPIC -rdynamic ${POL_CFLAGS}       \
        -Isrc                                       \
        ${f} &
    pids+=($!)
done

for p in ${pids[@]}; do
    wait $p || exit $?
done


export BKMALLOC_INC_PATH
cd bkhooks
echo "Compiling bkmalloc hooks.."
./build.sh || exit $?
cd ..

echo "Done."
