#include <md_policy.h>
#define MDADV_SENDER
#define MDADV_IMPL
#include <mdadv.h>

#include <regex.h>

use_tree(pid_t, int)

static float             purge_percent;
static char             *high_prio_pattern;
static regex_t           high_prio_regex;
static tree(pid_t, int)  high_prio_procs;

static int compile_regex(struct scfg *cfg, const char *val) {
    int  err;
    char buff[1024];

    err = regcomp(&high_prio_regex, val, REG_EXTENDED | REG_NOSUB);
    if (err) {
        buff[0] = 0;
        strcat(buff, "failed to compile regex: ");
        regerror(err, &high_prio_regex, buff + strlen(buff), sizeof(buff) - strlen(buff) - 1);
        scfg_validate_set_err(cfg, buff);
        return 0;
    }

    return 1;
}

void md_policy_add_options(void) {
    add_policy_option("pack-purge-percent",
        OPT_FLOAT,
        "For packing policy 'hybrid-hotset-purge-lockout', the percentage of data in the upper tier to purge.",
        &purge_percent);
        default_float("pack-purge-percent", 10.0);
        validate_float("pack-purge-percent", is_float_percentage);

    add_policy_option("pack-high-priority-regex",
        OPT_STRING,
        "For packing policy 'hybrid-hotset-purge-lockout', a regex pattern that when matched against a process name, will consider it to be high priority, causing objects from other tasks to be locked out of the upper tier.",
        &high_prio_pattern);
        validate_string("pack-high-priority-regex", compile_regex);
}

static int high_prio_pid(pid_t pid) {
    tree_it(pid_t, int) lookup;

    lookup = tree_lookup(high_prio_procs, pid);

    return tree_it_good(lookup) && tree_it_val(lookup);
}

void md_policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote) {
    u64           upper_tier_size;
    u64           purge_size;
    u64           size;
    int           has_high_prio;
    mdadv_sender *q;
    mdadv         advice_msg;
    md_proc      *proc;
    md_object    *obj;
    u64           obj_size;

    upper_tier_size = GB(1) * config.tier_1_capacity;
    purge_size      = (purge_percent / 100.0) * upper_tier_size;
    size            = 0;
    has_high_prio   = 0;

    if (high_prio_procs != NULL) {
        tree_free(high_prio_procs);
    }
    high_prio_procs = tree_make(pid_t, int);

    PROCESS_FOR(proc, {
        if (regexec(&high_prio_regex, proc->cmd_buff, 0, NULL, 0) == 0) {
            tree_insert(high_prio_procs, proc->pid, 1);
            has_high_prio = 1;
        }
    });

    if (config.advise_initial_placement) {
        PROCESS_FOR(proc, {
            lock_process(proc);

            q = &proc->advice_queue;

            if (q->qd <= 0) {
                mdadv_start_sender(q, proc->pid);
            }

            unlock_process(proc);

            if (q->qd <= 0) { continue; }

            advice_msg.inst_addr = MDADV_LOCKOUT;
            advice_msg.advice    = (has_high_prio && !high_prio_pid(proc->pid))
                                    ? MDADV_LOCKOUT_SET
                                    : MDADV_LOCKOUT_CLEAR;

            mdadv_send(q, &advice_msg);
        });
    }

    array_traverse(*ranked, obj) {
        obj_size = get_object_capacity_estimate(obj);

        if ((upper_tier_size - purge_size) >= (size + obj_size)
        &&  (!has_high_prio || high_prio_pid(obj->pid))) {

            array_push(*out_promote, obj);
            size = size + obj_size;
        } else {

            array_push(*out_demote, obj);
        }
    }
}
