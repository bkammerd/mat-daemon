#include <md_policy.h>

#include <regex.h>

use_tree(pid_t, int)

static float             purge_percent;
static char             *nice_pattern;
static regex_t           nice_regex;
static tree(pid_t, int)  nice_procs;

static int compile_regex(struct scfg *cfg, const char *val) {
    int  err;
    char buff[1024];

    err = regcomp(&nice_regex, val, REG_EXTENDED | REG_NOSUB);
    if (err) {
        buff[0] = 0;
        strcat(buff, "failed to compile regex: ");
        regerror(err, &nice_regex, buff + strlen(buff), sizeof(buff) - strlen(buff) - 1);
        scfg_validate_set_err(cfg, buff);
        return 0;
    }

    return 1;
}

void md_policy_add_options(void) {
    add_policy_option("pack-purge-percent",
        OPT_FLOAT,
        "For packing policy 'hybrid-hotset-purge-nice', the percentage of data in the upper tier to purge.",
        &purge_percent);
        default_float("pack-purge-percent", 10.0);
        validate_float("pack-purge-percent", is_float_percentage);

    add_policy_option("pack-nice-regex",
        OPT_STRING,
        "For packing policy 'hybrid-hotset-purge-nice', a regex pattern that when matched against a process name, will consider it as 'nice', causing its objects to be less prioritized for the upper tier.",
        &nice_pattern);
        validate_string("pack-nice-regex", compile_regex);
}

static int nice_pid(pid_t pid) {
    tree_it(pid_t, int) lookup;

    lookup = tree_lookup(nice_procs, pid);

    return tree_it_good(lookup) && tree_it_val(lookup);
}

void md_policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote) {
    u64         upper_tier_size;
    u64         purge_size;
    u64         size;
    u64         upper_obj_size, lower_obj_size;
    md_proc    *proc;
    array_t     ranked_notnice;
    array_t     ranked_nice;
    md_object **objp;
    md_object  *obj;
    u64         obj_size;

    upper_tier_size = GB(1) * config.tier_1_capacity;
    purge_size      = (purge_percent / 100.0) * upper_tier_size;
    size            = 0;

    upper_obj_size  = 0;
    lower_obj_size  = 0;

    if (nice_procs != NULL) {
        tree_free(nice_procs);
    }
    nice_procs = tree_make(pid_t, int);

    PROCESS_FOR(proc, {
        if (regexec(&nice_regex, proc->cmd_buff, 0, NULL, 0) == 0) {
            tree_insert(nice_procs, proc->pid, 1);
        }
    });

    ranked_notnice = array_make(md_object*);
    ranked_nice    = array_make(md_object*);

    array_traverse(*ranked, obj) {
        if (nice_pid(obj->pid)) {
            array_push(ranked_nice, obj);
        } else {
            array_push(ranked_notnice, obj);
        }
    }

    array_traverse(ranked_notnice, objp) {
        obj = *objp;

        obj_size = get_object_capacity_estimate(obj);

        if ((upper_tier_size - purge_size) >= (size + obj_size)) {
            array_push(*out_promote, obj);
            size = size + obj_size;
            upper_obj_size += obj_size;
        } else {
            array_push(*out_demote, obj);
            lower_obj_size += obj_size;
        }
    }
    array_traverse(ranked_nice, objp) {
        obj = *objp;

        obj_size = get_object_capacity_estimate(obj);

        if ((upper_tier_size - purge_size) >= (size + obj_size)) {
            array_push(*out_promote, obj);
            size = size + obj_size;
            upper_obj_size += obj_size;
        } else {
            array_push(*out_demote, obj);
            lower_obj_size += obj_size;
        }
    }

    array_free(ranked_nice);
    array_free(ranked_notnice);

    if (config.print_migrate_stats) {
        INFO("Expected sizes: upper: %12.2f MB lower: %12.2f MB\n",
             ((float)upper_obj_size)/(1024.0*1024.0),
             ((float)lower_obj_size)/(1024.0*1024.0));
    }
}
