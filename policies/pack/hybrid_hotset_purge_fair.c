#include <md_policy.h>

static float purge_percent;

void md_policy_add_options(void) {
    add_policy_option("pack-purge-percent",
        OPT_FLOAT,
        "For packing policy 'hybrid-hotset-purge-fair', the percentage of data in the upper tier to purge.",
        &purge_percent);
        default_float("pack-purge-percent", 10.0);
        validate_float("pack-purge-percent", is_float_percentage);
}

typedef struct {
    u64     total_size;
    u64     promoted_size;
    pid_t   pid;
    array_t objects;
} Proc_Stats;

static void sort_priority_list(array_t *prio) {
    u32         i;
    Proc_Stats  x;
    double      x_used;
    u32         j;
    Proc_Stats *y;
    double      y_used;

    i = 1;

    while (i < array_len(*prio)) {
        x = *(Proc_Stats*)array_item(*prio, i);
        x_used = x.total_size == 0
                    ? 1.0
                    : (double)x.promoted_size / (double)x.total_size;
        j = i - 1;
        while (j >= 0) {
            y      = array_item(*prio, i);
            y_used = y->total_size == 0
                        ? 1.0
                        : (double)y->promoted_size / (double)y->total_size;

            if (y_used <= x_used) { break; }

            memcpy(array_item(*prio, j + 1), y, sizeof(*y));
            j -= 1;
        }
        memcpy(array_item(*prio, j + 1), &x, sizeof(x));
        i += 1;
    }
}

void md_policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote) {
    u64                          upper_tier_size;
    u64                          purge_size;
    u64                          upper_obj_size, lower_obj_size;
    u64                          avail;
    Proc_Stats                   stats;
    md_proc                     *proc;
    Proc_Stats                  *statsp;
    md_object                   *obj;
    u64                          obj_size;
    array_t                      prio;
    md_object                  **obj_it;
    u32                          idx;

    upper_tier_size = GB(1) * config.tier_1_capacity;
    purge_size      = (purge_percent / 100.0) * upper_tier_size;
    upper_obj_size  = 0;
    lower_obj_size  = 0;
    avail           = upper_tier_size - purge_size;

    prio = array_make(Proc_Stats);

    memset(&stats, 0, sizeof(stats));
    PROCESS_FOR(proc, {
        stats.pid = proc->pid;
        array_push(prio, stats);
    });

    array_traverse(prio, statsp) {
        statsp->objects = array_make(md_object*);
        array_traverse(*ranked, obj) {
            if (obj->pid != statsp->pid) { continue; }

            array_push(statsp->objects, obj);

            obj_size = get_object_capacity_estimate(obj);
            statsp->total_size += obj_size;
        }
    }


    while (avail > PAGE_SIZE && array_len(prio) > 0) {
        sort_priority_list(&prio);

        array_traverse(prio, statsp) {
            idx = 0;
            array_traverse(statsp->objects, obj_it) {
                obj      = *obj_it;
                obj_size = get_object_capacity_estimate(obj);

                if (obj_size <= avail) {
                    array_push(*out_promote, obj);

                    avail          -= obj_size;
                    upper_obj_size += obj_size;

                    array_delete(statsp->objects, idx);
                    statsp->promoted_size += obj_size;

                    goto CONTINUE;
                }

                idx += 1;
            }
        }

        /* No suitable object found... */
        break;

CONTINUE:;
    }

    array_traverse(prio, statsp) {
        array_traverse(statsp->objects, obj_it) {
            obj      = *obj_it;
            obj_size = get_object_capacity_estimate(obj);

            lower_obj_size += obj_size;
            array_push(*out_demote, obj);
        }
        array_free(statsp->objects);
    }

    if (config.print_migrate_stats) {
        INFO("Expected sizes: upper: %12.2f MB lower: %12.2f MB\n",
             ((float)upper_obj_size)/(1024.0*1024.0),
             ((float)lower_obj_size)/(1024.0*1024.0));
    }

    array_free(prio);
}
