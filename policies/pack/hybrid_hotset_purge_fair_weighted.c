#include <md_policy.h>

#include <regex.h>

use_tree(pid_t, int)

static float             purge_percent;
static char             *nice_pattern;
static regex_t           nice_regex;
static tree(pid_t, int)  nice_procs;
static float             nice_coeff;

static int compile_regex(struct scfg *cfg, const char *val) {
    int   err;
    char *regex_start;
    char  buff[1024];

    nice_coeff = strtof(val, &regex_start);
    if (regex_start == val) {
        snprintf(buff, sizeof(buff), "failed to parse float from %s", val);
        scfg_validate_set_err(cfg, buff);
        return 0;
    }

    while (*regex_start == ' ') { regex_start += 1; }

    err = regcomp(&nice_regex, regex_start, REG_EXTENDED | REG_NOSUB);
    if (err) {
        buff[0] = 0;
        strcat(buff, "failed to compile regex: ");
        regerror(err, &nice_regex, buff + strlen(buff), sizeof(buff) - strlen(buff) - 1);
        scfg_validate_set_err(cfg, buff);
        return 0;
    }

    return 1;
}

void md_policy_add_options(void) {
    add_policy_option("pack-purge-percent",
        OPT_FLOAT,
        "For packing policy 'hybrid-hotset-purge-fair', the percentage of data in the upper tier to purge.",
        &purge_percent);
        default_float("pack-purge-percent", 10.0);
        validate_float("pack-purge-percent", is_float_percentage);
    add_policy_option("fair-weight-regex",
        OPT_STRING,
        "For packing policy 'hybrid-hotset-purge-fair-weighted', a float followed by a regex pattern that when matched against a process name, will use the float value as a coefficient for that process's packed object size.",
        &nice_pattern);
        validate_string("fair-weight-regex", compile_regex);
}

typedef struct {
    u64     promoted_size;
    u64     true_promoted_size;
    u64     demoted_size;
    pid_t   pid;
    array_t objects;
} Proc_Stats;

static int prio_cmp(const void *_a, const void *_b) {
    const Proc_Stats *a = _a;
    const Proc_Stats *b = _b;

    if (a->promoted_size == b->promoted_size) { return 0;  }
    if (a->promoted_size  < b->promoted_size) { return -1; }
    return 1;
}

static int nice_pid(pid_t pid) {
    tree_it(pid_t, int) lookup;

    lookup = tree_lookup(nice_procs, pid);

    return tree_it_good(lookup) && tree_it_val(lookup);
}

void md_policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote) {
    u64                          upper_tier_size;
    u64                          purge_size;
    u64                          upper_obj_size, lower_obj_size;
    u64                          avail;
    Proc_Stats                   stats;
    md_proc                     *proc;
    Proc_Stats                  *statsp;
    md_object                   *obj;
    u64                          obj_size;
    array_t                      prio;
    md_object                  **obj_it;
    u32                          idx;


    if (nice_procs != NULL) {
        tree_free(nice_procs);
    }
    nice_procs = tree_make(pid_t, int);

    PROCESS_FOR(proc, {
        if (regexec(&nice_regex, proc->cmd_buff, 0, NULL, 0) == 0) {
            tree_insert(nice_procs, proc->pid, 1);
        }
    });

    upper_tier_size = GB(1) * config.tier_1_capacity;
    purge_size      = (purge_percent / 100.0) * upper_tier_size;
    upper_obj_size  = 0;
    lower_obj_size  = 0;
    avail           = upper_tier_size - purge_size;

    prio = array_make(Proc_Stats);

    memset(&stats, 0, sizeof(stats));
    PROCESS_FOR(proc, {
        stats.pid = proc->pid;
        array_push(prio, stats);
    });

    array_traverse(prio, statsp) {
        statsp->objects = array_make(md_object*);
        array_traverse(*ranked, obj) {
            if (obj->pid == statsp->pid) {
                array_push(statsp->objects, obj);
            }
        }
    }


    while (avail > PAGE_SIZE && array_len(prio) > 0) {
        qsort(array_data(prio), array_len(prio), prio.elem_size, prio_cmp);

        array_traverse(prio, statsp) {
            idx = 0;
            array_traverse(statsp->objects, obj_it) {
                obj      = *obj_it;
                obj_size = get_object_capacity_estimate(obj);

                if (obj_size <= avail) {
                    array_push(*out_promote, obj);

                    avail          -= obj_size;
                    upper_obj_size += obj_size;

                    array_delete(statsp->objects, idx);

                    statsp->promoted_size +=
                        nice_pid(statsp->pid)
                            ? (u64)((1.0 / (double)nice_coeff) * (double)obj_size)
                            : obj_size;
                    statsp->true_promoted_size += obj_size;

                    goto CONTINUE;
                }

                idx += 1;
            }
        }

        /* No suitable object found... */
        break;

CONTINUE:;
    }

    array_traverse(prio, statsp) {
        array_traverse(statsp->objects, obj_it) {
            obj      = *obj_it;
            obj_size = get_object_capacity_estimate(obj);

            lower_obj_size       += obj_size;
            statsp->demoted_size += obj_size;
            array_push(*out_demote, obj);
        }
        array_free(statsp->objects);
    }

    if (config.print_migrate_stats) {
        array_traverse(prio, statsp) {
            INFO("FAIR-WEIGHTED: pid %d%7s %12.2f MB promoted    %12.2f MB demoted\n",
                statsp->pid,
                nice_pid(statsp->pid) ? " (nice)" : "",
                ((float)statsp->true_promoted_size)/(1024.0*1024.0),
                ((float)statsp->demoted_size)/(1024.0*1024.0));
        }

        INFO("Expected sizes: upper: %12.2f MB lower: %12.2f MB\n",
             ((float)upper_obj_size)/(1024.0*1024.0),
             ((float)lower_obj_size)/(1024.0*1024.0));
    }

    array_free(prio);
}
