#include <md_policy.h>

void md_policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote) {
    u64         upper_tier_size;
    u64         size;
    md_object  *obj;
    u64         obj_size;

    upper_tier_size = GB(1) * config.tier_1_capacity;
    size            = 0;

    array_traverse(*ranked, obj) {
        obj_size = get_object_capacity_estimate(obj);

        if (upper_tier_size >= (size + obj_size)) {
            array_push(*out_promote, obj);
            size = size + obj_size;
        } else {
            array_push(*out_demote, obj);
        }
    }
}
