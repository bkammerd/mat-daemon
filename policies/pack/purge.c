#include <md_policy.h>

static float purge_percent;

void md_policy_add_options(void) {
    add_policy_option("pack-purge-percent",
        OPT_FLOAT,
        "For packing policy 'purge', the percentage of data in the upper tier to purge.",
        &purge_percent);
        default_float("pack-purge-percent", 10.0);
        validate_float("pack-purge-percent", is_float_percentage);
}

void md_policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote) {
    u64        upper_tier_size;
    u64        migrate_size;
    u64        size;
    md_object *obj;
    u64        obj_size;

    upper_tier_size = GB(1) * config.tier_1_capacity;
    migrate_size    = (purge_percent / 100.0) * upper_tier_size;
    size            = 0;

    array_rtraverse(*ranked, obj) {
        obj_size = get_object_capacity_estimate(obj);

        if (migrate_size >= (size + obj_size)) {
            array_push(*out_demote, obj);
            size = size + obj_size;
        }
    }

    INFO("Purging %lu bytes\n", size);
}
