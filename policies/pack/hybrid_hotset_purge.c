#include <md_policy.h>

static float purge_percent;

void md_policy_add_options(void) {
    add_policy_option("pack-purge-percent",
        OPT_FLOAT,
        "For packing policy 'hybrid-hotset-purge', the percentage of data in the upper tier to purge.",
        &purge_percent);
        default_float("pack-purge-percent", 10.0);
        validate_float("pack-purge-percent", is_float_percentage);
}


void md_policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote) {
    u64           upper_tier_size;
    u64           purge_size;
    u64           size;
    md_object    *obj;
    u64           obj_size;
    u64           upper_obj_size, lower_obj_size;

    upper_tier_size = GB(1) * config.tier_1_capacity;
    purge_size      = (purge_percent / 100.0) * upper_tier_size;
    size            = 0;

    upper_obj_size  = 0;
    lower_obj_size  = 0;

    array_traverse(*ranked, obj) {
        obj_size = get_object_capacity_estimate(obj);

        if ((upper_tier_size - purge_size) >= (size + obj_size)) {
            array_push(*out_promote, obj);
            size = size + obj_size;
            upper_obj_size += obj_size;
        } else {
            array_push(*out_demote, obj);
            lower_obj_size += obj_size;
        }
    }

    if (config.print_migrate_stats) {
        INFO("Expected sizes: upper: %12.2f MB lower: %12.2f MB\n",
             ((float)upper_obj_size)/(1024.0*1024.0),
             ((float)lower_obj_size)/(1024.0*1024.0));
    }
}
