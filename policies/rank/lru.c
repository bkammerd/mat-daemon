#include <md_policy.h>

static int md_obj_lru_cmp(const void *a, const void *b) {
    const md_object *na;
    const md_object *nb;

    na = a;
    nb = b;

    if (na->last_access_time_ns == nb->last_access_time_ns) { return 0;  }
    if (na->last_access_time_ns >  nb->last_access_time_ns) { return -1; }
    return 1;
}

void md_policy_ranking_get_ranked_objects(array_t *profile) {
    qsort(array_data(*profile), array_len(*profile),
          sizeof(md_object), md_obj_lru_cmp);
}
