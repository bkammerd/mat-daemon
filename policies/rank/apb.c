#include <md_policy.h>

static int md_obj_apb_cmp(const void *a, const void *b) {
    int              n;
    double           a_accs_scaled, b_accs_scaled;
    u64              a_pres, b_pres;
    double           a_apb, b_apb;
    const md_object *na;
    const md_object *nb;

    na = a;
    nb = b;

    a_accs_scaled = b_accs_scaled = a_pres = b_pres = 0;
    for (n = 0; n < NR_NUMA_NODES; n++) {
        a_accs_scaled += na->llc_misses_scaled[n];
        b_accs_scaled += nb->llc_misses_scaled[n];
    }

    a_pres = get_object_capacity_estimate(na);
    b_pres = get_object_capacity_estimate(nb);

    a_apb = ( a_pres != 0 ) ? ( a_accs_scaled / a_pres ) : 0.0;
    b_apb = ( b_pres != 0 ) ? ( b_accs_scaled / b_pres ) : 0.0;

    if (a_apb == b_apb) { return 0; }
    if (a_apb <  b_apb) { return 1; }
    return -1;
}

void md_policy_ranking_get_ranked_objects(array_t *profile) {
    qsort(array_data(*profile), array_len(*profile),
          sizeof(md_object), md_obj_apb_cmp);
}
