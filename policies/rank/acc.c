#include <md_policy.h>

static int md_obj_acc_cmp(const void *a, const void *b) {
    const md_object *na;
    const md_object *nb;
    double           a_accs_scaled, b_accs_scaled;
    int              n;

    na = a;
    nb = b;

    a_accs_scaled = b_accs_scaled = 0;
    for (n = 0; n < NR_NUMA_NODES; n++) {
        a_accs_scaled += na->llc_misses_scaled[n];
        b_accs_scaled += nb->llc_misses_scaled[n];
    }

    if (a_accs_scaled == b_accs_scaled) { return 0; }
    if (a_accs_scaled <  b_accs_scaled) { return 1; }
    return -1;
}

void md_policy_ranking_get_ranked_objects(array_t *profile) {
    qsort(array_data(*profile), array_len(*profile),
          sizeof(md_object), md_obj_acc_cmp);
}
