#include <md_policy.h>

#include <regex.h>

use_tree(pid_t, int)

static char             *nice_pattern;
static regex_t           nice_regex;
static tree(pid_t, int)  nice_procs;
static float             nice_coeff;

static int compile_regex(struct scfg *cfg, const char *val) {
    int   err;
    char *regex_start;
    char  buff[1024];

    nice_coeff = strtof(val, &regex_start);
    if (regex_start == val) {
        snprintf(buff, sizeof(buff), "failed to parse float from %s", val);
        scfg_validate_set_err(cfg, buff);
        return 0;
    }

    while (*regex_start == ' ') { regex_start += 1; }

    err = regcomp(&nice_regex, regex_start, REG_EXTENDED | REG_NOSUB);
    if (err) {
        buff[0] = 0;
        strcat(buff, "failed to compile regex: ");
        regerror(err, &nice_regex, buff + strlen(buff), sizeof(buff) - strlen(buff) - 1);
        scfg_validate_set_err(cfg, buff);
        return 0;
    }

    return 1;
}

void md_policy_add_options(void) {
    add_policy_option("apb-nice-regex",
        OPT_STRING,
        "For ranking policy 'apb-nice', a float followed by a regex pattern that when matched against a process name, will use the float value as a coefficient for that process's APB metric.",
        &nice_pattern);
        validate_string("apb-nice-regex", compile_regex);
}

static int nice_pid(pid_t pid) {
    tree_it(pid_t, int) lookup;

    lookup = tree_lookup(nice_procs, pid);

    return tree_it_good(lookup) && tree_it_val(lookup);
}

static int md_obj_apb_cmp(const void *a, const void *b) {
    int              n;
    double           a_accs_scaled, b_accs_scaled;
    u64              a_pres, b_pres;
    double           a_apb, b_apb;
    const md_object *na;
    const md_object *nb;

    na = a;
    nb = b;

    a_accs_scaled = b_accs_scaled = a_pres = b_pres = 0;
    for (n = 0; n < NR_NUMA_NODES; n++) {
        a_accs_scaled += na->llc_misses_scaled[n];
        b_accs_scaled += nb->llc_misses_scaled[n];
    }

    a_pres = get_object_capacity_estimate(na);
    b_pres = get_object_capacity_estimate(nb);

    a_apb = ( a_pres != 0 ) ? ( a_accs_scaled / a_pres ) : 0.0;
    b_apb = ( b_pres != 0 ) ? ( b_accs_scaled / b_pres ) : 0.0;

    if (nice_pid(na->pid)) {
        a_apb *= nice_coeff;
    }
    if (nice_pid(nb->pid)) {
        b_apb *= nice_coeff;
    }

    if (a_apb == b_apb) { return 0; }
    if (a_apb <  b_apb) { return 1; }
    return -1;
}

void md_policy_ranking_get_ranked_objects(array_t *profile) {
    md_proc *proc;

    if (nice_procs != NULL) {
        tree_free(nice_procs);
    }
    nice_procs = tree_make(pid_t, int);

    PROCESS_FOR(proc, {
        if (regexec(&nice_regex, proc->cmd_buff, 0, NULL, 0) == 0) {
            tree_insert(nice_procs, proc->pid, 1);
        }
    });

    qsort(array_data(*profile), array_len(*profile),
          sizeof(md_object), md_obj_apb_cmp);
}
