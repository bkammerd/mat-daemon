#include <md_policy.h>

#include <numa.h>

static float threshold;

void md_policy_add_options(void) {
    add_policy_option("trigger-pressure-percent",
        OPT_FLOAT,
        "For trigger policy 'pressure', the threshold percentage of tier-1-capacity that triggers migration.",
        &threshold);
        default_float("trigger-pressure-percent", 98.0);
        validate_float("trigger-pressure-percent", is_float_percentage);
}

/* #define CGROUP_PER_NODE_MAX */

static u64 get_tier_1_used(void) {
#ifdef CGROUP_PER_NODE_MAX
    FILE *f;
    u64   used;

    f = fopen("/sys/fs/cgroup/1/memory.node0_current", "r");
    if (f == NULL) { return 0; }

    fscanf(f, "%lu", &used);

    fclose(f);

    return used;
#else
    long long numa_capacity;
    long long numa_free;

    numa_capacity = numa_node_size64(config.tier_1_node, &numa_free);
    return numa_capacity - numa_free;
#endif
}

int md_policy_trigger_should_migrate(void) {
    u64 upper_tier_cap;
    u64 upper_tier_used;

    upper_tier_cap  = GB(1) * config.tier_1_capacity;
    upper_tier_used = get_tier_1_used();

    return   ((((double)upper_tier_used)
             / ((double)upper_tier_cap))
             * 100.0)
           >= threshold;
}
