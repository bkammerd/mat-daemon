#include <md_policy.h>

static int threshold;
static int delay;

void md_policy_add_options(void) {
    add_policy_option("trigger-alloc-threshold",
        OPT_INT,
        "For trigger policy 'alloc', the threshold of new allocations (in MB) before triggering migration.",
        &threshold);
        default_int("trigger-alloc-threshold", 10000);

    add_policy_option("trigger-alloc-delay",
        OPT_INT,
        "For trigger policy 'alloc', intervals to wait after alloc threshold is reached before triggering migration.",
        &delay);
        default_int("trigger-alloc-delay", 10);
}

int md_policy_trigger_should_migrate(void) {
    u64      alloc_since_last_migration;
    u64      trigger_bytes;
    md_proc *proc;

    fprintf(config.profile_output_file, "jallcs: %lu %lu\n",
            profile_interval_counter, alloc_trigger_interval);

    if (alloc_trigger_interval != 0) {
        if (profile_interval_counter >= alloc_trigger_interval) {
            alloc_trigger_interval = 0;
            return 1;
        }
        return 0;
    }

    alloc_since_last_migration = 0;
    PROCESS_FOR(proc, {
        alloc_since_last_migration += proc->obj_allocs_since_last_migration;
    });

    trigger_bytes = MB(threshold);

    fprintf(config.profile_output_file, "allocs: %12.2f MB (trigger: %12.2f MB)\n",
            (((float)alloc_since_last_migration) / (1024.0*1024.0)),
            (((float)trigger_bytes) / (1024.0*1024.0))
            );

    if (alloc_since_last_migration > trigger_bytes) {
        if (delay == 0) {
            return 1;
        }
        alloc_trigger_interval = (profile_interval_counter + delay);
        fprintf(config.profile_output_file, " alloc trigger at: %lu\n",
                alloc_trigger_interval);
    }

    return 0;
}
