#include <md_policy.h>

static float threshold;

void md_policy_add_options(void) {
    add_policy_option("trigger-event-ratio-threshold",
        OPT_FLOAT,
        "For trigger policy 'event-ratio', threshold of relative change between intervals to move the data.",
        &threshold);
        default_float("trigger-event-ratio-threshold", 1.0);
}

int md_policy_trigger_should_migrate(void) {
    int      trigger;
    md_proc *proc;
    float    ratio;
    int      i;

    trigger = 0;

    PROCESS_FOR(proc, {
        if (proc->profile_info == NULL
        ||  proc->profile_info->profiled_intervals <= RATIO_WINDOW_SIZE) {

            continue;
        }

        ratio = 0.0;
        for (i = 0; i < RATIO_WINDOW_SIZE; i += 1) {
            ratio += proc->profile_info->perf.ratio_window[i];
        }
        ratio /= (float)RATIO_WINDOW_SIZE;

        ratio = fabs((proc->profile_info->perf.cur_ratio - ratio) / ratio);

        if (ratio >= threshold) {
            if (!proc->profile_info->perf.spiking) {
                proc->profile_info->perf.spiking = 1;
                trigger = 1;
            }
        } else {
            if (proc->profile_info->perf.spiking) {
                proc->profile_info->perf.spiking = 0;
            }
        }
    });

    return trigger;
}
