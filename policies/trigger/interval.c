#include <md_policy.h>

static int interval_period;

void md_policy_add_options(void) {
    add_policy_option("trigger-interval-period",
        OPT_INT,
        "For trigger policy 'interval', the number of profiling intervals to wait before triggering migration.",
        &interval_period);
        default_int("trigger-interval-period", 10);
}

int md_policy_trigger_should_migrate(void) {
    return (profile_interval_counter % interval_period == 0);
}
