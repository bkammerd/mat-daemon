#!/usr/bin/env bash

OPT="-O0 -g"
# OPT="-O3"
CFLAGS="-shared -fPIC ${OPT} -ftls-model=initial-exec -ldl -lrt -lnuma -I${BKMALLOC_INC_PATH} -I../src"

echo "  CC       mdbkhooks.so"
gcc -o mdbkhooks.so mdbkhooks.c ${CFLAGS}
