#define BKMALLOC_HOOK
#define BK_RETURN_ADDR
#include "bkmalloc.h"

#define MDMSG_IMPL
#define MDMSG_SENDER
#include "mdmsg.h"
#define MDADV_IMPL
#define MDADV_RECEIVER
#include "mdadv.h"

#include "tree.h"

#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <numaif.h>
#include <numa.h>
#include <execinfo.h>


#define NUM_HEAPS      (1024)
#define MAX_HEAP_IDX   (NUM_HEAPS - 1)
#define MD_BK_PMM_HEAP (1 << 8)
#define BT_LEN         (16)

static bk_Heap *pmm_heaps[NUM_HEAPS];

static thread_local int no_hooks;

static void *malloc_nohooks(size_t n_bytes) {
    void *ret;

    no_hooks = 1;
    ret      = malloc(n_bytes);
    no_hooks = 0;

    return ret;
}

static void free_nohooks(void *ptr) {
    no_hooks = 1;
    free(ptr);
    no_hooks = 0;
}

#define malloc malloc_nohooks
#define free   free_nohooks
use_tree(u64, u32);
#undef malloc
#undef free

static int                      initialized;
static int                      pid;
static mdmsg_sender             sender;
static mdadv_receiver           advice_receiver;
static int                      no_msg;
static int                      do_advice;
static int                      lockout;
static pthread_t                advice_receiver_pthread;
static tree(u64, u32)           advice_tree;
static pthread_rwlock_t         advice_rwlock = PTHREAD_RWLOCK_INITIALIZER;
static bk_Spinlock              site_print_lock;


static void *advice_receiver_thread(void *arg) {
    mdadv advice;

    (void)arg;

    for (;;) {
        mdadv_receive(&advice_receiver, &advice);
/*         if (advice.advice == 1) { */
/*             bk_printf("ADVICE RECOMMENDS PMM: site = 0x%X\n", (void*)advice.inst_addr); */
/*         } */
        if (advice.inst_addr == MDADV_LOCKOUT) {
            if (advice.advice == MDADV_LOCKOUT_SET) {
                if (!lockout) {
                    bk_printf("UPPER TIER LOCKOUT SET\n");
                    lockout = 1;
                }
            } else if (advice.advice == MDADV_LOCKOUT_CLEAR) {
                if (lockout) {
                    bk_printf("UPPER TIER LOCKOUT CLEAR\n");
                    lockout = 0;
                }
            }
        } else {
            pthread_rwlock_wrlock(&advice_rwlock);
            tree_insert(advice_tree, advice.inst_addr, advice.advice);
            pthread_rwlock_unlock(&advice_rwlock);
        }
    }

    return NULL;
}

__attribute__((constructor))
static void init(void) {
    int    err;
    mdmsg  msg;
    void  *bt_buff[BT_LEN];

    err = 0;

    if (getenv("MDBK_NOMSG") != NULL) {
        bk_printf("mdbkhooks: aligning objects, but not sending messages\n");
        no_msg = 1;
        goto out;
    }

    err = mdmsg_start_sender(&sender);
    if (err != 0) {
        bk_printf("mdbkhooks: mdmsg_start_sender() failed with error %d\nis mat-daemon running?\n", err);
        bk_unhook();
        return;
    }

    /* Call backtrace() once with our hooks disabled.
     * It might need to call malloc() for other lazy library loading
     * and we want to avoid that when we call it for each allocation.
     * It should not call malloc() after this point...
     */
    no_hooks = 1;
    backtrace(bt_buff, BT_LEN);
    no_hooks = 0;

    if (getenv("MDBK_ADV") != NULL) {
        do_advice = 1;

        bk_printf("mdbkhooks: planning to receive advice from mat-daemon\n");

        advice_tree = tree_make(u64, u32);

        err = mdadv_start_receiver(&advice_receiver, getpid());
        if (err != 0) {
            bk_printf("mdbkhooks: mdadv_start_receiver() failed with error %d\n", err);
            bk_unhook();
            return;
        }

        pthread_create(&advice_receiver_pthread, NULL, advice_receiver_thread, NULL);
    }

    pid = getpid();

    msg.header.msg_type = MDMSG_INIT;
    msg.header.pid      = pid;

    err = mdmsg_send(&sender, &msg);
    if (err < 0) {
        bk_printf("mdbkhooks: mdmsg_send() failed with error %d\n", err);
        bk_unhook();
        return;
    }

out:;
    initialized = 1;
}

__attribute__((destructor))
static void fini(void) {
    mdmsg msg;
    int   err;

    if (!initialized || no_msg) { return; }

    msg.header.msg_type = MDMSG_FINI;
    msg.header.pid      = pid;

    err = mdmsg_send(&sender, &msg);
    if (err < 0) {
        bk_printf("mdbkhooks: mdmsg_send() failed with error %d -- qd = %d\n", err, sender.qd);
    }

    if (do_advice) {
        mdadv_close_receiver(&advice_receiver);
    }

    mdmsg_close_sender(&sender);
}

static void alloc_msg(u32 hid, void *addr, u64 n_bytes) {
    struct timespec ts;
    mdmsg           msg;
    int             err;

    if (!initialized || no_msg) { return; }

    clock_gettime(CLOCK_MONOTONIC, &ts);

    msg.header.msg_type   = MDMSG_ALLO;
    msg.header.pid        = pid;
    msg.allo.addr         = (u64)addr;
    msg.allo.size         = n_bytes;
    msg.allo.timestamp_ns = 1000000000ULL * ts.tv_sec + ts.tv_nsec;
    msg.allo.inst_addr    = (u64)BK_GET_RA();
    msg.allo.hid          = hid;

    err = mdmsg_send(&sender, &msg);
    if (err < 0) {
        bk_printf("mdbkhooks: mdmsg_send() failed with error %d\n", err);
    }
}

static void free_msg(void *addr) {
    struct timespec ts;
    mdmsg           msg;
    int             err;

    if (!initialized || no_msg) { return; }

    clock_gettime(CLOCK_MONOTONIC, &ts);

    msg.header.msg_type   = MDMSG_FREE;
    msg.header.pid        = pid;
    msg.free.addr         = (u64)addr;
    msg.free.timestamp_ns = 1000000000ULL * ts.tv_sec + ts.tv_nsec;

    err = mdmsg_send(&sender, &msg);
    if (err < 0) {
        bk_printf("mdbkhooks: mdmsg_send() failed with error %d\n", err);
    }
}

static void bind_to_pmm(void *addr, u64 size) {
    u64 nodemask;

    nodemask = 1LL << 1;

    mbind(addr, size, MPOL_PREFERRED, &nodemask, 8 * sizeof(nodemask), MPOL_MF_MOVE);
}

void bk_block_new_hook(bk_Heap *heap, union bk_Block *block) {
    if (no_hooks) { return; }

    if (block->meta.size_class < PAGE_SIZE) {
        alloc_msg(heap == NULL ? 0 : !!(heap->flags & MD_BK_PMM_HEAP), (void*)block, block->meta.size);
    } else {
        if (do_advice && heap->flags & MD_BK_PMM_HEAP) {
            bind_to_pmm((void*)block, block->meta.size);
        }
    }
}

void bk_block_release_hook(bk_Heap *heap, union bk_Block *block) {
    if (no_hooks) { return; }

    if (block->meta.size_class < PAGE_SIZE) {
        free_msg((void*)block);
    }
}

static bk_Heap *get_pmm_heap(u32 idx) {
    bk_Heap *heap;
    char     name_buff[128];

    heap = pmm_heaps[idx];

    if (unlikely(heap == NULL)) {
        bk_sprintf(name_buff, "md_bk_pmm_heap_%u", idx);

        no_hooks = 1;
        heap     = bk_heap(name_buff);
        no_hooks = 0;

        heap->flags    |= MD_BK_PMM_HEAP;
        pmm_heaps[idx]  = heap;

        bk_printf("!!! made a new PMM heap for idx %u\n", idx);
    }

    return heap;
}

static u32 advice_for_site(u64 ra) {
    tree_it(u64, u32) advice_lookup;
    u32               advice;

    if (lockout) { return 1; }

    pthread_rwlock_rdlock(&advice_rwlock);
    advice_lookup = tree_lookup(advice_tree, ra);
    advice        = tree_it_good(advice_lookup) ? tree_it_val(advice_lookup) : 0;
    pthread_rwlock_unlock(&advice_rwlock);

    return advice;
}

#define GET_SITE_ID()                     \
({                                        \
    u64   site_id;                        \
    int   n_bt;                           \
    void *bt_buff[BT_LEN];                \
    int   i;                              \
                                          \
    site_id = 2654435761ULL;              \
    n_bt    = backtrace(bt_buff, BT_LEN); \
                                          \
    for (i = 0; i < n_bt; i += 1) {       \
        site_id *= (u64)bt_buff[i];       \
    }                                     \
    site_id ^= ((u64)n_bt) << 56;         \
                                          \
    site_id;                              \
})

void bk_pre_alloc_hook(bk_Heap **heapp, u64 *n_bytesp, u64 *alignmentp, int *zero_memp) {
    u64   site_id;
/*     u64   ra; */
    u32   advice;

    if (no_hooks) { return; }

    site_id = 0;

    if (*n_bytesp >= PAGE_SIZE) {
        if (*alignmentp < PAGE_SIZE) {
            *alignmentp = PAGE_SIZE;
        }
        if (!IS_ALIGNED(*n_bytesp, PAGE_SIZE)) {
            *n_bytesp = ALIGN_UP(*n_bytesp, PAGE_SIZE);
        }

        site_id = GET_SITE_ID();
    }

    if (do_advice && site_id) {
/*         ra     = (u64)BK_GET_RA(); */
/*         advice = advice_for_site(ra); */
        advice = advice_for_site(site_id);

        if (advice == 1) {
            if ((*heapp)->hid > MAX_HEAP_IDX) {
                bk_printf("mdbkhooks: HID too large (%u) to provide PMM alternative!\n", (*heapp)->hid);
                return;
            }

            *heapp = get_pmm_heap((*heapp)->hid);
        }
    }
}

void bk_post_alloc_hook(bk_Heap *heap, u64 n_bytes, u64 alignment, int zero_mem, void *addr) {
    bk_Block *block;
    u32       r;
    u32       s;
    bk_Chunk *chunk;

    if (no_hooks) { return; }

    if (n_bytes >= PAGE_SIZE) {
        block = BK_ADDR_PARENT_BLOCK(addr);

        if (addr >= block->meta.bump_base) {
            /* Don't need to do anything special for bump allocations. */

        } else if (addr >= (void*)block->_slot_space) {
            if (bk_addr_to_region_and_slot(addr, &r, &s)) {
                BK_SET_SLOT_HOOK_BIT(block->slots, r, s);
            }

        } else if (addr >= (void*)block->_chunk_space) {
            chunk = BK_CHUNK_FROM_USER_MEM(addr);
            chunk->header.flags |= BK_HOOK_FLAG_0;

        }

        alloc_msg(heap == NULL ? 0 : !!(heap->flags & MD_BK_PMM_HEAP), addr, n_bytes);
    }
}

void bk_pre_free_hook(bk_Heap *heap, void *addr) {
    bk_Block *block;
    u32       r;
    u32       s;
    bk_Chunk *chunk;

    if (no_hooks) { return; }

    block = BK_ADDR_PARENT_BLOCK(addr);

    if (addr >= block->meta.bump_base) {
        if (bk_malloc_size(addr) < PAGE_SIZE) {
            return;
        }

    } else if (addr >= (void*)block->_slot_space) {
        if (block->meta.size_class < PAGE_SIZE
        ||  !bk_addr_to_region_and_slot(addr, &r, &s)
        ||  !BK_GET_SLOT_HOOK_BIT(block->slots, r, s)) {

            return;
        }

    } else if (addr >= (void*)block->_chunk_space) {
        chunk = BK_CHUNK_FROM_USER_MEM(addr);

        if (!(chunk->header.flags & BK_HOOK_FLAG_0)) {
            return;
        }

        chunk->header.flags &= ~BK_HOOK_FLAG_0;

    }

    free_msg(addr);
}

void bk_post_mmap_hook(void *addr, size_t length, int prot, int flags, int fd, off_t offset, void *ret_addr) {
    u64 site_id;

    if (no_hooks) { return; }

    alloc_msg(fd >= 0 ? MDMSG_HID_FILE_BACKED : 0, ret_addr, length);

    site_id = GET_SITE_ID();

/*     if (do_advice && advice_for_site((u64)BK_GET_RA()) == 1) { */
    if (do_advice && advice_for_site(site_id) == 1) {
        bind_to_pmm(ret_addr, length);
    }
}

void bk_post_munmap_hook(void *addr, size_t length) {
    if (no_hooks) { return; }

    free_msg(addr);
}
