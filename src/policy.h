#ifndef __POLICY_H__
#define __POLICY_H__

#include "array.h"

typedef int  (*md_trigger_policy)(void);
typedef void (*md_ranking_policy)(array_t*);
typedef void (*md_packing_policy)(array_t*, array_t*, array_t*);

typedef struct {
    md_trigger_policy trigger;
    md_ranking_policy rank;
    md_packing_policy pack;
} md_policy;


int init_policies(void);
int load_trigger_policy(const char *path);
int load_ranking_policy(const char *path);
int load_packing_policy(const char *path);
int load_combined_policy(const char *path);

int  policy_trigger_should_migrate(void);
void policy_ranking_get_ranked_objects(array_t *collected_profile);
void policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote);

#endif
