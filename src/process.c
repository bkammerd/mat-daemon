#include "process.h"
#include "common.h"
#include "config.h"
#include "array.h"
#include "profiling.h"
#include "object_origin.h"

#include <pthread.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <numa.h>
#include <numaif.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sysinfo.h>


hash_table(pid_t, md_proc) process_table;
pthread_rwlock_t           process_table_rwlock = PTHREAD_RWLOCK_INITIALIZER;

#define WLOCK()  (pthread_rwlock_wrlock(&process_table_rwlock))

static u64 pid_hash(pid_t pid) { return (u64)pid; }

void add_process(pid_t pid) {
    md_proc new_proc;
    char    cmd_path[256];
    int     cmd_fd;
    int     n_read;

    memset(&new_proc, 0, sizeof(new_proc));

    new_proc.pid             = pid;
    new_proc.alive           = 1;
    new_proc.object_map      = make_object_map();
    new_proc.retired_objects = make_retired_map();
    new_proc.hid_string      = tree_make(u32, md_string);
    new_proc.active          = 1;

    /* Just to be cute. */

    snprintf(cmd_path, sizeof(cmd_path),
             "/proc/%d/cmdline", pid);

    cmd_fd = open(cmd_path, O_RDONLY);
    if (cmd_fd < 0)  { goto bad; }

    n_read = read(cmd_fd, new_proc.cmd_buff, sizeof(new_proc.cmd_buff) - 1);
    if (n_read <= 0) { goto bad; }

    new_proc.cmd_buff[n_read] = 0;

    close(cmd_fd);

    goto print;

bad:;
    sprintf(new_proc.cmd_buff, "<unknown>");

print:;
    if (!config.mdtop) {
        printf("PROC  %d %s\n", pid, new_proc.cmd_buff);
    }

    pthread_mutex_init(&new_proc.mtx, NULL);

    WLOCK(); {
        hash_table_insert(process_table, new_proc.pid, new_proc);
    } UNLOCK();

    setup_profiling_for_process(pid);
}

void del_process(pid_t pid, int needs_lock) {
    md_proc *proc;

    WLOCK();

    proc = hash_table_get_val(process_table, pid);
    if (proc == NULL) { goto out_unlock; }

    if (!config.mdtop) { printf("EXIT  %d\n", pid); }

    lock_process(proc);
    free_object_map(proc->object_map);
    stop_profiling_for_process(pid, proc, 0);
    if (proc->advice_queue.qd > 0) {
        mdadv_close_sender(&proc->advice_queue);
    }
    unlock_process(proc);

    hash_table_delete(process_table, pid);

out_unlock:;
    UNLOCK();
}


void add_object_to_process_no_lock(pid_t pid, md_object *obj) {
    md_object          split_object;
    md_proc           *proc;
    md_object         *lookup;
    md_object_map_it   it;
    u64                split_size;
    u64                cur_addr;
    u64                obj_size;
    md_retired_map_it  rit;

    if (obj->addr == 0ULL) {
        if (obj->end > PAGE_SIZE) { obj->addr = PAGE_SIZE; }
        else                      { return;                }
    }

    obj->addr = ALIGN(obj->addr, PAGE_SIZE);
    obj->end  = obj->end & ~(PAGE_SIZE - 1ULL);

    if (obj->end <= obj->start) { return; }

    obj->current_node = -1;
    obj_size   = obj->end - obj->start;
    split_size = MB(config.large_object_threshold);

    if (obj_size > TB(1)) {
        if (!(config.split_large_objects && split_size <= TB(1))) {
            ERR("pid %d requested object 0x%lx which has a size >= 1 TB (size = %lu)!\n"
                "This is not allowed since object splitting is disabled in the current configuration.\n",
                pid, obj->start, obj_size);
            return;
        }
    }

    if (config.split_large_objects && (split_size > PAGE_SIZE) && (obj_size > split_size)) {
#if 0
        fprintf(config.profile_output_file, "splitting object: %lx (%llu MB) split: %llu MB\n",
                obj->start, TO_MB(obj_size), TO_MB(split_size));
        fflush(config.profile_output_file);
#endif
        cur_addr = obj->start;
        memcpy(&split_object, obj, sizeof(split_object));
        while (cur_addr < obj->end) {
            split_object.start    = cur_addr;
            //cur_addr              = MIN(obj->end, cur_addr + split_size - PAGE_SIZE);
            cur_addr              = MIN(obj->end, cur_addr + split_size);
            split_object.end      = cur_addr;
            split_object.orig_end = obj->end;
            add_object_to_process_no_lock(pid, &split_object);
        }
#if 0
        fprintf(config.profile_output_file, "done splitting:   %lx (%llu MB) split: %llu MB\n",
                obj->start, TO_MB(obj_size), TO_MB(split_size));
        fflush(config.profile_output_file);
#endif
        return;
    }

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    proc->active = 1;

    lookup = map_get_object(proc->object_map, obj->addr);

    if (lookup != NULL) {
        fprintf(config.profile_output_file, "overwrite: %d %lx\n", pid, obj->addr);
        del_object_from_process_no_lock(pid, obj->addr);
    }

    rit = tree_lookup(proc->retired_objects, obj->addr);
    if (tree_it_good(rit)) {
        obj->current_node = tree_it_val(rit).node;
    }

    it = tree_insert(proc->object_map, obj->addr, *obj);
    if (!tree_it_good(it)) {
        ERR("failed to insert an object 0x%lx for pid %d\n", obj->addr, pid);
    }
#if OBJECT_ORIGIN_DETAIL
    object_origin_get_async(&tree_it_val(it));
#endif

    proc->obj_allocs_since_last_migration += get_object_capacity_estimate(obj);
    proc->obj_allocs_this_interval += 1;

#if 0
    fprintf(config.profile_output_file, "added object:       %lx (%llu MB)\n",
            obj->start, TO_MB(obj_size));
    fflush(config.profile_output_file);
#endif

    release_process(proc);
}

void del_object_from_process_no_lock(pid_t pid, u64 addr) {
    md_proc           *proc;
    md_object         *lookup;
    u64                split_size;
    u64                split_next;
    u64                lk_addr, lk_end, lk_orig_end;
    md_retired_object  retired;

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    proc->active = 1;

    lookup = map_get_object(proc->object_map, addr);

    if (lookup == NULL) {
        //fprintf(config.profile_output_file, " lookup nil\n");
        release_process(proc);
        return;
    }

    proc->obj_frees_this_interval += 1;

    if (addr != lookup->addr) {
        release_process(proc);
        return;
    }

    lk_addr     = lookup->addr;
    lk_end      = lookup->end;
    lk_orig_end = lookup->orig_end;

    retired.size = lookup->end - lookup->start;
    retired.node = lookup->current_node;
    tree_insert(proc->retired_objects, addr, retired);

#if OBJECT_ORIGIN_DETAIL
    object_origin_cancel_if_in_flight(lookup);
#endif
    tree_delete(proc->object_map, addr);

    release_process(proc);

    if (config.split_large_objects && (lk_orig_end > lk_end)) {
        split_size = MB(config.large_object_threshold);
        split_next = lk_addr + split_size;
        del_object_from_process_no_lock(pid, split_next);
    }
}

void update_object_hid_no_lock(pid_t pid, u64 addr, u32 hid) {
    md_proc   *proc;
    md_object *obj;

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    obj = map_get_object(proc->object_map, addr);
    if (obj != NULL) {
        obj->hid = hid;
    }

    release_process(proc);
}

void insert_string_for_hid_no_lock(pid_t pid, u32 hid, md_string s) {
    md_proc *proc;

    proc = acquire_process(pid);
    if (proc == NULL) {return;}

    tree_insert(proc->hid_string, hid, strdup(s));

    release_process(proc);
}

void create_regions_in_process_for_range_no_lock(pid_t pid, u64 start, u64 end, u64 ts_ns) {
    md_proc   *proc;
    md_object  obj;
    u64        region_addr;
    u64        region_size;

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    region_addr = (start >> config.region_shift) << config.region_shift;
    region_size = (1 << config.region_shift);

    while (region_addr < end) {
        if (map_get_object(proc->object_map, region_addr) != NULL) {
            goto next;
        }

        memset(&obj, 0, sizeof(obj));

        obj.start               = region_addr;
        obj.end                 = region_addr + region_size;
        obj.creation_time_ns    = ts_ns;
        obj.last_access_time_ns = ts_ns;
        obj.pid                 = pid;
        obj.current_node        = -1;

        tree_insert(proc->object_map, obj.addr, obj);

next:;
        region_addr += region_size;
    }

    release_process(proc);
}

void add_object_to_process(pid_t pid, md_object *obj) {
    md_proc *proc;

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    add_object_to_process_no_lock(pid, obj);

    release_process(proc);
}

void del_object_from_process(pid_t pid, u64 addr) {
    md_proc *proc;

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    del_object_from_process_no_lock(pid, addr);

    release_process(proc);
}

void update_object_hid(pid_t pid, u64 addr, u32 hid) {
    md_proc *proc;

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    update_object_hid_no_lock(pid, addr, hid);

    release_process(proc);
}

void insert_string_for_hid(pid_t pid, u32 hid, md_string s){
    md_proc *proc;

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    insert_string_for_hid_no_lock(pid, hid, s);

    release_process(proc);
}

void create_regions_in_process_for_range(pid_t pid, u64 start, u64 end, u64 ts_ns) {
    md_proc *proc;

    proc = acquire_process(pid);
    if (proc == NULL) { return; }

    create_regions_in_process_for_range_no_lock(pid, start, end, ts_ns);

    release_process(proc);
}

int init_process_table(void) {
    process_table = hash_table_make(pid_t, md_proc, pid_hash);
    return 0;
}

md_proc *acquire_process(pid_t pid) {
    md_proc *proc;

    RLOCK();

    proc = hash_table_get_val(process_table, pid);

    if (proc == NULL) {
        UNLOCK();
        return NULL;
    }

    return proc;
}

void release_process(md_proc *proc) {
    if (proc == NULL) { return; }

    UNLOCK();
}

void lock_process(md_proc *proc) {
  pthread_mutex_lock(&proc->mtx);
}
void unlock_process(md_proc *proc) {
  pthread_mutex_unlock(&proc->mtx);
}
