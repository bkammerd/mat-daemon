#include "object.h"
#include "mdmsg.h"

md_object *map_get_object(md_object_map map, u64 addr) {
    md_object_map_it  it;
    md_object        *obj;

    it  = tree_gtr(map, addr);
    tree_it_prev(it);

    if (!tree_it_good(it)) { return NULL; }

    obj = &tree_it_val(it);

    if (addr >= obj->end) { return NULL; }

    return obj;
}

md_object_map copy_object_map(md_object_map map) {
    md_object_map    new_map;
    md_object_map_it it;

    new_map = make_object_map();

    tree_traverse(map, it) {
        tree_insert(new_map, tree_it_key(it), tree_it_val(it));
    }

    return new_map;
}

int md_object_scaled_acc_cmp(const void *a, const void *b) {
    int              i;
    float            a_scaled;
    float            b_scaled;
    const md_object *na;
    const md_object *nb;

    na = a;
    nb = b;

    a_scaled = b_scaled = 0.0;
    for (i = 0; i < NR_NUMA_NODES; i++) {
        a_scaled += na->llc_misses_scaled[i];
        b_scaled += nb->llc_misses_scaled[i];
    }

    return  (int) ( b_scaled - a_scaled );
}

u64 get_object_capacity_estimate(const md_object *obj) {
/*     if (obj->hid == MDMSG_HID_FILE_BACKED) { */
/*         return (obj->present_pages[0] + obj->present_pages[1]) * PAGE_SIZE; */
/*     } */

    return obj->end - obj->start;
}
