#include "thread_safe_queue.h"

md_ts_queue _thread_safe_queue_make(int elem_size) {
    md_ts_queue q;

    memset(&q, 0, sizeof(q));

    pthread_mutex_init(&q.mtx, NULL);
    q.q = _array_make(elem_size);

    return q;
}

void _thread_safe_queue_free(md_ts_queue *q) {
    pthread_mutex_lock(&q->mtx);
    array_free(q->q);
    pthread_mutex_unlock(&q->mtx);
    pthread_mutex_destroy(&q->mtx);
}

void _thread_safe_queue_enqueue(md_ts_queue *q, void *elem_ptr) {
    pthread_mutex_lock(&q->mtx);

    _array_insert(&q->q, 0, elem_ptr);

    pthread_mutex_unlock(&q->mtx);
}

int _thread_safe_queue_peek_keep_locked(md_ts_queue *q, void *dst_ptr) {
    void *last_elem;

    pthread_mutex_lock(&q->mtx);

    last_elem = array_last(q->q);

    if (last_elem == NULL) { return 0; }

    if (dst_ptr != NULL) {
        memcpy(dst_ptr, last_elem, q->q.elem_size);
    }

    return 1;
}

int _thread_safe_queue_peek(md_ts_queue *q, void *dst_ptr) {
    int ret;

    ret = _thread_safe_queue_peek_keep_locked(q, dst_ptr);

    pthread_mutex_unlock(&q->mtx);

    return ret;
}

int _thread_safe_queue_dequeue_keep_locked(md_ts_queue *q, void *dst_ptr) {
    void *last_elem;

    pthread_mutex_lock(&q->mtx);

    last_elem = array_last(q->q);

    if (last_elem == NULL) { return 0; }

    if (dst_ptr != NULL) {
        memcpy(dst_ptr, last_elem, q->q.elem_size);
    }

    array_pop(q->q);

    return 1;
}

int _thread_safe_queue_dequeue(md_ts_queue *q, void *dst_ptr) {
    int ret;

    ret = _thread_safe_queue_dequeue_keep_locked(q, dst_ptr);

    pthread_mutex_unlock(&q->mtx);

    return ret;
}
