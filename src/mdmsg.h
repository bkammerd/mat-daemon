#ifndef __MDMSG_H__
#define __MDMSG_H__

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>

#define MDMSG_MQ_NAME         "/mdmsg.mq"
#define MDMSG_PRIO            (0)
#define MDMSG_HID_FILE_BACKED (0xFFFFFFFF)

enum {
    MDMSG_INIT,
    MDMSG_FINI,
    MDMSG_ALLO,
    MDMSG_UPDT,
    MDMSG_LABL,
    MDMSG_FREE,
};

enum {
    MDMSG_MODE_OBJECT,
    MDMSG_MODE_SITE_BLOCK,
};

typedef struct __attribute__((packed)) {
    uint8_t msg_type;
    pid_t   pid;
} mdmsg_header;

typedef struct __attribute__((packed)) {
    uint8_t mode;
} md_init_msg;

typedef struct __attribute__((packed)) {
} md_fini_msg;

typedef struct __attribute__((packed)) {
    uint64_t addr;
    uint64_t size;
    uint64_t timestamp_ns;
    uint64_t inst_addr;
    uint32_t hid;
} md_allo_msg;

typedef struct __attribute__((packed)) {
    uint64_t addr;
    uint32_t hid;
} md_update_hid_msg;

typedef struct __attribute__((packed)) {
    uint32_t hid;
    char     label[32];
} md_label_hid_msg;

typedef struct __attribute__((packed)) {
    uint64_t addr;
    uint64_t timestamp_ns;
} md_free_msg;

typedef struct __attribute__((packed)) {
    mdmsg_header   header;
    union {
        md_init_msg       init;
        md_fini_msg       fini;
        md_allo_msg       allo;
        md_update_hid_msg updt;
        md_label_hid_msg  labl;
        md_free_msg       free;
    };
} mdmsg;



#ifdef MDMSG_SENDER

typedef struct {
    mqd_t qd;
} mdmsg_sender;

int mdmsg_start_sender(mdmsg_sender *sender);
int mdmsg_close_sender(mdmsg_sender *sender);
int mdmsg_send(mdmsg_sender *sender, mdmsg *msg);

#endif /* MDMSG_SENDER */

#ifdef MDMSG_RECEIVER

typedef struct {
    mqd_t qd;
} mdmsg_receiver;

int mdmsg_start_receiver(mdmsg_receiver *receiver);
int mdmsg_close_receiver(mdmsg_receiver *receiver);
int mdmsg_receive(mdmsg_receiver *receiver, mdmsg *msg);
int mdmsg_receive_or_timeout_ms(mdmsg_receiver *receiver, mdmsg *msg, uint64_t timeout_ms);

#endif /* MDMSG_RECEIVER */


#ifdef MDMSG_IMPL

static uint32_t _mdmsg_size(mdmsg *msg) {
    uint32_t size;

    size = sizeof(mdmsg_header);

    switch (msg->header.msg_type) {
        case MDMSG_INIT:
            size += sizeof(md_init_msg);
            break;
        case MDMSG_FINI:
            size += sizeof(md_fini_msg);
            break;
        case MDMSG_ALLO:
            size += sizeof(md_allo_msg);
            break;
        case MDMSG_UPDT:
            size += sizeof(md_update_hid_msg);
            break;
        case MDMSG_LABL:
            size += sizeof(md_label_hid_msg);
            break;
        case MDMSG_FREE:
            size += sizeof(md_free_msg);
            break;
        default:
            size = 0;
    }

    return size;
}

#ifdef MDMSG_SENDER

int mdmsg_start_sender(mdmsg_sender *sender) {
    int err;

    err = 0;

    if ((sender->qd = mq_open(MDMSG_MQ_NAME, O_WRONLY)) == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    return err;
}

int mdmsg_close_sender(mdmsg_sender *sender) { return mq_close(sender->qd); }

int mdmsg_send(mdmsg_sender *sender, mdmsg *msg) {
    int err;

    err = mq_send(sender->qd, (const char*)msg, _mdmsg_size(msg), MDMSG_PRIO);

    if (err == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    return err;
}

#endif /* MDMSG_SENDER */

#ifdef MDMSG_RECEIVER

int mdmsg_start_receiver(mdmsg_receiver *receiver) {
    int            err;
    struct mq_attr attr;
    mode_t         old_mode;

    err = 0;

    mq_unlink(MDMSG_MQ_NAME);

    attr.mq_flags   = 0;
    attr.mq_maxmsg  = 10;
    attr.mq_msgsize = sizeof(mdmsg);
    attr.mq_curmsgs = 0;

    old_mode = umask(0);
    if ((receiver->qd = mq_open(MDMSG_MQ_NAME, O_RDONLY | O_CREAT | O_EXCL, 0666, &attr)) == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    umask(old_mode);
    return err;
}

int mdmsg_close_receiver(mdmsg_receiver *receiver) {
    int err;

    err = mq_close(receiver->qd);
    if (err == -1) {
        err   = -errno;
        errno = 0;
    }

    mq_unlink(MDMSG_MQ_NAME);

    return err;
}

int mdmsg_receive(mdmsg_receiver *receiver, mdmsg *msg) {
    int err;

    err = mq_receive(receiver->qd, (char*)msg, sizeof(*msg), NULL);

    if (err == -1) {
        err   = -errno;
        errno = 0;
    }

    return err;
}

int mdmsg_receive_or_timeout_ms(mdmsg_receiver *receiver, mdmsg *msg, uint64_t timeout_ms) {
    struct timespec ts;
    int             err;

    ts.tv_sec  = timeout_ms / 1000;
    ts.tv_nsec = 1000000 * (timeout_ms % 1000);

    err = mq_timedreceive(receiver->qd, (char*)msg, sizeof(*msg), NULL, &ts);

    if (err == -1) {
        err   = -errno;
        errno = 0;
    }

    return err;
}

#endif /* MDMSG_RECEIVER */
#endif /* MDMSG_IMPL */

#endif
