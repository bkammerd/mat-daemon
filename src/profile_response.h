#ifndef __PROFILE_RESPONSE_H__
#define __PROFILE_RESPONSE_H__

#include "array.h"

extern u64 total_mig_pages;
extern u64 total_mig_time;
extern u64 total_mig_count;

int init_profile_response(void);
int start_profile_response_thread(void);
int start_migration_thread(void);
int migration_thread_running(void);
void trigger_profile_response_thread(array_t *_collected_profile);

#endif
