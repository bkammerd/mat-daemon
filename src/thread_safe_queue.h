#ifndef __THREAD_SAFE_QUEUE_H__
#define __THREAD_SAFE_QUEUE_H__

#include "common.h"
#include "array.h"

#include <pthread.h>

typedef struct {
    pthread_mutex_t mtx;
    array_t         q;
} md_ts_queue;

md_ts_queue _thread_safe_queue_make(int elem_size);
void _thread_safe_queue_free(md_ts_queue *q);
void _thread_safe_queue_enqueue(md_ts_queue *q, void *elem_ptr);
int _thread_safe_queue_dequeue_keep_locked(md_ts_queue *q, void *dst_ptr);
int _thread_safe_queue_dequeue(md_ts_queue *q, void *dst_ptr);
int _thread_safe_queue_peek_keep_locked(md_ts_queue *q, void *dst_ptr);
int _thread_safe_queue_peek(md_ts_queue *q, void *dst_ptr);

#define thread_safe_queue_make(T) \
    (_thread_safe_queue_make(sizeof(T)))

#define thread_safe_queue_free(_q) \
    (_thread_safe_queue_free(&(_q))

#define thread_safe_queue_enqueue(_q, _elem) \
    (_thread_safe_queue_enqueue(&(_q), &(_elem)))

#define thread_safe_queue_dequeue_keep_locked(_q, _dst_ptr) \
    (_thread_safe_queue_dequeue_keep_locked(&(_q), (_dst_ptr)))

#define thread_safe_queue_dequeue(_q, _dst_ptr) \
    (_thread_safe_queue_dequeue(&(_q), (_dst_ptr)))

#define thread_safe_queue_peek_keep_locked(_q, _dst_ptr) \
    (_thread_safe_queue_peek_keep_locked(&(_q), (_dst_ptr)))

#define thread_safe_queue_peek(_q, _dst_ptr) \
    (_thread_safe_queue_peek(&(_q), (_dst_ptr)))

#endif
