#ifndef __PROFILING_PRIMITIVES_H__
#define __PROFILING_PRIMITIVES_H__

enum numa_node_id {
    DDR_NODE,
    PMM_NODE,
    NR_NUMA_NODES
};

enum md_event_type {
    MD_EVENT_LLC_MISS,
    MD_EVENT_PAGE_ALLOC,
    MD_EVENT_PAGE_RELEASE,
    NR_MD_EVENT_TYPES
};

#endif
