#include "profile_response.h"
#include "common.h"
#include "process.h"
#include "config.h"
#include "policy.h"
#include "policy.h"
#include "thread_safe_queue.h"
#include "tree.h"
#include "array.h"
#include "advice.h"

#include <stdio.h>
#include <pthread.h>
#include <stdatomic.h>
#include <unistd.h>
#include <time.h>
#include <numa.h>
#include <assert.h>
#include <inttypes.h>
#include <numaif.h>
#include <math.h>
#include <signal.h>


#define MIGRATE_DATA_INITIAL_CAP (16384)

typedef struct {
    pid_t   pid;
    array_t pages;
    array_t dest_nodes;
} md_bg_migrate_data;

typedef struct {
    u64 pro_bytes;
    u64 dem_bytes;
} Site_Advice;

use_tree(pid_t, md_bg_migrate_data);
use_tree(pid_t, md_object_map);
use_tree(u64, Site_Advice);
typedef tree(u64, Site_Advice) md_advice_map;
use_tree(pid_t, md_advice_map);

static pthread_t       profile_response_pthread;
static pthread_cond_t  profile_response_thread_cond     = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t profile_response_thread_mtx      = PTHREAD_MUTEX_INITIALIZER;
static atomic_int      profile_response_thread_running;
static atomic_int      _migration_thread_running;
static array_t         collected_profile;
static pthread_t       migration_pthread;
static pthread_cond_t  migration_cond            = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t migration_mtx             = PTHREAD_MUTEX_INITIALIZER;
static md_ts_queue     migration_queue;

u64 total_mig_pages;
u64 total_mig_time;
u64 total_mig_count;

float  pre_move_accs[NR_NUMA_NODES];
u64    pre_move_cap[NR_NUMA_NODES];
float  move_accs[NR_NUMA_NODES];
u64    move_cap[NR_NUMA_NODES];
u64    unknown_cap[NR_NUMA_NODES];

tree(pid_t, md_object_map) by_pid_object;

void init_migrate_stats() {
    pre_move_accs[config.tier_1_node]  = 0.0;
    pre_move_accs[config.tier_2_node]  = 0.0;
    pre_move_cap[config.tier_1_node]   = 0ul;
    pre_move_cap[config.tier_2_node]   = 0ul;
    unknown_cap[config.tier_1_node]    = 0ul;
    unknown_cap[config.tier_2_node]    = 0ul;

    move_accs[config.tier_1_node]      = 0.0;
    move_accs[config.tier_2_node]      = 0.0;
    move_cap[config.tier_1_node]       = 0ul;
    move_cap[config.tier_2_node]       = 0ul;
}

float get_cgroup_cur(int node) {
    long long numa_capacity;
    long long numa_free;

    numa_capacity = numa_node_size64(node, &numa_free);
    return numa_capacity - numa_free;

#if 0

    FILE *cgfile;
    char buf[512];
    unsigned long val;

    sprintf(buf, "/sys/fs/cgroup/1/memory.node%d_current", node);
    cgfile = fopen(buf, "r");
    if (cgfile == NULL) {
        INFO("could not open file: %s\n", buf);
        return -1.0;
    }

    fgets(buf, 512, cgfile);
    fclose(cgfile);

    val = strtoul(buf, NULL, 10);
    return (((float)val)/(1024.0*1024.0));
#endif
}

void print_migrate_stats() {
    float  total_accs;
    u64    total_cap;
    float  pre_accs_rat[NR_NUMA_NODES];
    float  pre_cap_rat[NR_NUMA_NODES];
    u64    pre_move_unknown_cap;
    float  pre_unknown_cap_rat;

    float  post_move_accs[NR_NUMA_NODES];
    u64    post_move_cap[NR_NUMA_NODES];
    float  post_accs_rat[NR_NUMA_NODES];
    float  post_cap_rat[NR_NUMA_NODES];

    total_accs = pre_move_accs[config.tier_1_node] +
                 pre_move_accs[config.tier_2_node];
    total_cap  = pre_move_cap[config.tier_1_node] +
                 pre_move_cap[config.tier_2_node] +
                 unknown_cap[config.tier_1_node] +
                 unknown_cap[config.tier_2_node];
    pre_move_unknown_cap = unknown_cap[config.tier_1_node] +
                           unknown_cap[config.tier_2_node];

    pre_accs_rat[config.tier_1_node] = (total_accs > 0.00001) ?
        pre_move_accs[config.tier_1_node] / total_accs : 0.0;
    pre_accs_rat[config.tier_2_node] = (total_accs > 0.00001) ?
        pre_move_accs[config.tier_2_node] / total_accs : 0.0;
    pre_cap_rat[config.tier_1_node] = (total_cap != 0) ?
        ((float)pre_move_cap[config.tier_1_node]) / total_cap : 0.0;
    pre_cap_rat[config.tier_2_node] = (total_cap != 0) ?
        ((float)pre_move_cap[config.tier_2_node]) / total_cap : 0.0;
    pre_unknown_cap_rat = (total_accs != 0) ?
        ((float)pre_move_unknown_cap) / total_cap : 0.0;

    post_move_accs[config.tier_1_node] = pre_move_accs[config.tier_1_node];
    post_move_accs[config.tier_2_node] = pre_move_accs[config.tier_2_node];

    post_move_accs[config.tier_1_node] -= move_accs[config.tier_2_node];
    post_move_accs[config.tier_1_node] += move_accs[config.tier_1_node];
    post_move_accs[config.tier_2_node] -= move_accs[config.tier_1_node];
    post_move_accs[config.tier_2_node] += move_accs[config.tier_2_node];

    post_move_cap[config.tier_1_node] = pre_move_cap[config.tier_1_node];
    post_move_cap[config.tier_2_node] = pre_move_cap[config.tier_2_node];

    post_move_cap[config.tier_1_node] -= move_cap[config.tier_2_node];
    post_move_cap[config.tier_1_node] += move_cap[config.tier_1_node];
    post_move_cap[config.tier_1_node] += unknown_cap[config.tier_1_node];
    post_move_cap[config.tier_2_node] -= move_cap[config.tier_1_node];
    post_move_cap[config.tier_2_node] += move_cap[config.tier_2_node];
    post_move_cap[config.tier_2_node] += unknown_cap[config.tier_2_node];

    post_accs_rat[config.tier_1_node] = (total_accs > 0.00001) ?
        post_move_accs[config.tier_1_node] / total_accs : 0.0;
    post_accs_rat[config.tier_2_node] = (total_accs > 0.00001) ?
        post_move_accs[config.tier_2_node] / total_accs : 0.0;
    post_cap_rat[config.tier_1_node] = (total_cap != 0) ?
        ((float)post_move_cap[config.tier_1_node]) / total_cap : 0.0;
    post_cap_rat[config.tier_2_node] = (total_cap != 0) ?
        ((float)post_move_cap[config.tier_2_node]) / total_cap : 0.0;

    fprintf(config.profile_output_file, "MIGRATE stats:\n");
    fprintf(config.profile_output_file,
            "  move_up:   %12.2f accs %12.2f MB (%12.2f MB unknown)\n",
               move_accs[config.tier_1_node],
               (((float)move_cap[config.tier_1_node])/(1024.0*1024.0)),
               (((float)unknown_cap[config.tier_1_node])/(1024.0*1024.0)));
    fprintf(config.profile_output_file,
            "  move_down: %12.2f accs %12.2f MB (%12.2f MB unknown)\n\n",
               move_accs[config.tier_2_node],
               (((float)move_cap[config.tier_2_node])/(1024.0*1024.0)),
               (((float)unknown_cap[config.tier_2_node])/(1024.0*1024.0)));

    fprintf(config.profile_output_file,
            "  pre_DDR:   %12.2f accs %12.2f MB (%3.4f %3.4f)\n",
               pre_move_accs[config.tier_1_node],
               (((float)pre_move_cap[config.tier_1_node])/(1024.0*1024.0)),
               pre_accs_rat[config.tier_1_node],
               pre_cap_rat[config.tier_1_node]);
    fprintf(config.profile_output_file,
            "  pre_PMM:   %12.2f accs %12.2f MB (%3.4f %3.4f)\n",
               pre_move_accs[config.tier_2_node],
               (((float)pre_move_cap[config.tier_2_node])/(1024.0*1024.0)),
               pre_accs_rat[config.tier_2_node],
               pre_cap_rat[config.tier_2_node]);
    fprintf(config.profile_output_file,
            "  pre_UNK:                     %12.2f MB (%3.4f)\n",
               (((float)pre_move_unknown_cap)/(1024.0*1024.0)),
               pre_unknown_cap_rat);

    fprintf(config.profile_output_file,
            "  post_DDR:  %12.2f accs %12.2f MB (%3.4f %3.4f)\n",
               post_move_accs[config.tier_1_node],
               (((float)post_move_cap[config.tier_1_node])/(1024.0*1024.0)),
               post_accs_rat[config.tier_1_node],
               post_cap_rat[config.tier_1_node]);
    fprintf(config.profile_output_file,
            "  post_PMM:  %12.2f accs %12.2f MB (%3.4f %3.4f)\n\n",
               post_move_accs[config.tier_2_node],
               (((float)post_move_cap[config.tier_2_node])/(1024.0*1024.0)),
               post_accs_rat[config.tier_2_node],
               post_cap_rat[config.tier_2_node]);

    fprintf(config.profile_output_file,
            "  act_DDR:   %12.2f MB\n  act_PMM:   %12.f MB\n\n",
               get_cgroup_cur(0), get_cgroup_cur(1));
    fflush(config.profile_output_file);

    INFO("MOVING UP:   %12.2f accs %12.2f MB (%12.2f MB unknown)\n",
         move_accs[config.tier_1_node],
         (((float)move_cap[config.tier_1_node])/(1024.0*1024.0)),
         (((float)unknown_cap[config.tier_1_node])/(1024.0*1024.0)));
    INFO("MOVING DOWN: %12.2f accs %12.2f MB (%12.2f MB unknown)\n",
         move_accs[config.tier_2_node],
         (((float)move_cap[config.tier_2_node])/(1024.0*1024.0)),
         (((float)unknown_cap[config.tier_2_node])/(1024.0*1024.0)));
}

int init_profile_response(void) {
    migration_queue = thread_safe_queue_make(md_bg_migrate_data*);

    return 0;
}

static void free_profile(array_t prof) {
    array_free(prof);
}

static void enqueue_migration(md_bg_migrate_data *migrate_data) {
    thread_safe_queue_enqueue(migration_queue, migrate_data);
}

static md_bg_migrate_data *dequeue_migration(void) {
    md_bg_migrate_data *migrate_data;

    migrate_data = NULL;

    if (!thread_safe_queue_dequeue(migration_queue, &migrate_data)) {
        return NULL;
    }

    return migrate_data;
}

static void filter_objects(void) {
    u64        now;
    md_object *obj;

    now = measure_time_now_ns();

    /* Filter out objects based on config options. */
again:;
    array_traverse(collected_profile, obj) {
        /* Is the object too young? */
        if (now - obj->creation_time_ns < (config.migration_min_age_ms * 1000000UL)) {
            array_delete(collected_profile, obj - ((md_object*)array_data(collected_profile)));
            goto again;
        }
    }
}

static void background_migrate_pages(md_bg_migrate_data migrate_data) {
    md_bg_migrate_data *new;

    new = malloc(sizeof(*new));
    memcpy(new, &migrate_data, sizeof(*new));

    enqueue_migration(new);
}

static tree(pid_t, md_advice_map) advice_by_pid;

static void try_send_advice_to_allocator(array_t *promote, array_t *demote) {
#if OBJECT_ORIGIN

    md_proc                        *proc;
    md_object                     **obj_it;
    md_object                      *obj;
    tree_it(pid_t, md_advice_map)   adv_map_it;
    tree_it(u64, Site_Advice)       adv_it;
    Site_Advice                     new_advice;
    mdadv_sender                   *q;
    mdadv                           advice_msg;
    double                          cold_ratio;


    if (advice_by_pid == NULL) {
        advice_by_pid = tree_make(pid_t, md_advice_map);
    }

    PROCESS_FOR(proc, {
        adv_map_it = tree_lookup(advice_by_pid, proc->pid);
        if (tree_it_good(adv_map_it)) {
            tree_traverse(tree_it_val(adv_map_it), adv_it) {
                tree_it_val(adv_it).pro_bytes = 0;
                tree_it_val(adv_it).dem_bytes = 0;
            }
        } else {
            tree_insert(advice_by_pid, proc->pid, tree_make(u64, Site_Advice));
        }
    });

    array_traverse(*demote, obj_it) {
        obj        = *obj_it;
        adv_map_it = tree_lookup(advice_by_pid, obj->pid);
        if (!tree_it_good(adv_map_it)) { continue; }

        adv_it = tree_lookup(tree_it_val(adv_map_it), obj->origin_inst_addr);
        if (tree_it_good(adv_it)) {
            tree_it_val(adv_it).dem_bytes += obj->end - obj->start;
        } else {
            new_advice = (Site_Advice){ 0, obj->end - obj->start };
            adv_it     = tree_insert(tree_it_val(adv_map_it), obj->origin_inst_addr, new_advice);
        }
    }
    array_traverse(*promote, obj_it) {
        obj        = *obj_it;
        adv_map_it = tree_lookup(advice_by_pid, obj->pid);
        if (!tree_it_good(adv_map_it)) { continue; }

        adv_it = tree_lookup(tree_it_val(adv_map_it), obj->origin_inst_addr);
        if (tree_it_good(adv_it)) {
            tree_it_val(adv_it).pro_bytes += obj->end - obj->start;
        } else {
            new_advice = (Site_Advice){ obj->end - obj->start, 0 };
            adv_it     = tree_insert(tree_it_val(adv_map_it), obj->origin_inst_addr, new_advice);
        }
    }

    PROCESS_FOR(proc, {
        lock_process(proc);

        q = &proc->advice_queue;

        if (q->qd <= 0) {
            mdadv_start_sender(q, proc->pid);
        }

        unlock_process(proc);

        if (q->qd <= 0) { break; }

        adv_map_it = tree_lookup(advice_by_pid, proc->pid);
        if (!tree_it_good(adv_map_it)) { continue; }

        tree_traverse(tree_it_val(adv_map_it), adv_it) {
            cold_ratio =   ((double)tree_it_val(adv_it).dem_bytes)
                         / (((double)tree_it_val(adv_it).dem_bytes) + ((double)tree_it_val(adv_it).pro_bytes));

            advice_msg.inst_addr = tree_it_key(adv_it);
            advice_msg.advice    = cold_ratio >= config.advice_cold_threshold;
            mdadv_send(q, &advice_msg);
        }
    });


#endif /* OBJECT_ORIGIN */
}

static void respond_to_profile(void) {
    md_proc                             *proc;
    array_t                              promote;
    array_t                              demote;
    tree(pid_t, md_bg_migrate_data)      by_pid;
    tree_it(pid_t, md_bg_migrate_data)   it;
    tree_it(pid_t, md_object_map)        oit;
    tree_it(u64, md_object)              rit;
    md_bg_migrate_data                   migrate_data;
    md_object                          **object_to_move;
    md_object                           *orig_obj;
    u64                                  addr;

    u64   dem_bytes     = 0;
    u64   pmm_dem_bytes = 0;
    float dem_acc       = 0;
    float pmm_dem_acc   = 0;
    u64   pro_bytes     = 0;
    u64   pmm_pro_bytes = 0;
    float pro_acc       = 0;
    float pmm_pro_acc   = 0;


    filter_objects();

    PROCESS_FOR(proc, proc->mdtop_did_migrate = 1;);

    policy_ranking_get_ranked_objects(&collected_profile);

    promote = array_make_with_cap(md_object*, array_len(collected_profile));
    demote  = array_make_with_cap(md_object*, array_len(collected_profile));
    policy_packing_get_objects_to_migrate(&collected_profile, &promote, &demote);

    pthread_mutex_lock(&migration_mtx);

    if (config.print_migrate_stats) {
        init_migrate_stats();
    }

    by_pid = tree_make(pid_t, md_bg_migrate_data);
    if (by_pid_object) {
        tree_traverse(by_pid_object, oit) {
            tree_free(tree_it_val(oit));
        }
        tree_free(by_pid_object);
    }
    by_pid_object = tree_make(pid_t, md_object_map);

    array_traverse(demote, object_to_move) {
        dem_bytes += (*object_to_move)->end - (*object_to_move)->start;
        dem_acc   += (*object_to_move)->llc_misses_scaled[config.tier_1_node] + (*object_to_move)->llc_misses_scaled[config.tier_2_node];
        if ((*object_to_move)->hid == 1) {
            pmm_dem_bytes += (*object_to_move)->end - (*object_to_move)->start;
            pmm_dem_acc   += (*object_to_move)->llc_misses_scaled[config.tier_1_node] + (*object_to_move)->llc_misses_scaled[config.tier_2_node];
        }

        if (config.print_migrate_stats) {
            pre_move_accs[config.tier_1_node] += (*object_to_move)->llc_misses_scaled[config.tier_1_node];
            pre_move_accs[config.tier_2_node] += (*object_to_move)->llc_misses_scaled[config.tier_2_node];
            if ((*object_to_move)->current_node == -1) {
                unknown_cap[config.tier_2_node] += get_object_capacity_estimate((*object_to_move));
            } else {
                pre_move_cap[(*object_to_move)->current_node] += get_object_capacity_estimate((*object_to_move));
            }
        }

        if ((*object_to_move)->current_node == config.tier_2_node) { continue; }

        if (config.print_migrate_stats) {
            move_accs[config.tier_2_node] += (*object_to_move)->llc_misses_scaled[config.tier_1_node];
            if ((*object_to_move)->current_node != -1) {
                move_cap[config.tier_2_node] += get_object_capacity_estimate((*object_to_move));
            }
        }

        it = tree_lookup(by_pid, (*object_to_move)->pid);

        if (!tree_it_good(it)) {
            migrate_data.pid        = (*object_to_move)->pid;
            migrate_data.pages      = array_make_with_cap(void*, MIGRATE_DATA_INITIAL_CAP);
            migrate_data.dest_nodes = array_make_with_cap(int,   MIGRATE_DATA_INITIAL_CAP);

            it = tree_insert(by_pid, (*object_to_move)->pid, migrate_data);
        }

        for (addr = (*object_to_move)->start; addr < (*object_to_move)->end; addr += PAGE_SIZE) {
            array_push(tree_it_val(it).pages,      addr);
            array_push(tree_it_val(it).dest_nodes, config.tier_2_node);
        }

        oit = tree_lookup(by_pid_object, (*object_to_move)->pid);
        if(!tree_it_good(oit)) {
            oit = tree_insert(by_pid_object, (*object_to_move)->pid,
                              tree_make(u64, md_object));
        }
        (*object_to_move)->current_node = config.tier_2_node;
        tree_insert(tree_it_val(oit), (*object_to_move)->start, (**object_to_move));
    }

    tree_traverse(by_pid, it) {
        background_migrate_pages(tree_it_val(it));
    }

    tree_free(by_pid);

    by_pid = tree_make(pid_t, md_bg_migrate_data);

    array_traverse(promote, object_to_move) {
/*         if ((*object_to_move)->preassigned_pmm) { continue; } */

        pro_bytes += (*object_to_move)->end - (*object_to_move)->start;
        pro_acc   += (*object_to_move)->llc_misses_scaled[config.tier_1_node] + (*object_to_move)->llc_misses_scaled[config.tier_2_node];
        if ((*object_to_move)->hid == 1) {
            pmm_pro_bytes += (*object_to_move)->end - (*object_to_move)->start;
            pmm_pro_acc   += (*object_to_move)->llc_misses_scaled[config.tier_1_node] + (*object_to_move)->llc_misses_scaled[config.tier_2_node];
        }

        pre_move_accs[config.tier_1_node] += (*object_to_move)->llc_misses_scaled[config.tier_1_node];
        pre_move_accs[config.tier_2_node] += (*object_to_move)->llc_misses_scaled[config.tier_2_node];
        if ((*object_to_move)->current_node == -1) {
            unknown_cap[config.tier_1_node] += get_object_capacity_estimate((*object_to_move));
        } else {
            pre_move_cap[(*object_to_move)->current_node] += get_object_capacity_estimate((*object_to_move));
        }

        if ((*object_to_move)->current_node == config.tier_1_node) { continue; }

        move_accs[config.tier_1_node] += (*object_to_move)->llc_misses_scaled[config.tier_2_node];
        if ((*object_to_move)->current_node != -1) {
            move_cap[config.tier_1_node] += get_object_capacity_estimate((*object_to_move));
        }

        it = tree_lookup(by_pid, (*object_to_move)->pid);

        if (!tree_it_good(it)) {
            migrate_data.pid        = (*object_to_move)->pid;
            migrate_data.pages      = array_make_with_cap(void*, MIGRATE_DATA_INITIAL_CAP);
            migrate_data.dest_nodes = array_make_with_cap(int,   MIGRATE_DATA_INITIAL_CAP);

            it = tree_insert(by_pid, (*object_to_move)->pid, migrate_data);
        }

        for (addr = (*object_to_move)->start; addr < ((*object_to_move)->end); addr += PAGE_SIZE) {
            array_push(tree_it_val(it).pages,      addr);
            array_push(tree_it_val(it).dest_nodes, config.tier_1_node);
        }

        oit = tree_lookup(by_pid_object, (*object_to_move)->pid);
        if(!tree_it_good(oit)) {
            oit = tree_insert(by_pid_object, (*object_to_move)->pid,
                              tree_make(u64, md_object));
        }
        (*object_to_move)->current_node = config.tier_1_node;
        tree_insert(tree_it_val(oit), (*object_to_move)->start, (**object_to_move));
    }

    tree_traverse(by_pid, it) {
        background_migrate_pages(tree_it_val(it));
    }

    if (config.print_migrate_stats) {
        print_migrate_stats();
    }

    PROCESS_FOR(proc, {
        lock_process(proc);
        oit = tree_lookup(by_pid_object, proc->pid);
        if (tree_it_good(oit)) {
            tree_traverse(tree_it_val(oit), rit) {
                orig_obj = map_get_object(proc->object_map, tree_it_key(rit));
                if (orig_obj) {
                    orig_obj->current_node          = tree_it_val(rit).current_node;
                    orig_obj->last_migrate_time_ns  = measure_time_now_ns();
                    orig_obj->mig_count            += 1;
                }
            }
        }
        proc->obj_allocs_since_last_migration = 0;
        unlock_process(proc);
    });

    fprintf(mig_stats,
            "DIV %lu %lu %f %f %lu %lu %f %f\n",
            pro_bytes,
            pmm_pro_bytes,
            pro_acc,
            pmm_pro_acc,
            dem_bytes,
            pmm_dem_bytes,
            dem_acc,
            pmm_dem_acc);

    if (config.advise_initial_placement) {
        try_send_advice_to_allocator(&promote, &demote);
    }

    total_mig_count += 1;

    array_free(demote);
    array_free(promote);

    pthread_cond_signal(&migration_cond);
    pthread_mutex_unlock(&migration_mtx);
}

static void *profile_response_thread(void *arg) {
    u64 start;
    u64 elapsed;

    for (;;) {
        pthread_mutex_lock(&profile_response_thread_mtx);
        pthread_cond_wait(&profile_response_thread_cond, &profile_response_thread_mtx);
        pthread_mutex_unlock(&profile_response_thread_mtx);

        profile_response_thread_running = 1;
        start = measure_time_now_ms();

        respond_to_profile();

        free_profile(collected_profile);

        elapsed = measure_time_now_ms() - start;
        (void)elapsed;
        profile_response_thread_running = 0;
    }
    return NULL;
}

static void *migration_thread(void *arg) {
    md_bg_migrate_data *migrate_data;
    u64                 num_pages;
    int                *status;
    u64                 t, start, init, elapsed;
    int                 i;
    int                 already_sleeping;
    pid_t              *sp;
    md_object          *obj;
    u64                 total;
    array_t             sleeping_pids;
    md_object           val;

    tree_it(pid_t, md_object_map) it;
    tree_it(u64, md_object)       oit;

    if (config.print_migrate_stats) {
        init = measure_time_now_ms();
    }

    for (;;) {
        pthread_mutex_lock(&migration_mtx);
        pthread_cond_wait(&migration_cond, &migration_mtx);

        if (config.stop_app_during_migrations) {
            _migration_thread_running = 1;
        }

        if (config.stop_app_during_migrations) {
            sleeping_pids = array_make(pid_t);
        }

        if (config.print_migrate_stats) {
            start = measure_time_now_ms();
            fprintf(config.profile_output_file,
                    "MIGRATE: move_pages start:   %12lu\n", start-init);
            fflush(config.profile_output_file);
        }
        while ((migrate_data = dequeue_migration()) != NULL) {
            /* move_pages */
            num_pages = array_len(migrate_data->pages);
            status    = malloc(sizeof(int) * num_pages);
            memset(status, 123, sizeof(*status) * num_pages);

            if (config.stop_app_during_migrations) {
                already_sleeping = 0;
                array_traverse(sleeping_pids, sp) {
                    if ((*sp) == migrate_data->pid) {
                        already_sleeping = 1;
                    }
                }
                if (!already_sleeping) {
                    kill(migrate_data->pid, SIGSTOP);
                    array_push(sleeping_pids, migrate_data->pid);
                }
            }

            t = measure_time_now_ns();

            move_pages(migrate_data->pid,
                       num_pages,
                       (void**)array_data(migrate_data->pages),
                       (int*)array_data(migrate_data->dest_nodes),
                       status,
                       MPOL_MF_MOVE_ALL);

            total_mig_time  += measure_time_now_ns() - t;
            total_mig_pages += num_pages;

            if (config.print_migrate_stats) {
                for (i = 0; i < num_pages; i++) {
                    if(status[i] < 0) {
                        it = tree_lookup(by_pid_object, migrate_data->pid);
                        if (!tree_it_good(it)) {
                            fprintf(config.profile_output_file,
                                    "bad process: %d\n", migrate_data->pid);
                            continue;
                        }
                        obj = map_get_object(tree_it_val(it),
                                             ((u64)((void**)(array_data(migrate_data->pages)))[i]));

                        if (!obj) {
                            fprintf(config.profile_output_file,
                                    "no object for page: %d %lx\n",
                                    migrate_data->pid,
                                    ((u64)((void**)(array_data(migrate_data->pages)))[i]));
                            continue;
                        }
                        obj->mig_err += 1;
                    }
                }
            }

            free(status);
            array_free(migrate_data->dest_nodes);
            array_free(migrate_data->pages);
            free(migrate_data);
        }

        if (config.print_migrate_stats) {
            elapsed = (measure_time_now_ms() - start);
            fprintf(config.profile_output_file,
                    "MIGRATE: move_pages elapsed: %12lu\n", elapsed);
            total = 0;
            tree_traverse(by_pid_object, it) {
                tree_traverse(tree_it_val(it), oit) {
                    val = tree_it_val(oit);
                    if (val.mig_err > 0) {
                        total += val.mig_err;
                    }
                }
            }
            fprintf(config.profile_output_file,
                    "  unmoved total:  %12lu %12.2f MB\n",
                    total, (((float)(total*4096.0))/(1024.0*1024.0)));
            fflush(config.profile_output_file);
            fprintf(config.profile_output_file,
                    "  act_DDR:   %12.2f MB\n  act_PMM: %12.f MB\n\n",
                       get_cgroup_cur(0), get_cgroup_cur(1));
        }

        if (config.stop_app_during_migrations) {
            array_traverse(sleeping_pids, sp) {
                kill((*sp), SIGCONT);
            }
            array_free(sleeping_pids);
            _migration_thread_running = 0;
        }
        pthread_mutex_unlock(&migration_mtx);
    }

    return NULL;
}

int start_migration_thread(void) {
    int status;
    status = pthread_create(&migration_pthread, NULL, migration_thread, NULL);
    if (status != 0) {
        WARN("failed to create migration thread\n");
    }
    return status;
}

int start_profile_response_thread(void) {
    int status;
    status = pthread_create(&profile_response_pthread, NULL, profile_response_thread, NULL);
    if (status != 0) {
        WARN("failed to create profile_response thread\n");
    }
    return status;
}

int migration_thread_running (void) {
  return _migration_thread_running;
}

void trigger_profile_response_thread(array_t *_collected_profile) {
    if (array_len(*_collected_profile) == 0
    ||  !policy_trigger_should_migrate()) {
        free_profile(*_collected_profile);
        return;
    }

    if (profile_response_thread_running) {
        WARN("profile_response thread is too slow to react to profiling\n");
        free_profile(*_collected_profile);
        return;
    }

    collected_profile = *_collected_profile;

    pthread_mutex_lock(&profile_response_thread_mtx);
    pthread_cond_signal(&profile_response_thread_cond);
    pthread_mutex_unlock(&profile_response_thread_mtx);
}
