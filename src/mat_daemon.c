#include "config.h"
#include "process.h"
#include "msg.h"
#include "profiling.h"
#include "policy.h"
#include "cleanup.h"
#include "mdtop.h"
#include "profile_response.h"
#include "object_origin.h"
#include "sig.h"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

const char *argv0;
FILE *mig_stats;

int main(int argc, const char **argv) {
    argv0 = argv[0];

    if (init_config(argc, argv)          != 0) { goto init_err; }

    printf("mat-daemon -- the Memory Auto-Tiering Daemon\n\n");

    show_config();

    INFO("Initializing...\n");

    init_cleanup();

#if OBJECT_ANALYSIS
    object_analysis.t_start             = measure_time_now_ms();
    object_analysis.mapped_err.min      =
    object_analysis.mapped_file_err.min =
    object_analysis.faults_err.min      = 99999999.9999;
#endif


    mig_stats = fopen("mig_stats.txt", "w");


    if (init_sig())                                      { goto init_err; }

    if (config.increase_num_fd_rlimit
    &&  bump_nofile_rlimit())                            { goto init_err; }

    if (init_profiling() != 0)                           { goto init_err; }
        INFO("Initialized profiling.\n");
    if (init_process_table()             != 0)           { goto init_err; }
        INFO("Created process table.\n");
#if OBJECT_ORIGIN_DETAIL
    if (start_object_origin_parsing_thread() != 0)       { goto init_err; }
        INFO("Started object origin parsing thread.\n");
#endif
    if (init_messages()                  != 0)           { goto init_err; }
        INFO("Opened message queue.\n");
    if (start_message_threads() != 0)                    { goto init_err; }
        INFO("Started message threads.\n");
    if (start_profiling_thread() != 0)                   { goto init_err; }
        INFO("Started profiling thread.\n");
    if (init_policies()                  != 0)           { goto init_err; }
    if (init_profile_response()          != 0)           { goto init_err; }
    if (start_migration_thread()         != 0)           { goto init_err; }
        INFO("Started migration thread.\n");
    if (start_profile_response_thread()  != 0)           { goto init_err; }
        INFO("Started profile response thread.\n");

    printf("Everything is initialized.\n\n");

    if (!config.mdtop) {
        printf("Waiting for messages...\n");
    }

    for (;;) {
        if (config.mdtop) {
            mdtop_update();
        } else {
            bpf_print();
            fflush(stdout);
            fflush(stderr);
            sleep(1);
        }
    }

    return 0;

init_err:;
    ERR("failed to initialize mat-daemon\n");
    return 1;
}
