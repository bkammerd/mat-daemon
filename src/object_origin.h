#ifndef __OBJECT_ORIGIN_H__
#define __OBJECT_ORIGIN_H__

#if OBJECT_ORIGIN_DETAIL

#include "common.h"

struct md_object_t;

typedef struct {
    char *func_name;
    char *file_name;
    u64   line_number;
} md_object_origin;

int  start_object_origin_parsing_thread(void);
void object_origin_get_async(struct md_object_t *obj);
void object_origin_cancel_if_in_flight(struct md_object_t *obj);

#endif

#endif
