#include "policy.h"
#include "object.h"
#include "config.h"
#include "profiling.h"
#include <math.h>
#include <numa.h>

static md_policy policy;

#define BYTES_IN_RANGE(obj_info_ptr) \
    ((obj_info_ptr)->end - (obj_info_ptr)->start)

#define RESIDENT_BYTES_IN_OBJECT(obj_info_ptr) \
    ((obj_info_ptr)->n_resident_pages * 4096)

#define ACCESSES_PER_BYTE(obj_info_ptr) \
    ( sum_float_array(obj_info_ptr->accs_scaled, MAX_PROFILE_EVENTS) / \
      (float)BYTES_IN_RANGE(obj_info_ptr))

#define ACCESSES_PER_RESIDENT_BYTE(obj_info_ptr) \
    ( sum_float_array(obj_info_ptr->accs_scaled, MAX_PROFILE_EVENTS) / \
      (float)RESIDENT_BYTES_IN_OBJECT(obj_info_ptr))


int load_trigger_policy(const char *path) {
    void  *handle;
    void (*add_options)(void);

    if (policy.trigger != NULL) {
        ERR_NOEXIT("policy-trigger-path: trigger policy already loaded!\n");
        return 0;
    }

    /* @bad @todo: just leak the handle */

    handle = dlopen(path, RTLD_NOW);
    if (handle) {
        policy.trigger = dlsym(handle, "md_policy_trigger_should_migrate");
        if (policy.trigger == NULL) {
            ERR_NOEXIT("policy-trigger-path: %s\n", dlerror());
            return 0;
        }
        add_options = dlsym(handle, "md_policy_add_options");
        if (add_options != NULL) {
            add_options();
        }
    } else {
        ERR_NOEXIT("policy-trigger-path: %s\n", dlerror());
        return 0;
    }

    return 1;
}

int load_ranking_policy(const char *path) {
    void  *handle;
    void (*add_options)(void);

    if (policy.rank != NULL) {
        ERR_NOEXIT("policy-ranking-path: ranking policy already loaded!\n");
        return 0;
    }

    /* @bad @todo: just leak the handle */

    handle = dlopen(path, RTLD_NOW);
    if (handle) {
        policy.rank = dlsym(handle, "md_policy_ranking_get_ranked_objects");
        if (policy.rank == NULL) {
            ERR_NOEXIT("policy-ranking-path: %s\n", dlerror());
            return 0;
        }
        add_options = dlsym(handle, "md_policy_add_options");
        if (add_options != NULL) {
            add_options();
        }
    } else {
        ERR_NOEXIT("policy-ranking-path: %s\n", dlerror());
        return 0;
    }

    return 1;
}

int load_packing_policy(const char *path) {
    void  *handle;
    void (*add_options)(void);

    if (policy.pack != NULL) {
        ERR_NOEXIT("policy-packing-path: packing policy already loaded!\n");
        return 0;
    }

    /* @bad @todo: just leak the handle */

    handle = dlopen(path, RTLD_NOW);
    if (handle) {
        policy.pack = dlsym(handle, "md_policy_packing_get_objects_to_migrate");
        if (policy.pack == NULL) {
            ERR_NOEXIT("policy-packing-path: %s\n", dlerror());
            return 0;
        }
        add_options = dlsym(handle, "md_policy_add_options");
        if (add_options != NULL) {
            add_options();
        }
    } else {
        ERR_NOEXIT("policy-packing-path: %s\n", dlerror());
        return 0;
    }

    return 1;
}

int load_combined_policy(const char *path) {
    void  *handle;
    void (*add_options)(void);

    if (policy.trigger != NULL) {
        ERR_NOEXIT("policy-combined-path: trigger policy already loaded!\n");
        return 0;
    }
    if (policy.rank != NULL) {
        ERR_NOEXIT("policy-combined-path: ranking policy already loaded!\n");
        return 0;
    }
    if (policy.pack != NULL) {
        ERR_NOEXIT("policy-combined-path: packing policy already loaded!\n");
        return 0;
    }

    /* @bad @todo: just leak the handle */

    handle = dlopen(path, RTLD_NOW);
    if (handle) {
        policy.trigger = dlsym(handle, "md_policy_trigger_should_migrate");
        if (policy.trigger == NULL) {
            ERR_NOEXIT("policy-combined-path: %s\n", dlerror());
            return 0;
        }
        policy.rank = dlsym(handle, "md_policy_ranking_get_ranked_objects");
        if (policy.rank == NULL) {
            ERR_NOEXIT("policy-combined-path: %s\n", dlerror());
            return 0;
        }
        policy.pack = dlsym(handle, "md_policy_packing_get_objects_to_migrate");
        if (policy.pack == NULL) {
            ERR_NOEXIT("policy-combined-path: %s\n", dlerror());
            return 0;
        }
        add_options = dlsym(handle, "md_policy_add_options");
        if (add_options != NULL) {
            add_options();
        } else {
            INFO("!!! %s\n", dlerror());
        }
    } else {
        ERR_NOEXIT("policy-combined-path: %s\n", dlerror());
        return 0;
    }

    return 1;
}


int init_policies(void) {
    int status;

    status = 0;

    if (policy.trigger == NULL) {
        ERR_NOEXIT("no trigger policy loaded\n");
        status = 1;
    }
    if (policy.rank == NULL) {
        ERR_NOEXIT("no ranking policy loaded\n");
        status = 1;
    }
    if (policy.pack == NULL) {
        ERR_NOEXIT("no packing policy loaded\n");
        status = 1;
    }

    return status;
}

int policy_trigger_should_migrate(void) {
    return policy.trigger();
}
void policy_ranking_get_ranked_objects(array_t *collected_profile) {
/*     md_object *obj; */
/*     int        n; */
/*     double     accs_scaled; */
/*     u64        pres; */
/*     double     apb; */

    policy.rank(collected_profile);

/*     INFO("---------------------------------------\n"); */
/*     array_traverse(*collected_profile, obj) { */
/*         accs_scaled = pres = 0; */
/*         for (n = 0; n < NR_NUMA_NODES; n++) { */
/*             accs_scaled += 1024.0 * 1024.0 * obj->llc_misses_scaled[n]; */
/*         } */
/*         pres = get_object_capacity_estimate(obj); */
/*         apb = ( pres != 0 ) ? ( accs_scaled / pres ) : 0.0; */
/*  */
/*         INFO("  accs_scaled=%f  pres=%lu  apb=%f\n", accs_scaled, pres, apb); */
/*     } */
}
void policy_packing_get_objects_to_migrate(array_t *ranked, array_t *out_promote, array_t *out_demote) {
    policy.pack(ranked, out_promote, out_demote);
}
