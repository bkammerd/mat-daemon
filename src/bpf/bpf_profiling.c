#include "../profiling.h"
#include "../common.h"
#include "../tree.h"
#include "../array.h"
#include "../object.h"
#include "../process.h"
#include "../config.h"
#include "../profile_response.h"
#include "bpf_profiling.h"
#include "mat_daemon.skel.h"
#include "bpf_common.h"
#include "bpf_trace_helpers.h"
#include "bpf_map_helpers.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <asm/unistd.h>
#include <linux/perf_event.h>
#include <pthread.h>
#include <perfmon/pfmlib_perf_event.h>
#include <sys/mman.h>

#define LINE_SIZE (512)

use_tree(pid_t, array_t);

static int nr_cpus;
static int bpf_md_procs_fd;
static pthread_mutex_t bpf_events_mtx = PTHREAD_MUTEX_INITIALIZER;
static struct ring_buffer *bpf_event_rb;
static pthread_t bpf_polling_pthread;

static tree(pid_t, array_t) bpf_events;
static tree(pid_t, array_t) bpf_events_working;

static FILE *trace_pipe;

int handle_bpf_event (void *ctx, void *data, size_t data_sz) {
    struct md_event evt;
    tree_it(pid_t, array_t) it;
    array_t new_array;

    memcpy(&evt, ((struct md_event*)data), sizeof(struct md_event));
    it = tree_lookup(bpf_events, evt.pid);
    if (!tree_it_good(it)) {
        new_array = array_make(struct md_event);
        it = tree_insert(bpf_events, evt.pid, new_array);
    }
    array_push(tree_it_val(it), evt);

    return 0;
}

int bpf_poll_ring_buffer() {
    int err;

    pthread_mutex_lock(&bpf_events_mtx);
    if (bpf_events == NULL) {
        bpf_events = tree_make(pid_t, array_t);
    }
    err = ring_buffer__poll(bpf_event_rb, config.bpf_event_poll_period_ms);
    pthread_mutex_unlock(&bpf_events_mtx);

    return err;
}

static void *bpf_poll_thread(void *args) {
    int  err;

    for (;;) {
        err = bpf_poll_ring_buffer();
        if (err < 0) {
            printf("Error polling ring buffer: %d\n", err);
        }
        /* Give the profiling thread a chance to grab the lock. */
        usleep(config.bpf_event_poll_period_ms * 1000);
    }
}

static int open_and_attach_perf_event(struct perf_event_attr *attr,
				struct bpf_program *prog,
				struct bpf_link *links[])
{
	int i, fd;

  fd = -1;

	for (i = 0; i < nr_cpus; i++) {
		fd = syscall(__NR_perf_event_open, attr, -1, i, -1, 0);
		if (fd < 0) {
			WARN("failed to init perf sampling: %s\n", strerror(errno));
			return -1;
		}
		links[i] = bpf_program__attach_perf_event(prog, fd);
		if (libbpf_get_error(links[i])) {
			WARN("failed to attach perf event on cpu: %d\n", i);
			links[i] = NULL;
			close(fd);
			return -1;
		}
	}

	return fd;
}

static int get_perf_event_attr(const char *event_str, struct perf_event_attr *pe) {
    int                   err;
    pfm_perf_encode_arg_t pfm;

    memset(pe, 0, sizeof(struct perf_event_attr));
    memset(&pfm, 0, sizeof(pfm_perf_encode_arg_t));

    /* perf_event_open */
    pe->size = sizeof(struct perf_event_attr);
    pe->exclude_kernel = 1;
    pe->exclude_hv     = 1;
    pe->precise_ip     = 2;
    pe->task           = 1;
    pe->use_clockid    = 1;
    pe->clockid        = CLOCK_MONOTONIC_RAW;
    pe->sample_period  = config.profile_overflow_thresh;
    pe->disabled       = 1;

    pe->sample_type  = PERF_SAMPLE_TID;
    pe->sample_type |= PERF_SAMPLE_TIME;
    pe->sample_type |= PERF_SAMPLE_ADDR;
/*     pe->sample_type |= PERF_SAMPLE_PHYS_ADDR; */

    pfm.size = sizeof(pfm_perf_encode_arg_t);
    pfm.attr = pe;

    err = pfm_get_os_event_encoding(event_str, PFM_PLM2 | PFM_PLM3,
        PFM_OS_PERF_EVENT, &pfm);

    if (err != PFM_SUCCESS) {
        WARN("Couldn't find an appropriate event to use. (error %d) Aborting.\n", err);
        return EINVAL;
    }

    return 0;
}

static int open_and_load_mat_daemon(struct mat_daemon_bpf **obj) {
    int err;

    (*obj) = mat_daemon_bpf__open();
    if (!(*obj)) {
        ERR("failed to open BPF object\n");
    }

    err = mat_daemon_bpf__load((*obj));
    if (err) {
        WARN("failed to load BPF object: %d\n", err);
        goto cleanup;
    }

    err = mat_daemon_bpf__attach((*obj));
    if (err) {
        WARN("failed to attach BPF object: %d\n", err);
        goto cleanup;
    }

    return 0;

cleanup:
    mat_daemon_bpf__destroy((*obj));
    (*obj) = NULL;
    return err != 0;
}


static void init_bpf_print(void) {
    (void)trace_pipe;
#if BPF_PRINTK
    // for debugging with bpf_printk
    int trace_fd, flags;

    trace_pipe = fopen("/sys/kernel/debug/tracing/trace_pipe", "r");
    trace_fd = fileno(trace_pipe);
    flags = fcntl(trace_fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(trace_fd, F_SETFL, flags);
#endif
}

int init_bpf_profiling(void) {
    int                      i;
    int                      err;
    struct mat_daemon_bpf   *obj;
    struct perf_event_attr   pe;
    struct bpf_link        **ddr_links;
    struct bpf_link        **pmm_links;

    /* Apparently, this is not necessary since kernel 5.11...
     * I'm also not sure if it's needed for our use case any more with a single ring buffer.
     *
     * Taking it out for now so that we can (try) to have things running without root.
     */
#if 0
    err = bump_memlock_rlimit();
    if (err) {
        WARN("failed to increase rlimit: %d\n", err);
        return err;
    }
#endif

    init_bpf_print();

    nr_cpus = libbpf_num_possible_cpus();

    err = open_and_load_mat_daemon(&obj);
    if (err) {
        WARN("failed to open and load mat_daemon_bpf\n");
        return err;
    }

    ddr_links = calloc(nr_cpus, sizeof(*ddr_links));
    pmm_links = calloc(nr_cpus, sizeof(*pmm_links));
    if (!ddr_links || !pmm_links) {
        err = 1;
        WARN("failed to alloc ddr_links or pmm_links\n");
        goto cleanup;
    }

    pfm_initialize();

    if (get_perf_event_attr(config.profile_ddr_event_string, &pe)) {
        err = 1;
        WARN("failed to get attr for event: %s\n", config.profile_ddr_event_string);
        goto cleanup;
    }

    err = open_and_attach_perf_event(&pe, obj->progs.on_ddr_miss, ddr_links);
    if (err < 0) {
        WARN("failed to attach perf event: %s\n", config.profile_ddr_event_string);
        goto cleanup;
    }

    if (get_perf_event_attr(config.profile_pmm_event_string, &pe)) {
        err = 1;
        WARN("failed to get attr for event: %s\n", config.profile_pmm_event_string);
        goto cleanup;
    }

    err = open_and_attach_perf_event(&pe, obj->progs.on_pmm_miss, pmm_links);
    if (err < 0) {
        WARN("failed to attach perf event: %s\n", config.profile_pmm_event_string);
        goto cleanup;
    }

    bpf_md_procs_fd = map_fd_by_name("md_procs");
    if (bpf_md_procs_fd == -1) {
        err = 1;
        WARN("failed to get bpf_md_procs_fd\n");
        goto cleanup;
    }

		bpf_event_rb = ring_buffer__new(bpf_map__fd(obj->maps.event_rb), handle_bpf_event, NULL, NULL);
		if (!bpf_event_rb) {
				err = 1;
				WARN("failed to create bpf ring buffer\n");
				goto cleanup;
		}

    err = pthread_create(&bpf_polling_pthread, NULL, bpf_poll_thread, NULL);
    if (err != 0) {
        WARN("failed to create bpf polling thread\n");
				goto cleanup;
    }

    return 0;

cleanup:
    for (i = 0; i < nr_cpus; i++) {
        bpf_link__destroy(ddr_links[i]);
        bpf_link__destroy(pmm_links[i]);
    }
    free(ddr_links);
    free(pmm_links);
    mat_daemon_bpf__destroy(obj);
    obj = NULL;

    return err != 0;
}

int add_proc_to_bpf(pid_t pid) {
    int one;
    one = 1;
    bpf_map_update_elem(bpf_md_procs_fd, &pid, &one, 0);
    return 0;
}

int del_proc_from_bpf(pid_t pid) {
    bpf_map_delete_elem(bpf_md_procs_fd, &pid);
    return 0;
}

void bpf_add_interval_to_profile(void) {
    md_proc                 *proc;
    int                      n;
    tree_it(pid_t, array_t)  eit;
    struct md_event         *evt;
    md_object_map_it         oit;
    md_object               *obj;

    if (pthread_mutex_lock(&bpf_events_mtx) == 0) {
        bpf_events_working = bpf_events;
        bpf_events         = NULL;
        pthread_mutex_unlock(&bpf_events_mtx);
    }

    if (bpf_events_working == NULL) {
        return;
    }

    tree_traverse(bpf_events_working, eit) {
        proc = acquire_process(tree_it_key(eit));
        if (proc == NULL) { goto next; }

        lock_process(proc);
        for (n = 0; n < NR_NUMA_NODES; n += 1) {
            proc->untracked_llc_misses[n] = 0;
        }

        tree_traverse(proc->object_map, oit) {
            obj = &tree_it_val(oit);
            for (n = 0; n < NR_NUMA_NODES; n += 1) {
                obj->llc_misses[n] = 0;
            }
        }

        array_traverse(tree_it_val(eit), evt) {
            if (evt->type == MD_EVENT_LLC_MISS) {
                obj = map_get_object(proc->object_map, evt->addr);
                if (obj == NULL) {
                    proc->untracked_llc_misses[evt->node]         += 1;
                    proc->untracked_llc_misses_running[evt->node] += 1;
                } else {
                    obj->llc_misses[evt->node]         += 1;
                    obj->llc_misses_running[evt->node] += 1;
                    if (evt->time_ns > obj->last_access_time_ns) {
                        obj->last_access_time_ns = evt->time_ns;
                        obj->current_node        = evt->node;
                    }
                }
            }
        }

        unlock_process(proc);
        release_process(proc);
next:;
        array_clear(tree_it_val(eit));
    }

    tree_traverse(bpf_events_working, eit) {
        array_free(tree_it_val(eit));
    }
    tree_free(bpf_events_working);
}

void bpf_print(void) {
#if BPF_PRINTK
    char line[512];

    if (trace_pipe == NULL) { return; }

    // for debugging with bpf_printk
    while (fgets(line, sizeof(line), trace_pipe)) {
        fprintf(stderr, "%s", line);
    }
#endif
}
