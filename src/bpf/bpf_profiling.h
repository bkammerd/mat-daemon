#ifndef __BPF_PROFILING_H__
#define __BPF_PROFILING_H__

#include "../common.h"
#include "../process.h"

#include <sys/types.h>

int init_bpf_profiling(void);
int start_bpf_profiling_thread(void);
int add_proc_to_bpf(pid_t pid);
int del_proc_from_bpf(pid_t pid);
int add_object_to_bpf(md_proc *proc, u64 addr, u64 size);
int del_object_from_bpf(md_proc *proc, u64 addr);
int bpf_poll_ring_buffer(void);
void bpf_add_interval_to_profile(void);
void bpf_print(void);

#endif
