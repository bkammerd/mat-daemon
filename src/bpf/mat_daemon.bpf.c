#include <vmlinux.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>
#include <asm-generic/errno.h>
#include <linux/version.h> //I am using the linux/version.h, but defining myself
#include "bpf_common.h"

/* found these in kernel headers and configs for v. 5.7.2 -- not portable to
 * other kernels / configurations
 */
#define PAGE_POISON_PATTERN -11
#define SECTIONS_WIDTH      0
#define NODES_WIDTH         6
#define NODES_MASK          ((1UL << NODES_WIDTH) - 1)
#define SECTIONS_PGOFF      ((sizeof(unsigned long)*8) - SECTIONS_WIDTH)
#define NODES_PGOFF         (SECTIONS_PGOFF - NODES_WIDTH)
#define NODES_PGSHIFT       (NODES_PGOFF * (NODES_WIDTH != 0))

#define MAX_UNTRACKED_FAULTS (1<<21)

#define LOG2_8BIT(v)  (8 - 90/(((v)/4+14)|1) - 2/((v)/2+1))
#define LOG2_16BIT(v) (8*((v)>255) + LOG2_8BIT((v) >>8*((v)>255)))
#define LOG2_32BIT(v)                                        \
    (16*((v)>65535L) + LOG2_16BIT((v)*1L >>16*((v)>65535L)))
#define LOG2_64BIT(v)                                        \
    (32*((v)/2L>>31 > 0)                                     \
     + LOG2_32BIT((v)*1L >>16*((v)/2L>>31 > 0)               \
                         >>16*((v)/2L>>31 > 0)))

#if BPF_PRINTK
#define PRINTK(...) (bpf_printk(__VA_ARGS__))
#else
#define PRINTK(...) ;
#endif

struct unmap_info {
    struct page *page;
    unsigned long addr;
};

struct {
    __uint(type, BPF_MAP_TYPE_HASH);
    __uint(key_size, sizeof(__u32));
    __uint(value_size, sizeof(__u32));
    __uint(max_entries, MAX_PROCS);
} md_procs SEC(".maps");

struct {
    __uint(type, BPF_MAP_TYPE_RINGBUF);
    __uint(max_entries, (sizeof(struct md_event) * MAX_EVENTS));
} event_rb SEC(".maps");


static __always_inline
enum numa_node_id page_to_nid(const struct page *page) {
    struct page *p = (struct page *)page;

    if (p->flags == PAGE_POISON_PATTERN) {
        return NR_NUMA_NODES;
    }

    return ( (enum numa_node_id)
      ( ((p->flags) >> NODES_PGSHIFT) & NODES_MASK ) );
}

static __always_inline
int is_valid_proc(u32 tgid) {
    return bpf_map_lookup_elem(&md_procs, &tgid) != NULL;
}

#define REGION_IDX_IN_BOUNDS(_r) ((_r) >= 0 && (_r) <= MAX_REGIONS_PER_BUCKET - 1)

static __always_inline
int on_llc_miss( struct bpf_perf_event_data *ctx, enum numa_node_id node ) {

    u32 tgid;
    struct md_event evt;

    if (ctx == NULL) { return 0; }

    tgid = (bpf_get_current_pid_tgid() >> 32);

    if (!is_valid_proc(tgid)) { return 0; }

    __builtin_memset(&evt, 0, sizeof(evt));

    evt.pid     = tgid;
    evt.type    = MD_EVENT_LLC_MISS;
    evt.addr    = ctx->addr;
    evt.node    = node;
    evt.time_ns = bpf_ktime_get_ns();

    bpf_ringbuf_output(&event_rb, &evt, sizeof(evt), 0);

    return 0;
}

SEC("perf_event/1")
int on_ddr_miss(struct bpf_perf_event_data *ctx)
{
    return on_llc_miss(ctx, DDR_NODE);
}


SEC("perf_event/2")
int on_pmm_miss(struct bpf_perf_event_data *ctx)
{
    return on_llc_miss(ctx, PMM_NODE);
}

#if 0
/* MRJ -- I haven't tested this code -- but it should work with the ring
 * buffer
 */
struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __uint(key_size, sizeof(__u32));
    __uint(value_size, sizeof(__u64));
    __uint(max_entries, 1);
} tmp_addrs SEC(".maps");

struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __uint(key_size, sizeof(__u32));
    __uint(value_size, sizeof(__u32));
    __uint(max_entries, 1);
} in_fault SEC(".maps");

struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __uint(key_size, sizeof(__u32));
    __uint(value_size, sizeof(__u32));
    __uint(max_entries, 1);
} in_unmap SEC(".maps");

struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __uint(key_size, sizeof(__u32));
    __uint(value_size, sizeof(__u32));
    __uint(max_entries, 1);
} in_move_pages SEC(".maps");

struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __uint(key_size, sizeof(__u32));
    __uint(value_size, sizeof(struct unmap_info));
    __uint(max_entries, 1);
} tmp_unmap_info SEC(".maps");

static __always_inline
int add_page_to_rss(u32 tgid, u64 addr, enum numa_node_id node) {
    struct md_event evt;

    __builtin_memset(&evt, 0, sizeof(evt));

    evt.pid     = tgid;
    evt.type    = MD_EVENT_PAGE_ALLOC;
    evt.addr    = addr;
    evt.node    = node;
    evt.time_ns = bpf_ktime_get_ns();

    bpf_ringbuf_output(&event_rb, &evt, sizeof(evt), 0);

    return 0;
}

static __always_inline
int remove_page_from_rss(u32 tgid, u64 addr, enum numa_node_id node) {
    struct md_event evt;

    __builtin_memset(&evt, 0, sizeof(evt));

    evt.pid     = tgid;
    evt.type    = MD_EVENT_PAGE_RELEASE;
    evt.addr    = addr;
    evt.node    = node;
    evt.time_ns = bpf_ktime_get_ns();

    bpf_ringbuf_output(&event_rb, &evt, sizeof(evt), 0);

    return 0;
}

static __always_inline
int on_fault_entry( __u64 address )
{
    u64 pid_tgid;
    u32 pid, tgid, zero;
    zero = 0;

    pid_tgid = bpf_get_current_pid_tgid();

    tgid = ( pid_tgid >> 32 );

    if (!is_valid_proc(tgid)) { return 0; }

    pid = ( (u32) pid_tgid );

    bpf_map_update_elem(&in_fault, &zero, &pid, BPF_ANY);
    bpf_map_update_elem(&tmp_addrs, &zero, &address, BPF_ANY);

    return 0;
}

static __always_inline
int on_fault_exit( )
{
    u32 zero;

    /* in the common case, tmp_addrs will already be cleared, but we still
     * need to clear it here in case of an error condition
     */
    zero = 0;
    bpf_map_delete_elem(&tmp_addrs, &zero);
    bpf_map_delete_elem(&in_fault, &zero);
    return 0;
}

static __always_inline
int on_fault_page(struct page * page){
    u64  pid_tgid;
    u32  tgid;
    u32  zero;
    u32 *exp;
    u32  pid;
    u64 *addr;

    zero = 0;
    exp = bpf_map_lookup_elem(&in_fault, &zero);
    if (!exp) { return 0; }

    pid_tgid = bpf_get_current_pid_tgid();
    tgid = ( pid_tgid >> 32 );
    pid = ( (u32) pid_tgid );

    if (pid != (*exp)) { return 0; }

    addr = bpf_map_lookup_elem(&tmp_addrs, &zero);
    if (addr == NULL) { return 0; }
    if (*addr == 0){ return 0; }

    if (page != NULL) {
        add_page_to_rss(tgid, (*addr), page_to_nid(page));
    }

    bpf_map_delete_elem(&tmp_addrs, &zero);
    return 0;
}

//instrument page_add_anon_rmap and page_add_new_anon_rmap
//these are the code paths for tracking anonymous rss
SEC("fentry/page_add_anon_rmap")
int BPF_PROG(page_add_anon_rmap_entry,struct page *page,
    struct vm_area_struct *vma, unsigned long address, bool compound)
{
    return on_fault_entry(address);
}

SEC("fexit/page_add_anon_rmap")
int BPF_PROG(page_add_anon_rmap_return,struct page *page,
    struct vm_area_struct *vma, unsigned long address, bool compound)
{
    on_fault_page(page);
    return on_fault_exit();
}

SEC("fentry/page_add_new_anon_rmap")
int BPF_PROG(page_add_new_anon_rmap_entry,struct page *page,
     struct vm_area_struct *vma, unsigned long address, bool compound){
    return on_fault_entry(address);
}

SEC("fexit/page_add_new_anon_rmap")
int BPF_PROG(page_add_new_anon_rmap_return,struct page *page,
     struct vm_area_struct *vma, unsigned long address, bool compound){
    on_fault_page(page);
    return on_fault_exit();
}

#if LINUX_VERSION >= KERNEL_VERSION(5,14,0)
SEC("kprobe/do_set_pte")
int BPF_KPROBE(do_set_pte_entry,struct vm_fault *vmf, struct page *page, unsigned long addr)
{
    return on_fault_entry(addr);
}
#endif

SEC("fentry/remove_migration_pte")
int BPF_PROG(remove_migration_pte_entry,struct page *page, struct vm_area_struct *vma,
                 unsigned long addr, void *old){
    return on_fault_entry(addr);
}

SEC("kprobe/shmem_mfill_atomic_pte")
int BPF_KPROBE(shmem_mfill_atomic_pte_entry,struct mm_struct *dst_mm,
                   pmd_t *dst_pmd,
                   struct vm_area_struct *dst_vma,
                   unsigned long dst_addr){
    return on_fault_entry(dst_addr);
}

SEC("fexit/page_add_file_rmap")
int BPF_PROG(page_add_file_rmap_exit,struct page *page, bool compound){
    on_fault_page(page);
    return on_fault_exit();
}


/* unmap_page_range is the path taken by munmap -- we will need to cover
 * another path for page migration
 */
SEC("fentry/unmap_page_range")
int BPF_PROG(unmap_page_range_entry)
{
    u64 pid_tgid;
    u32 tgid;
    u64 zero;
    u32 pid;

    pid_tgid = bpf_get_current_pid_tgid();

    tgid = ( pid_tgid >> 32 );
    if (!is_valid_proc(tgid)) { return 0; }

    zero = 0;
    pid  = ( (u32) pid_tgid );
    bpf_map_update_elem(&in_unmap, &zero, &pid, BPF_ANY);
    return 0;
}

/* MRJ -- need to use a kprobe because vm_normal_page is not supported by
 * fentry/fexit
 */
SEC("kprobe/vm_normal_page")
int BPF_KPROBE(vm_normal_page_entry, struct vm_area_struct *vma,
    unsigned long addr, u64 pte)
{
    u32  zero;
    u32 *exp;
    u32  pid;

    zero = 0;

    exp = bpf_map_lookup_elem(&in_unmap, &zero);
    if (!exp) { return 0; }

    pid = bpf_get_current_pid_tgid();
    if (pid != (*exp)) { return 0; }

    bpf_map_update_elem(&tmp_addrs, &zero, &addr, BPF_ANY);
    return 0;
}

SEC("fentry/page_remove_rmap")
int BPF_PROG(page_remove_rmap_entry, struct page *page)
{
    u64  pid_tgid;
    u32  tgid;
    u32  zero;
    u32 *exp;
    u32  pid;
    u64 *addr;

    zero = 0;
    exp  = bpf_map_lookup_elem(&in_unmap, &zero);
    if (!exp) { return 0; }

    pid_tgid = bpf_get_current_pid_tgid();
    tgid = ( pid_tgid >> 32 );
    pid = ( (u32) pid_tgid );

    if (pid != (*exp)) { return 0; }

    addr = bpf_map_lookup_elem(&tmp_addrs, &zero);
    if (addr == NULL) { return 0; }

    if (page != NULL) {
        remove_page_from_rss(tgid, (*addr), page_to_nid(page));
    }

    bpf_map_delete_elem(&tmp_addrs, &zero);
    return 0;
}

SEC("fexit/unmap_page_range")
int BPF_PROG(unmap_page_range_exit) {
    u32 zero;

    /* in the common case, tmp_addrs will already be cleared, but we still
     * need to clear it here in case of an error condition
     */
    zero = 0;
    bpf_map_delete_elem(&tmp_addrs, &zero);
    bpf_map_delete_elem(&in_unmap,  &zero);
    return 0;
}

SEC("fentry/kernel_move_pages")
int BPF_PROG(move_pages_entry, pid_t pid) {
    u32 u32_pid;
    u32 zero;

    u32_pid = ((u32)pid);
    if (!is_valid_proc(u32_pid)) { return 0; }

    zero = 0;
    bpf_map_update_elem(&in_move_pages, &zero, &u32_pid, BPF_ANY);

    return 0;
}

SEC("fexit/kernel_move_pages")
int BPF_PROG(move_pages_exit) {
    u32 zero;

    zero = 0;
    bpf_map_delete_elem(&in_move_pages,  &zero);
    bpf_map_delete_elem(&tmp_unmap_info, &zero);

    return 0;
}

SEC("fentry/try_to_unmap_one")
int BPF_PROG(try_to_unmap_one_entry, struct page *page, struct vm_area_struct *vma, unsigned long address, void *arg) {
    u32                zero;
    u32               *pid;
    struct unmap_info  tmp_info;

    zero = 0;

    pid = bpf_map_lookup_elem(&in_move_pages, &zero);
    if (pid == NULL) { return 0; }

    bpf_probe_read_kernel(&(tmp_info.page), sizeof(struct page*),  &page);
    bpf_probe_read_kernel(&(tmp_info.addr), sizeof(unsigned long), &address);

    bpf_map_update_elem(&tmp_unmap_info, &zero, &tmp_info, BPF_ANY);

    return 0;
}

SEC("fentry/mem_cgroup_migrate")
int BPF_PROG(mem_cgroup_migrate_entry, struct page *oldpage, struct page *newpage) {
    u32                zero;
    u32               *pid_ptr;
    u32                pid;
    struct unmap_info *tmp;

    zero = 0;

    pid_ptr = bpf_map_lookup_elem(&in_move_pages, &zero);
    if (pid_ptr == NULL) { return 0; }

    pid = (*pid_ptr);

    if (!is_valid_proc(pid)) { return 0; }

    tmp = bpf_map_lookup_elem(&tmp_unmap_info, &zero);
    if (tmp == NULL) { return 0; }

    if (tmp->page != oldpage) {
        bpf_map_delete_elem(&tmp_unmap_info, &zero);
        return 0;
    }

    if (oldpage != NULL) {
        remove_page_from_rss(pid, tmp->addr, page_to_nid(oldpage));
    }
    if (newpage != NULL) {
        add_page_to_rss(pid, tmp->addr, page_to_nid(newpage));
    }

    return 0;
}
#endif

char LICENSE[] SEC("license") = "GPL";
