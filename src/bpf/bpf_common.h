#ifndef __BPF_COMMON_H__
#define __BPF_COMMON_H__

#include "../profiling_primitives.h"

#define ENCODE_ADDR_AND_SIZE(_addr, _size) \
    (((((__u64)(_addr)) >> 12ULL) << 28ULL) | (((__u64)(_size)) >> 12ULL))
#define DECODE_ADDR(_enc) \
    (((_enc) >> 28ULL) << 12ULL)
#define DECODE_SIZE(_enc) \
    (((_enc) & 0xFFFFFFF) << 12ULL)

/* MRJ -- code for encoding page faults into an 8-byte field
 * This could be useful if we need to stream page faults over a ring buffer
 */
#define ENCODE_FAULT(_addr, _tgid, _node, _type) \
    ( ( ( ( \
              ( (((__u64)(_addr)) >> 12ULL) << 24ULL ) \
          ) | \
            ((((__u32)(_tgid)) << 10ULL) >> 10ULL) << 2ULL \
        ) | \
          ((((unsigned char)(_node)) << 7ULL) >> 7ULL) << 1ULL \
      ) | \
      ((((unsigned char)(_type)) << 7ULL) >> 7ULL) \
    )

#define DECODE_FAULT_ADDR(_enc) \
    ( (((_enc) >> 24ULL) << 12ULL) & 0xFFFFFFFFFFFF )
#define DECODE_FAULT_TGID(_enc) \
    (((_enc) & 0xFFFFFF) >> 2ULL)
#define DECODE_FAULT_NODE(_enc) \
    (((_enc) & 0x2) >> 1ULL)
#define DECODE_FAULT_TYPE(_enc) \
    ((_enc) & 0x1)

/* #define BPF_PRINTK (1) */
#define MAX_PROCS  (256)
#define MAX_EVENTS (1ull << 20)

struct md_event {
  __u32               pid;
  enum md_event_type  type;
  __u64               addr;
  enum numa_node_id   node;
  __u64               time_ns;
};
#endif
