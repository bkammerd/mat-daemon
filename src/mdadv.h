#ifndef __MDADV_H__
#define __MDADV_H__

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>        /* For mode constants */
#include <limits.h>
#include <mqueue.h>

#define MDADV_PRIO           (0)
#define MDADV_QUEUE_NAME_MAX NAME_MAX
#define MDADV_LOCKOUT        (0xFFFFFFFFFFFFFFFF)
#define MDADV_LOCKOUT_SET    (1)
#define MDADV_LOCKOUT_CLEAR  (0)

typedef struct __attribute__((packed)) {
    uint64_t inst_addr;
    uint32_t advice;
} mdadv;


#ifdef MDADV_SENDER

typedef struct {
    mqd_t qd;
    pid_t pid;
} mdadv_sender;

int mdadv_start_sender(mdadv_sender *sender, pid_t pid);
int mdadv_close_sender(mdadv_sender *sender);
int mdadv_send(mdadv_sender *sender, mdadv *adv);

#endif /* MDADV_SENDER */

#ifdef MDADV_RECEIVER

typedef struct {
    mqd_t qd;
    pid_t pid;
} mdadv_receiver;

int mdadv_start_receiver(mdadv_receiver *receiver, pid_t pid);
int mdadv_close_receiver(mdadv_receiver *receiver);
int mdadv_receive(mdadv_receiver *receiver, mdadv *adv);
int mdadv_receive_or_timeout_ms(mdadv_receiver *receiver, mdadv *adv, uint64_t timeout_ms);

#endif /* MDADV_RECEIVER */


#ifdef MDADV_IMPL

static void mdadv_get_queue_name(char *buff, pid_t pid) {
    int   pid_start;
    pid_t save_pid;
    int   ndig;
    int   i;

    buff[0] = 0;
    strcat(buff, "/mdadv");

    pid_start = strlen(buff);
    save_pid  = pid;
    ndig      = 1;

    while (pid /= 10) { ndig += 1; }
    pid = save_pid;

    for (i = 0; i < ndig; i += 1) {
        buff[pid_start + ndig - i - 1] = '0' + (pid % 10);
        pid /= 10;
    }
    buff[pid_start + ndig] = 0;

    strcat(buff, ".mq");
}

#ifdef MDADV_SENDER

int mdadv_start_sender(mdadv_sender *sender, pid_t pid) {
    char buff[MDADV_QUEUE_NAME_MAX];
    int  err;

    mdadv_get_queue_name(buff, pid);

    err = 0;

    sender->pid = pid;

    if ((sender->qd = mq_open(buff, O_WRONLY)) == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    return err;
}

int mdadv_close_sender(mdadv_sender *sender) { return mq_close(sender->qd); }

int mdadv_send(mdadv_sender *sender, mdadv *adv) {
    int err;

    err = mq_send(sender->qd, (const char*)adv, sizeof(*adv), MDADV_PRIO);

    if (err == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    return err;
}

#endif /* MDADV_SENDER */

#ifdef MDADV_RECEIVER

int mdadv_start_receiver(mdadv_receiver *receiver, pid_t pid) {
    char           buff[MDADV_QUEUE_NAME_MAX];
    int            err;
    struct mq_attr attr;
    mode_t         old_mode;

    mdadv_get_queue_name(buff, pid);

    mq_unlink(buff);

    receiver->pid = pid;

    err = 0;

    attr.mq_flags   = 0;
    attr.mq_maxmsg  = 10;
    attr.mq_msgsize = sizeof(mdadv);
    attr.mq_curmsgs = 0;

    old_mode = umask(0);
    if ((receiver->qd = mq_open(buff, O_RDONLY | O_CREAT | O_EXCL, 0666, &attr)) == -1) {
        err   = -errno;
        errno = 0;
        goto out;
    }

out:;
    umask(old_mode);
    return err;
}

int mdadv_close_receiver(mdadv_receiver *receiver) {
    int  err;
    char buff[MDADV_QUEUE_NAME_MAX];

    err = mq_close(receiver->qd);
    if (err == -1) {
        err   = -errno;
        errno = 0;
    }

    mdadv_get_queue_name(buff, receiver->pid);

    mq_unlink(buff);

    return err;
}

int mdadv_receive(mdadv_receiver *receiver, mdadv *adv) {
    int err;

    err = mq_receive(receiver->qd, (char*)adv, sizeof(*adv), NULL);

    if (err == -1) {
        err   = -errno;
        errno = 0;
    }

    return err;
}

int mdadv_receive_or_timeout_ms(mdadv_receiver *receiver, mdadv *adv, uint64_t timeout_ms) {
    struct timespec ts;
    int             err;

    ts.tv_sec  = timeout_ms / 1000;
    ts.tv_nsec = 1000000 * (timeout_ms % 1000);

    err = mq_timedreceive(receiver->qd, (char*)adv, sizeof(*adv), NULL, &ts);

    if (err == -1) {
        err   = -errno;
        errno = 0;
    }

    return err;
}

#endif /* MDADV_RECEIVER */
#endif /* MDADV_IMPL */

#endif
