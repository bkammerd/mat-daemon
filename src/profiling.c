#include "profiling.h"
#include "bpf/bpf_common.h"
#include "bpf/bpf_profiling.h"
#include "perf_profiling.h"
#include "config.h"
#include "profile_response.h"
#include "object.h"
#include "mdmsg.h"
#include "tree.h"

#if 0
use_tree(u64, float);
use_tree(u64, u64);
#endif

#include <fcntl.h>
#include <math.h>

u64              profile_interval_counter;
u64              alloc_trigger_interval;
u64              profile_interval_start_time_us;
static pthread_t profiling_pthread;

int init_profiling(void) {
    int err;
    err = 0;
    if ((err = init_bpf_profiling()) != 0)   { goto out; }
        INFO("Initialized BPF.\n");
out:;
    return err;
}

static void convert_to_flat(md_object_map objects, array_t *flat_profile) {
    md_object_map_it it;

    tree_traverse(objects, it) {
        array_push((*flat_profile), tree_it_val(it));
    }
}

static void print_profile(u64 cur_time) {
    array_t    flat_profile;
    md_proc   *proc;
    pid_t      pid;
    u64        total_objects;
    u64        total_accesses[NR_NUMA_NODES];
    u64        total_alloc[NR_NUMA_NODES];
    u64        total_free[NR_NUMA_NODES];
    u64        total_pages_ddr;
    u64        total_pages_pmm;
    md_object *obj;
    //int        cnt;

    flat_profile = array_make(md_object);

    fprintf(config.profile_output_file,
            "profile: %16lu   time(ms): %12lu %lu\n",
            profile_interval_counter, cur_time, hash_table_len(process_table));

    PROCESS_FOR(proc, {
        array_clear(flat_profile);

        lock_process(proc);

        pid = proc->pid;

        convert_to_flat(proc->object_map, &flat_profile);

        qsort(array_data(flat_profile), array_len(flat_profile),
            sizeof(md_object), md_object_scaled_acc_cmp);

        total_objects            =
        total_accesses[DDR_NODE] =
        total_accesses[PMM_NODE] =
        total_alloc[DDR_NODE]    =
        total_free[DDR_NODE]     =
        total_alloc[PMM_NODE]    =
        total_free[PMM_NODE]     =
        total_pages_ddr          =
        total_pages_pmm          = 0;

        array_traverse(flat_profile, obj) {
            total_objects            += 1;
            total_accesses[DDR_NODE] += obj->llc_misses[DDR_NODE];
            total_accesses[PMM_NODE] += obj->llc_misses[PMM_NODE];
/*             total_alloc[DDR_NODE]    += obj->alloc_pages[DDR_NODE]; */
/*             total_free[DDR_NODE]     += obj->free_pages[DDR_NODE]; */
/*             total_alloc[PMM_NODE]    += obj->alloc_pages[PMM_NODE]; */
/*             total_free[PMM_NODE]     += obj->free_pages[PMM_NODE]; */
/*             total_pages_ddr          += obj->present_pages[DDR_NODE]; */
/*             total_pages_pmm          += obj->present_pages[PMM_NODE]; */
        }

        fprintf(config.profile_output_file,
                "  PID %-16u\n"
                "    DDR_ACC:           %-16lu\n"
                "    PMM_ACC:           %-16lu\n"
                "    UNTRACKED DDR_ACC: %-16lu\n"
                "    UNTRACKED PMM_ACC: %-16lu\n"
                "    DDR_ALLOC:         %-16lu\n"
                "    DDR_FREE:          %-16lu\n"
                "    PMM_ALLOC:         %-16lu\n"
                "    PMM_FREE:          %-16lu\n"
                "    DDR(MB):           %-13.4f\n"
                "    PMM(MB):           %-13.4f\n"
                "    UNTRACKED DDR(MB): %-13.4f\n"
                "    UNTRACKED PMM(MB): %-13.4f\n"
                "    objects:           %-16lu\n"
                "    allocs:            %-16u\n"
                "    frees:             %-16u\n"
                "    ignored bytes:     %-16lu\n",
                pid,
                total_accesses[DDR_NODE],
                total_accesses[PMM_NODE],
                proc->untracked_llc_misses[DDR_NODE],
                proc->untracked_llc_misses[PMM_NODE],
                total_alloc[DDR_NODE],
                total_free[DDR_NODE],
                total_alloc[PMM_NODE],
                total_free[PMM_NODE],
                pages_to_MB(total_pages_ddr),
                pages_to_MB(total_pages_pmm),
                pages_to_MB(proc->untracked_present_pages[DDR_NODE]),
                pages_to_MB(proc->untracked_present_pages[PMM_NODE]),
                total_objects,
                proc->obj_allocs_this_interval,
                proc->obj_frees_this_interval,
                proc->ignored_bytes_total);

#if 0
        cnt = 0;
        array_traverse(flat_profile, obj) {
            fprintf(config.profile_output_file,
                    "    %p-%p  node: %d DDR_ACC: %-12lu PMM_ACC: %-12lu"
                    " DDR_SCA: %-12.4f PMM_SCA: %-12.4f"
                    " DDR_RSS: %-12.4f PMM_RSS: %-12.4f\n",
                    (void*)obj->start,
                    (void*)obj->end,
                    obj->current_node,
                    obj->llc_misses[DDR_NODE],
                    obj->llc_misses[PMM_NODE],
                    obj->llc_misses_scaled[DDR_NODE],
                    obj->llc_misses_scaled[PMM_NODE],
                    pages_to_MB(obj->present_pages[DDR_NODE]),
                    pages_to_MB(obj->present_pages[PMM_NODE]));
            cnt++;
            if (cnt >= 300) {
                break;
            }
        }
#endif

#if 0
        array_traverse(flat_profile, obj) {
            fprintf(config.profile_output_file,
                    "    %p-%p  hid: %-10u creation_time: %16lu last_access_time: %16lu DDR_ACC: %-12.4f PMM_ACC: %-12.4f"
                    " DDR_RSS: %-12.4f PMM_RSS: %-12.4f\n",
                    (void*)obj->start,
                    (void*)obj->end,
                    obj->hid,
                    obj->creation_time_ns,
                    obj->last_access_time_ns,
                    obj->llc_misses_scaled[DDR_NODE],
                    obj->llc_misses_scaled[PMM_NODE],
                    pages_to_MB(obj->present_pages[DDR_NODE]),
                    pages_to_MB(obj->present_pages[PMM_NODE]));
        }
#endif

        unlock_process(proc);
    });

    array_free(flat_profile);
}

#if OBJECT_ANALYSIS
u64 get_rss(int pid) {
    char  buff[4096];
    FILE *statm;
    u32   tmp, rss;

    sprintf(buff, "/proc/%d/statm", pid);

    statm = fopen(buff, "r");
    if (statm == NULL) { return 0; }

    if (fscanf(statm, "%u %u", &tmp, &rss) != 2) {  return 0; }

    fclose(statm);

    return PAGE_SIZE * (u64)rss;
}

void do_object_analysis(md_proc *proc) {
    u64               tot_rss;
    u64               anon_rss;
    u64               file_rss;
    md_object_map_it  obj_it;
    md_object        *obj;

    (void)anon_rss;
    (void)file_rss;

    tot_rss = get_rss(proc->pid);

    if (tot_rss > 0) {
        char *x = pretty_bytes(tot_rss);
        INFO("statm:              %10s\n", x);
        INFO("                    ----------\n");
        free(x);

        u64 mapped_bytes       = 0;
        u64 faulted_bytes      = 0;
        u64 file_faulted_bytes = 0;
        u64 file_mapped_bytes  = 0;

        tree_traverse(proc->object_map, obj_it) {
            obj            = &(tree_it_val(obj_it));
            mapped_bytes  += obj->end - obj->start;
            faulted_bytes += (obj->present_pages[0] + obj->present_pages[1]) * PAGE_SIZE;

            object_analysis.sum_tracked_pages += obj->present_pages[0] + obj->present_pages[1];
            if (obj->hid == MDMSG_HID_FILE_BACKED) {
                file_mapped_bytes  += obj->end - obj->start;
                file_faulted_bytes += (obj->present_pages[0] + obj->present_pages[1]) * PAGE_SIZE;
            }
        }
        faulted_bytes += (proc->untracked_present_pages[0] + proc->untracked_present_pages[1]) * PAGE_SIZE;

        object_analysis.sum_untracked_pages += proc->untracked_present_pages[0] + proc->untracked_present_pages[1];

        double ratio, err;

        x = pretty_bytes(faulted_bytes);
        INFO("faults:             %10s\n", x);
        free(x);
        x = pretty_bytes(mapped_bytes);
        INFO("mapped:             %10s\n", x);
        free(x);
        x = pretty_bytes((mapped_bytes - file_mapped_bytes) + file_faulted_bytes);
        INFO("mapped+file_faults: %10s\n", x);
        free(x);
        INFO("\n");

        ratio = (double)faulted_bytes / (double)tot_rss;
        err   = fabs(ratio - 1.0);

        object_analysis.faults_err.min    = MIN(err, object_analysis.faults_err.min);
        object_analysis.faults_err.max    = MAX(err, object_analysis.faults_err.max);
        object_analysis.faults_err.sum   += err;
        object_analysis.faults_err.sign  += ratio > 1.0 ? 1 : 0;

        ratio = (double)mapped_bytes / (double)tot_rss;
        err   = fabs(ratio - 1.0);

        object_analysis.mapped_err.min    = MIN(err, object_analysis.mapped_err.min);
        object_analysis.mapped_err.max    = MAX(err, object_analysis.mapped_err.max);
        object_analysis.mapped_err.sum   += err;
        object_analysis.mapped_err.sign  += ratio > 1.0 ? 1 : 0;

        ratio = (double)((mapped_bytes - file_mapped_bytes) + file_faulted_bytes) / (double)tot_rss;
        err   = fabs(ratio - 1.0);

        object_analysis.mapped_file_err.min    = MIN(err, object_analysis.mapped_file_err.min);
        object_analysis.mapped_file_err.max    = MAX(err, object_analysis.mapped_file_err.max);
        object_analysis.mapped_file_err.sum   += err;
        object_analysis.mapped_file_err.sign  += ratio > 1.0 ? 1 : 0;

/*                 object_analysis.sum_file_ratio += (double)file_rss / (double)tot_rss; */

        object_analysis.count += 1;
    }
}

#endif

static void *profile_thread_fn(void *args) {
    u64                  profile_print_cnt;
    u64                  start;
    u64                  elapsed;
    u64                  profile_start;
    array_t              collected_profile;
    md_proc             *proc;
    md_object_map_it     obj_it;
    md_object           *obj;
    u64                  now_ns;
    double               obj_scaled_acc_sum;
    enum numa_node_id    node;

#if 0
    FILE                *sitef;
    tree(u64, float)     site_acc;
    tree_it(u64, float)  site_acc_it;
    tree(u64, u64)       site_sizes;
    tree_it(u64, u64)    site_sizes_it;
    const char          *lazy_space = "";

    sitef      = fopen("sites.txt", "w");
    site_acc   = NULL;
    site_sizes = NULL;
#endif

    profile_print_cnt = 0;
    profile_start     = start = measure_time_now_ms();
    for (;;) {
        elapsed = measure_time_now_ms() - start;
        if (elapsed < config.profile_period_ms) {
            usleep(1000 * (config.profile_period_ms - elapsed));
        } else {
            WARN("profiling took longer than profile-period-ms\n");
        }
        start                          = measure_time_now_ms();
        profile_interval_start_time_us = measure_time_now_us();


/*         INFO("PROFILE\n"); */

        if (migration_thread_running()) {
/*             INFO("  SKIPPING: MIGRATE\n"); */
            continue;
        }

/*         INFO("  BPF...\n"); */
        bpf_add_interval_to_profile();
        /* for the LLC miss rate trigger
         * TODO: can we do the miss rate trigger in BPF as well?
         */
/*         INFO("  PERF...\n"); */
        perf_profile_interval();

        PROCESS_FOR(proc, {
            lock_process(proc);

            now_ns = 1000ULL * measure_time_now_us();

            proc->mdtop_max_scaled_acc  = 0.0;
            proc->mdtop_timestamp_ns    = now_ns;
            proc->mdtop_max_age_ns      = 0;
            proc->mdtop_max_stale_tm_ns = 0;

#if 0
            if (site_acc   != NULL) { tree_free(site_acc);   }
            if (site_sizes != NULL) { tree_free(site_sizes); }

            site_acc   = tree_make(u64, float);
            site_sizes = tree_make(u64, u64);
#endif

            /* Do the history scaling. */
            tree_traverse(proc->object_map, obj_it) {
                obj                = &tree_it_val(obj_it);
                obj_scaled_acc_sum = 0.0;

                for (node = 0; node < NR_NUMA_NODES; node++) {
#if 1
                    double scale = 1.0;
                    scale *= (1.0 - config.profile_history_weight);

                    obj->llc_misses_scaled[node]  = (config.profile_history_weight * obj->llc_misses_scaled[node]);
                    obj->llc_misses_scaled[node] += (scale * obj->llc_misses[node]);
                    obj_scaled_acc_sum           += obj->llc_misses_scaled[node];
#endif
#if 0
                    /* MRJ -- tested and removed these variations because they
                     * did not make much difference
                     */
                    double scale = 1024 * 1024;
                    if (node == PMM_NODE) { scale *= 4.0; }
                    scale *= (1.0 - config.profile_history_weight);
#endif
                }

                proc->mdtop_max_scaled_acc  = MAX(proc->mdtop_max_scaled_acc,  obj_scaled_acc_sum);
                proc->mdtop_max_age_ns      = MAX(proc->mdtop_max_age_ns,      now_ns - obj->creation_time_ns);
                proc->mdtop_max_stale_tm_ns = MAX(proc->mdtop_max_stale_tm_ns, now_ns - obj->last_access_time_ns);

#if 0
                if (obj->origin_inst_addr != 0) {
                    site_acc_it = tree_lookup(site_acc, obj->origin_inst_addr);
                    if (!tree_it_good(site_acc_it)) {
                        site_acc_it = tree_insert(site_acc, obj->origin_inst_addr, 0);
                    }
                    tree_it_val(site_acc_it) += obj_scaled_acc_sum;

                    site_sizes_it = tree_lookup(site_sizes, obj->origin_inst_addr);
                    if (!tree_it_good(site_sizes_it)) {
                        site_sizes_it = tree_insert(site_sizes, obj->origin_inst_addr, 0);
                    }
                    tree_it_val(site_sizes_it) += get_object_capacity_estimate(obj);
                }
#endif
            }

#if 0
            lazy_space = "";
            tree_traverse(site_acc, site_acc_it) {
                site_sizes_it = tree_lookup(site_sizes, tree_it_key(site_acc_it));
                fprintf(sitef, "%s%lu:%lu:%f",
                        lazy_space,
                        tree_it_key(site_acc_it),
                        tree_it_val(site_sizes_it),
                        tree_it_val(site_acc_it));
                lazy_space = " ";
            }
            fprintf(sitef, "\n");
            fflush(sitef);
#endif

            unlock_process(proc);
        });

/*         INFO("  DONE.\n"); */

        collected_profile = array_make(md_object);
        PROCESS_FOR(proc, {
            lock_process(proc);
            tree_traverse(proc->object_map, obj_it) {
                obj = &(tree_it_val(obj_it));
                array_push(collected_profile, *obj);
            }
            unlock_process(proc);
        });

        trigger_profile_response_thread(&collected_profile);

        if (profile_print_cnt == config.profile_print_interval_period) {
            print_profile(start - profile_start);
            profile_print_cnt = 0;
        }

        PROCESS_FOR(proc, {
            lock_process(proc);
            proc->obj_allocs_this_interval = proc->obj_frees_this_interval = 0;

            if (proc->profile_info != NULL) {
                proc->profile_info->profiled_intervals += 1;
            }

#if OBJECT_ANALYSIS
            do_object_analysis(proc);
#endif
            unlock_process(proc);
        });

        profile_print_cnt        += 1;
        profile_interval_counter += 1;
    }

    return NULL;
}

int start_profiling_thread(void) {
    int status;
    status = pthread_create(&profiling_pthread, NULL, profile_thread_fn, NULL);
    if (status != 0) {
        WARN("failed to create profiling thread\n");
    }
    return status;
}

void setup_profiling_for_process(int pid) {
    process_profile_info *info;
    md_proc              *proc;

    info = malloc(sizeof(*info));
    memset(info, 0, sizeof(*info));

    proc = acquire_process(pid);
    if (proc != NULL) {
        lock_process(proc);

        proc->profile_info = info;
        add_proc_to_bpf(pid);
        /* this is for the LLC miss rate trigger
         * TODO: could we do this with BPF as well?
         */
        setup_perf_profiling_for_process(pid, &info->perf);

        unlock_process(proc);
        release_process(proc);
    }

}

void stop_profiling_for_process(int pid, md_proc *proc, int needs_lock) {
    int err;

    if (proc == NULL) {
        return;
    }

    if (needs_lock) {
        lock_process(proc);
    }

    if ((err = del_proc_from_bpf(pid)) != 0) {
        ERR("failed to delete process %d from bpf; err = %d\n", pid, err);
    }

    stop_perf_profiling_for_process(pid, &proc->profile_info->perf);

    free(proc->profile_info);
    proc->profile_info = NULL;

    if (needs_lock) {
        unlock_process(proc);
    }
}

float get_proc_event_ratio(md_proc *proc) {
    float ratio = 0.0;

    if (proc->profile_info != NULL
    &&  proc->profile_info->perf.prev_ratio > 0.0) {
        ratio =   fabs(proc->profile_info->perf.cur_ratio - proc->profile_info->perf.prev_ratio)
                / proc->profile_info->perf.prev_ratio;
    }

    return ratio;
}
