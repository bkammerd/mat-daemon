#include "msg.h"
#define MDMSG_RECEIVER
#define MDMSG_IMPL
#include "mdmsg.h"
#include "process.h"
#include "config.h"
#include "threadpool.h"
#include "thread_safe_queue.h"
#include "array.h"
#include "hash_table.h"
#include "tree.h"

#include <stdlib.h>
#include <pthread.h>


use_hash_table(pid_t, u64);
use_tree(pid_t, array_t);

#if OBJECT_ANALYSIS
use_tree(u64, u64);
#endif


static mdmsg_receiver       receiver;
static pthread_t            message_pull_pthread;
static pthread_t            message_process_pthread;
pthread_cond_t              message_cond             = PTHREAD_COND_INITIALIZER;
pthread_mutex_t             message_cond_mtx         = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t             message_mtx              = PTHREAD_MUTEX_INITIALIZER;
tree(pid_t, array_t)        pull_messages;
tree(pid_t, array_t)        process_messages;

void close_message_queue(void) {
    mdmsg_close_receiver(&receiver);
    INFO("Closed message receiver.\n");
}

int init_messages(void) {
    int err;

    (void)_mdmsg_size;

    err = mdmsg_start_receiver(&receiver);
    if (err != 0) {
        WARN("failed to start, err = %d\n", err);
        goto out;
    }

    pull_messages    = tree_make(pid_t, array_t);
    process_messages = tree_make(pid_t, array_t);

out:;
    return err;
}

static u64 pid_hash(pid_t pid) { return (u64)pid; }
static void handle_message(mdmsg *msg);

static void *message_pull_thread(void *arg) {
    const u32               hold_time_limit = config.profile_period_ms / 2;
    int                     err;

    u64                     start_ms;
    hash_table(pid_t, u64)  ignore_counts;
    mdmsg                   msg;
    tree_it(pid_t, array_t) it;
    u32                     idx;
    u64                    *count_ptr;
    array_t                 new_array;
    u64                     now_ms;
    u64                     elapsed_ms;
    tree(pid_t, array_t)    tmp;
    mdmsg                  *msg_it;
    pid_t                   pid;
    md_proc                *proc;

#if OBJECT_ANALYSIS
    tree(u64, u64)    objs;
    tree_it(u64, u64) obj_it;
    objs = tree_make(u64, u64);
    objs = tree_make(u64, u64);
#endif

    err           = 0;
    start_ms      = measure_time_now_ms();
    ignore_counts = hash_table_make(pid_t, u64, pid_hash);

    for (;;) {
        err = mdmsg_receive_or_timeout_ms(&receiver, &msg, hold_time_limit);

        if (err <= 0) {
            if (err == -ETIMEDOUT) { goto check_time; }
            if (err == -EBADF)     { return NULL;     }

            ERR("failed to receive message, err = %d\n", err);
        }

        if (msg.header.msg_type == MDMSG_INIT) {
            /* Wan't these to be done immediately. */

            handle_message(&msg);
            goto check_time;
        }

        if (msg.header.msg_type == MDMSG_FREE) {
            it = tree_lookup(pull_messages, msg.header.pid);
            if (tree_it_good(it)) {
                idx = array_len(tree_it_val(it)) - 1;
                array_rtraverse(tree_it_val(it), msg_it) {
                    if (msg_it->header.msg_type == MDMSG_ALLO
                    &&  msg_it->allo.addr       == msg.free.addr) {

                        if ((count_ptr = hash_table_get_val(ignore_counts, msg_it->header.pid)) == NULL) {
                            hash_table_insert(ignore_counts, msg_it->header.pid, msg_it->allo.size);
                        } else {
                            *count_ptr += msg_it->allo.size;
                        }

                        array_delete(tree_it_val(it), idx);
                        goto check_time;
                    }
                    idx -= 1;
                }
            }

#if OBJECT_ANALYSIS
            obj_it = tree_lookup(objs, msg.free.addr);
            if (tree_it_good(obj_it)) {
                object_analysis.sum_lifetime_ms += (msg.free.timestamp_ns - tree_it_val(obj_it)) / 1000000;
            }
#endif
        } else if (msg.header.msg_type == MDMSG_ALLO) {
#if OBJECT_ANALYSIS
            object_analysis.sum_objects += 1;
            if (msg.allo.hid == MDMSG_HID_FILE_BACKED) { object_analysis.sum_file_objects += 1; }
            object_analysis.sum_bytes   += msg.allo.size;
            tree_insert(objs, msg.allo.addr, msg.allo.timestamp_ns);
#endif
        }

        it = tree_lookup(pull_messages, msg.header.pid);
        if (!tree_it_good(it)) {
            new_array = array_make(mdmsg);
            it = tree_insert(pull_messages, msg.header.pid, new_array);
        }
        array_push(tree_it_val(it), msg);

check_time:;
        now_ms     = measure_time_now_ms();
        elapsed_ms = now_ms - start_ms;

        if (elapsed_ms >= hold_time_limit
        &&  pthread_mutex_trylock(&message_mtx) == 0) {

            tmp              = pull_messages;
            pull_messages    = process_messages;
            process_messages = tmp;

            pthread_mutex_lock(&message_cond_mtx);
            pthread_cond_signal(&message_cond);
            pthread_mutex_unlock(&message_cond_mtx);

            pthread_mutex_unlock(&message_mtx);

            hash_table_traverse(ignore_counts, pid, count_ptr) {
                proc = acquire_process(pid);
                if (proc == NULL) { continue; }

                /*
                 * Technically, we should lock the process, but this memory isn't
                 * going to be read/written anywhere else so it shouldn't matter.
                 */
                proc->ignored_bytes_total += *count_ptr;

                release_process(proc);
            }

            hash_table_free(ignore_counts);
            ignore_counts = hash_table_make(pid_t, u64, pid_hash);

            start_ms = now_ms;
        }

    }

    return NULL;
}

static void handle_message_no_lock(mdmsg *msg) {
    pid_t       pid;
    md_object   new_object;

    pid = msg->header.pid;

    switch (msg->header.msg_type) {
        case MDMSG_INIT:
            add_process(pid);
            break;
        case MDMSG_FINI:
            del_process(pid, 0 /* no lock */);
            break;
        case MDMSG_ALLO:
            if (config.mode == MODE_region) {
                create_regions_in_process_for_range_no_lock(
                    pid,
                    msg->allo.addr,
                    msg->allo.addr + msg->allo.size,
                    msg->allo.timestamp_ns);
            } else {
                memset(&new_object, 0, sizeof(new_object));

                new_object.start                = msg->allo.addr;
                new_object.end                  = new_object.start + msg->allo.size;
                new_object.creation_time_ns     = msg->allo.timestamp_ns;
                new_object.last_access_time_ns  = msg->allo.timestamp_ns;
                new_object.last_migrate_time_ns = msg->allo.timestamp_ns;
                new_object.pid                  = pid;
#if OBJECT_ORIGIN
                new_object.origin_inst_addr     = msg->allo.inst_addr;
#endif
                new_object.hid                  = msg->allo.hid;
                new_object.preassigned_pmm      = config.advise_initial_placement && msg->allo.hid;

                if (new_object.preassigned_pmm) {
                    new_object.current_node = PMM_NODE;
                }

#if 0
                fprintf(config.profile_output_file, "adding object: %d %lx (%llu MB)\n",
                        pid, new_object.start, TO_MB((new_object.end - new_object.start)));
                fflush(config.profile_output_file);
#endif
                add_object_to_process_no_lock(pid, &new_object);
            }

            break;
        case MDMSG_UPDT:
            update_object_hid_no_lock(pid, msg->updt.addr, msg->updt.hid);
            break;
        case MDMSG_LABL:
            insert_string_for_hid_no_lock(pid, msg->labl.hid, msg->labl.label);
            break;
        case MDMSG_FREE:
            if (config.mode == MODE_object) {
#if 0
                fprintf(config.profile_output_file, "remove object: %d %lx\n",
                        pid, msg->free.addr);
                fflush(config.profile_output_file);
#endif
                del_object_from_process_no_lock(pid, msg->free.addr);
            }
            break;
    }
}

static void handle_message(mdmsg *msg) {
    pid_t       pid;
    md_object   new_object;

    pid = msg->header.pid;

    switch (msg->header.msg_type) {
        case MDMSG_INIT:
            add_process(pid);
            break;
        case MDMSG_FINI:
            del_process(pid, 1 /* needs lock */);
            break;
        case MDMSG_ALLO:
            if (config.mode == MODE_region) {
                create_regions_in_process_for_range(
                    pid,
                    msg->allo.addr,
                    msg->allo.addr + msg->allo.size,
                    msg->allo.timestamp_ns);
            } else {
                memset(&new_object, 0, sizeof(new_object));

                new_object.start                = msg->allo.addr;
                new_object.end                  = new_object.start + msg->allo.size;
                new_object.creation_time_ns     = msg->allo.timestamp_ns;
                new_object.last_access_time_ns  = msg->allo.timestamp_ns;
                new_object.last_migrate_time_ns = msg->allo.timestamp_ns;
                new_object.pid                  = pid;
#if OBJECT_ORIGIN
                new_object.origin_inst_addr     = msg->allo.inst_addr;
#endif
                new_object.hid                  = msg->allo.hid;

#if 0
                fprintf(config.profile_output_file, "alling object: %d %lx (%llu MB)\n",
                        pid, new_object.start, TO_MB((new_object.end - new_object.start)));
                fflush(config.profile_output_file);
#endif
                add_object_to_process(pid, &new_object);
            }

            break;
        case MDMSG_UPDT:
            update_object_hid(pid, msg->updt.addr, msg->updt.hid);
            break;
        case MDMSG_LABL:
            insert_string_for_hid(pid, msg->labl.hid, msg->labl.label);
            break;
        case MDMSG_FREE:
            if (config.mode == MODE_object) {
#if 0
                fprintf(config.profile_output_file, "relove object: %d %lx\n",
                        pid, msg->free.addr);
                fflush(config.profile_output_file);
                del_object_from_process(pid, msg->free.addr);
#endif
            }
            break;
    }
}

static void *message_process_thread(void *arg) {
    tree_it(pid_t, array_t)  it;
    md_proc                 *proc;
    mdmsg                   *msg_it;
    int                      del;

    for (;;) {
        pthread_mutex_lock(&message_cond_mtx);
        pthread_cond_wait(&message_cond, &message_cond_mtx);
        pthread_mutex_unlock(&message_cond_mtx);

        pthread_mutex_lock(&message_mtx);
        tree_traverse(process_messages, it) {
            if (array_len(tree_it_val(it)) > 0) {
                proc = acquire_process(tree_it_key(it));
                if (proc == NULL) { goto clear; }

                del = 0;

                lock_process(proc);
#if 0
                fprintf(config.profile_output_file, "handling %d messages\n",
                        array_len(tree_it_val(it)));
                fflush(config.profile_output_file);
#endif
                array_traverse(tree_it_val(it), msg_it) {
                    if (msg_it->header.msg_type == MDMSG_FINI) {
                        del = 1;
                        goto unlock;
                    }
                    handle_message_no_lock(msg_it);
                }
#if 0
                fprintf(config.profile_output_file, "done with %d messages\n",
                        array_len(tree_it_val(it)));
                fflush(config.profile_output_file);
#endif

unlock:;
                unlock_process(proc);
                release_process(proc);

                if (del) {
                    handle_message(msg_it);
                }
clear:;
                array_clear(tree_it_val(it));
            }
        }
        pthread_mutex_unlock(&message_mtx);
    }

    return NULL;
}

int start_message_threads(void) {
    int status;

    status = pthread_create(&message_pull_pthread, NULL, message_pull_thread, NULL);
    if (status != 0) {
        WARN("failed to start message pull thread\n");
        goto out;
    }

    status = pthread_create(&message_process_pthread, NULL, message_process_thread, NULL);
    if (status != 0) {
        WARN("failed to start message processing thread\n");
        goto out;
    }

out:;
    return status;
}
