#ifndef __MSG_H__
#define __MSG_H__

int init_messages(void);
int start_message_threads(void);
void close_message_queue(void);

#endif
