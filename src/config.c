#include "config.h"
#include "policy.h"

#define SIMPLE_CONFIG_IMPL
#include "simple_config.h"

#include <limits.h>

md_config config;

#define X(mode) #mode,
const char *mode_strings[N_MODES] = { LIST_MODES };
#undef X

static int is_mode_string(struct scfg *cfg, const char *val) {
    char        err_buff[1024];
    const char *lazy_comma;

    err_buff[0] = 0;
    strcpy(err_buff, "invalid mode-string -- options are: ");

    lazy_comma = "";

#define X(_mode)                                                  \
if (strcmp(val, #_mode) == 0) {                                   \
    config.mode = MODE_##_mode;                                   \
    return 1;                                                     \
}                                                                 \
sprintf(err_buff + strlen(err_buff), "%s%s", lazy_comma, #_mode); \
lazy_comma = ", ";

    LIST_MODES
#undef X

    scfg_validate_set_err(cfg, err_buff);

    return 0;
}

static int is_valid_region_shift(struct scfg *cfg, int val) {
    char err_buff[1024];

    err_buff[0] = 0;
    strcpy(err_buff, "value must be an integer in the range [12-47]");

    if (val < 12 || val > 47) {
        scfg_validate_set_err(cfg, err_buff);
        return 0;
    }

    return 1;
}

static int do_load_trigger_policy(struct scfg *cfg, const char *val) {
    int success;

    success = load_trigger_policy(val);
    if (!success) {
        scfg_validate_set_err(cfg, "error when loading trigger policy");
    }

    return success;
}

static int do_load_ranking_policy(struct scfg *cfg, const char *val) {
    int success;

    success = load_ranking_policy(val);
    if (!success) {
        scfg_validate_set_err(cfg, "error when loading ranking policy");
    }

    return success;
}

static int do_load_packing_policy(struct scfg *cfg, const char *val) {
    int success;

    success = load_packing_policy(val);
    if (!success) {
        scfg_validate_set_err(cfg, "error when loading packing policy");
    }

    return success;
}

static int do_load_combined_policy(struct scfg *cfg, const char *val) {
    int success;

    success = load_combined_policy(val);
    if (!success) {
        scfg_validate_set_err(cfg, "error when loading combined policy");
    }

    return success;
}

static void usage(const char *argv0) {
    printf("usage: %s [CONFIG_PATH] [OPTIONS]\n", argv0);
    printf("       %s --print-config [CONFIG_PATH] [OPTIONS]\n", argv0);
    printf("       %s --help\n", argv0);
}

enum {
    OPT_GENERAL,
    OPT_SYSTEM,
    OPT_OBJECT_FILTERING,
    OPT_PROFILING,
    OPT_POLICIES,
    OPT_MISC,
    NR_OPT_CATEGORIES,
};

const char *opt_category_strings[] = {
    "GENERAL",
    "SYSTEM",
    "OBJECT FILTERING",
    "PROFILING",
    "POLICIES",
    "MISC",
};

typedef struct {
    char *name;
    int   type;
    int   category;
    char *desc;
    void *ptr;
} opt;

static array_t options;

static void option(const char *name, int type, int category, const char *desc, void *ptr) {
    opt new_opt;

    new_opt.name     = strdup(name);
    new_opt.type     = type;
    new_opt.category = category;
    new_opt.desc     = strdup(desc);
    new_opt.ptr      = ptr;

    array_push(options, new_opt);

    switch (type) {
        case OPT_BOOL:
            scfg_add_bool(config._scfg, name, ptr);   break;
        case OPT_INT:
            scfg_add_int(config._scfg, name, ptr);    break;
        case OPT_FLOAT:
            scfg_add_float(config._scfg, name, ptr);  break;
        case OPT_STRING:
            scfg_add_string(config._scfg, name, ptr); break;
    }
}

static void help(const char *argv0) {
    const char *red;
    const char *reset;
    opt        *o;
    int         i;
    int         col;
    int         j;
    char        c;

    red   = isatty(1) ? "\e[31m" : "";
    reset = isatty(1) ? "\e[0m"  : "";

    usage(argv0);

    printf("\n");
    printf("Use the --print-config leading flag to print the current configuration\n"
           "in valid config file syntax and exit.\n");
    printf("Use the --help flag option to print this information and exit.\n");

    printf("\n");
    printf("CONFIG_PATH: The path to a file that sets configuration options. Optional.\n");
    printf("OPTIONS:     Any number of options may be set on the command line.\n"
           "             They override those set in the config file if there was one.\n");

    printf("\nAvailable options:\n");

    for (i = 0; i < NR_OPT_CATEGORIES; i += 1) {
        printf("  %s\n", opt_category_strings[i]);
        array_traverse(options, o) {
            if (o->category == i) {
                printf("    ");
                switch (o->type) {
                    case OPT_BOOL:
                        printf("--%s%s%s, --%s%s%s=BOOL\n", red, o->name, reset, red, o->name, reset); break;
                    case OPT_INT:
                        printf("--%s%s%s=INT\n", red, o->name, reset);                 break;
                    case OPT_FLOAT:
                        printf("--%s%s%s=FLOAT\n", red, o->name, reset);               break;
                    case OPT_STRING:
                        printf("--%s%s%s=STRING\n", red, o->name, reset);              break;
                }
                printf("        ");
                col = 9;
                for (j = 0; j < strlen(o->desc); j += 1) {
                    c = o->desc[j];

                    if (c == ' ' && col > 70) { c = '\n'; }

                    printf("%c", c);

                    col += 1;

                    if (c == '\n') {
                        printf("        ");
                        col = 9;
                    }
                }
                printf("\n");
            }
        }
    }

    printf("\nOther options may be made available by loading policy plugins.\n");
}

int init_config(int argc, const char **argv) {
    int status;
    int do_print_config;

    status                     = 0;
    do_print_config            = 0;
    config._scfg               = scfg_make();
    config.profile_output_file = NULL;
    options                    = array_make(opt);


    option("mode-string",                        OPT_STRING, OPT_GENERAL,
                                                 "'object' or 'region'.\n"
                                                 "'object' tracks arbitrary memory ranges while 'region' tracks ranges of 1 << region-shift bytes.",
                                                 &config.mode_string);
                                                 require("mode-string");
                                                 validate_string("mode-string", is_mode_string);

    option("region-shift",                       OPT_INT, OPT_GENERAL,
                                                 "Determines the alignment and size of regions to track.\n"
                                                 "Example: --region-shift=21 gives 2MB regions.",
                                                 &config.region_shift);
                                                 default_int("region-shift", 21);
                                                 validate_int("region-shift", is_valid_region_shift);

    option("split-large-objects",                OPT_BOOL, OPT_GENERAL,
                                                 "When this option is set to true (the default), objects larger than"
                                                 " large-object-threshold will be split up.",
                                                 &config.split_large_objects);
                                                 default_bool("split-large-objects", 1);

    option("large-object-threshold",             OPT_INT, OPT_GENERAL,
                                                 "Threshold in MB to use to split large objects.",
                                                 &config.large_object_threshold);
                                                 default_int("large-object-threshold", 64);

    option("tier-1-node",                        OPT_INT, OPT_SYSTEM,
                                                 "NUMA node ID for the upper memory tier.",
                                                 &config.tier_1_node);
                                                 require("tier-1-node");

    option("tier-2-node",                        OPT_INT, OPT_SYSTEM,
                                                 "NUMA node ID for the lower memory tier.",
                                                 &config.tier_2_node);
                                                 require("tier-2-node");

    option("tier-1-capacity",                    OPT_FLOAT, OPT_SYSTEM,
                                                 "Capacity in GB of the upper memory tier.",
                                                 &config.tier_1_capacity);
                                                 require("tier-1-capacity");

    option("tier-2-capacity",                    OPT_FLOAT, OPT_SYSTEM,
                                                 "Capacity in GB of the lower memory tier.",
                                                 &config.tier_2_capacity);
                                                 require("tier-2-capacity");

    option("migration-min-age-ms",               OPT_INT, OPT_OBJECT_FILTERING,
                                                 "Ensures that objects younger than this will never be migrated.",
                                                 &config.migration_min_age_ms);
                                                 default_int("migration-min-age-ms", 1000);

    option("profile-period-ms",                  OPT_INT, OPT_PROFILING,
                                                 "The number of milliseconds between profile collection intervals.",
                                                 &config.profile_period_ms);
                                                 default_int("profile-period-ms", 1000);

    option("profile-overflow-thresh",            OPT_INT, OPT_PROFILING,
                                                 "Sets the perf sample period. See `man perf_event_open`.",
                                                 &config.profile_overflow_thresh);
                                                 default_int("profile-overflow-thresh", 512);

    option("profile-print-interval-period",      OPT_INT, OPT_PROFILING,
                                                 "Allow this many profile intervals to pass before printing the profile to the output file.",
                                                 &config.profile_print_interval_period);
                                                 default_int("profile-print-interval-period", 1);

    option("profile-ddr-event-string",           OPT_STRING, OPT_PROFILING,
                                                 "String encoding the hardware event for LLC misses to DDR.",
                                                 &config.profile_ddr_event_string);
                                                 require("profile-ddr-event-string");

    option("profile-pmm-event-string",           OPT_STRING, OPT_PROFILING,
                                                 "String encoding the hardware event for LLC misses to PMM.",
                                                 &config.profile_pmm_event_string);
                                                 require("profile-pmm-event-string");

    option("profile-perf-max-sample-pages",      OPT_INT, OPT_PROFILING,
                                                "The max number of pages per sample.",
                                                 &config.profile_perf_max_sample_pages);
                                                 require("profile-perf-max-sample-pages");

    option("profile-output-file-string",         OPT_STRING, OPT_PROFILING,
                                                 "The path of the output file where profiling information is written.",
                                                 &config.profile_output_file_string);
                                                 default_string("profile-output-file-string", "profile.txt");

    option("profile-history-weight",             OPT_FLOAT, OPT_PROFILING,
                                                 "A ratio of the weight of events from previous intervals versus the current interval.\n"
                                                 "Must be between 0.0 and 1.0 (inclusive).",
                                                 &config.profile_history_weight);
                                                 default_float("profile-history-weight", 0.5);
                                                 validate_float("profile-history-weight", is_float_between_zero_and_one);

    option("perf-event-string-1",                OPT_STRING, OPT_PROFILING,
                                                 "String encoding the first of two perf events collected for each process.",
                                                 &config.perf_event_string_1);
                                                 require("perf-event-string-1");

    option("perf-event-string-2",                OPT_STRING, OPT_PROFILING,
                                                 "String encoding the second of two perf events collected for each process.",
                                                 &config.perf_event_string_2);
                                                 require("perf-event-string-2");

    option("load-trigger-policy",                OPT_STRING, OPT_POLICIES,
                                                 "The path of the trigger policy plugin to load.",
                                                 &config.load_trigger_policy);
                                                 validate_string("load-trigger-policy", do_load_trigger_policy);

    option("load-ranking-policy",                OPT_STRING, OPT_POLICIES,
                                                 "The path of the ranking policy plugin to load.",
                                                 &config.load_ranking_policy);
                                                 validate_string("load-ranking-policy", do_load_ranking_policy);

    option("load-packing-policy",                OPT_STRING, OPT_POLICIES,
                                                 "The path of the packing policy plugin to load.",
                                                 &config.load_packing_policy);
                                                 validate_string("load-packing-policy", do_load_packing_policy);

    option("load-combined-policy",               OPT_STRING, OPT_POLICIES,
                                                 "The path of the policy plugin to load which contains routines for triggering, ranking, and packing.",
                                                 &config.load_combined_policy);
                                                 validate_string("load-combined-policy", do_load_combined_policy);

    option("auto-purge-inactive-procs",          OPT_BOOL, OPT_MISC,
                                                 "Detect when a process is inactive (maybe it got killed) and stop tracking it automatically.",
                                                 &config.auto_purge_inactive_procs);
                                                 default_bool("auto-purge-inactive-procs", 1);

    option("auto-purge-inactive-time-ms",        OPT_INT, OPT_MISC,
                                                 "How long processes can look inactive before they are purged.",
                                                 &config.auto_purge_inactive_time_ms);
                                                 default_int("auto-purge-inactive-time-ms", 3000);

    option("mdtop",                              OPT_BOOL, OPT_MISC,
                                                 "Show a top-like interface for processes currently communicating with mat-daemon.",
                                                 &config.mdtop);
                                                 default_bool("mdtop", 0);

    option("weird-perf-issue-detect",            OPT_BOOL, OPT_MISC,
                                                 "Whether or not mat-daemon should try to detect a strange perf bug where samples stopped being unexpectedly.",
                                                 &config.weird_perf_issue_detect);
                                                 default_bool("weird-perf-issue-detect", 0);

    option("weird-perf-issue-interval-limit",    OPT_INT, OPT_MISC,
                                                 "The number of intervals a process can go without receiving a sample for any event without"
                                                 " being flagged as having encountered the weird perf issue we experienced on certain access"
                                                 " patterns.",
                                                 &config.weird_perf_issue_interval_limit);
                                                 default_int("weird-perf-issue-interval-limit", 25);

    option("stop-app-during-migrations",         OPT_BOOL, OPT_MISC,
                                                 "Stop app during migrations.",
                                                 &config.stop_app_during_migrations);
                                                 default_bool("stop-app-during-migrations", 0);

    option("print-migrate-stats",                OPT_BOOL, OPT_MISC,
                                                 "Print migration stats.",
                                                 &config.print_migrate_stats);
                                                 default_bool("print-migrate-stats", 0);

    option("bpf-event-poll-period-ms",           OPT_INT, OPT_PROFILING,
                                                 "The number of milliseconds to wait before polling the bpf ring buffer.",
                                                 &config.bpf_event_poll_period_ms);
                                                 default_int("bpf-event-poll-period-ms", 100);
    option("increase-num-fd-rlimit",             OPT_BOOL, OPT_MISC,
                                                 "Try to increase the number of allowed file descriptors."
                                                 " Use this if you are using a many-core system and MAT Daemon can't open enough file descriptors for each perf event per core."
                                                 " Requires root privileges.",
                                                 &config.increase_num_fd_rlimit);
                                                 default_bool("increase-num-fd-rlimit", 0);

    option("advise-initial-placement",           OPT_BOOL, OPT_MISC,
                                                 "Send the allocator advise for where to place new pages for each allocation site.",
                                                 &config.advise_initial_placement);
                                                 default_bool("advise-initial-placement", 0);

    option("advice-cold-threshold",              OPT_FLOAT, OPT_MISC,
                                                 "When using advise-initial-placement, this ratio of object bytes from each allocation site"
                                                 " must be recommended for demotion before the entire site is marked as \"cold\".",
                                                 &config.advice_cold_threshold);
                                                 default_float("advice-cold-threshold", 1.0);
                                                 validate_float("advice-cold-threshold", is_float_between_zero_and_one);


    if (argc < 2) {
        usage(argv[0]);
        return 1;
    }

    if (strcmp(argv[1], "--help") == 0) {
        help(argv[0]);
        exit(0);
    } else if (strcmp(argv[1], "--print-config") == 0) {
        do_print_config = 1;
    }

    if (argc > 1 + do_print_config) {
        if (strncmp(argv[1 + do_print_config], "--", 2) == 0) {
            status = scfg_parse_cmd_line(config._scfg, argc - 1 - do_print_config, argv + 1 + do_print_config);
        } else {
            status = scfg_parse(config._scfg, argv[1 + do_print_config]);

            if (status == SCFG_ERR_NONE) {
                status = scfg_parse_cmd_line(config._scfg, argc - 2 - do_print_config, argv + 2 + do_print_config);
            }
        }
    } else {
        status = scfg_parse_cmd_line(config._scfg, argc - 2 - do_print_config, argv + 2 + do_print_config);
    }

    if (status != SCFG_ERR_NONE) {
        ERR_NOEXIT("%s\n", scfg_err_msg(config._scfg));
        if (status == SCFG_ERR_BAD_KEY) {
            INFO("To see a list of all available options, run `%s --help`.\n", argv[0]);
        }
    }

    if (status == SCFG_ERR_NONE) {
        config.profile_output_file = fopen(config.profile_output_file_string, "w");
        if (!config.profile_output_file) {
            ERR_NOEXIT("%s: error opening profile output file: %s\n", argv[1 + do_print_config], config.profile_output_file_string);
            return 1;
        }
        if (config.mode == MODE_region && config.region_shift == -1) {
            ERR_NOEXIT("%s: 'mode-string' is 'region' but 'region-shift' is unset!\n", argv[1 + do_print_config]);
            return 1;
        }
    }

    if (do_print_config && status == SCFG_ERR_NONE) {
        print_config();
        exit(0);
    }

    return status;
}

void print_config(void) {
    int  max_name_len;
    opt *o;
    int  i;

    max_name_len = 0;
    array_traverse(options, o) {
        if (strlen(o->name) > max_name_len) {
            max_name_len = strlen(o->name);
        }
    }
    for (i = 0; i < NR_OPT_CATEGORIES; i += 1) {
        array_traverse(options, o) {
            if (o->category == i) {
                switch (o->type) {
                    case OPT_BOOL:
                        printf("%-*s  %s\n", max_name_len, o->name, *(int*)o->ptr ? "YES" : "NO");
                        break;
                    case OPT_INT:
                        printf("%-*s  %d\n", max_name_len, o->name, *(int*)o->ptr);
                        break;
                    case OPT_FLOAT:
                        printf("%-*s  %f\n", max_name_len, o->name, *(float*)o->ptr);
                        break;
                    case OPT_STRING:
                        if (*(char**)o->ptr) {
                            printf("%-*s  '%s'\n", max_name_len, o->name, *(char**)o->ptr);
                        }
                        break;
                }
            }
        }
    }
}

void show_config(void) {
    int  max_name_len;
    opt *o;
    int  i;

    max_name_len = 0;
    array_traverse(options, o) {
        if (strlen(o->name) > max_name_len) {
            max_name_len = strlen(o->name);
        }
    }
    printf("================================ CURRENT CONFIG ================================\n");
    for (i = 0; i < NR_OPT_CATEGORIES; i += 1) {
        printf("%s:\n", opt_category_strings[i]);
        array_traverse(options, o) {
            if (o->category == i) {
                switch (o->type) {
                    case OPT_BOOL:
                        printf("  %-*s  %s\n", max_name_len, o->name, *(int*)o->ptr ? "YES" : "NO");
                        break;
                    case OPT_INT:
                        printf("  %-*s  %d\n", max_name_len, o->name, *(int*)o->ptr);
                        break;
                    case OPT_FLOAT:
                        printf("  %-*s  %f\n", max_name_len, o->name, *(float*)o->ptr);
                        break;
                    case OPT_STRING:
                        if (*(char**)o->ptr) {
                            printf("  %-*s  '%s'\n", max_name_len, o->name, *(char**)o->ptr);
                        } else {
                            printf("  %-*s  UNSET\n", max_name_len, o->name);
                        }
                        break;
                }
            }
        }
    }
    printf("================================================================================\n\n");
}

void add_policy_option(const char *name, int type, const char *desc, void *ptr) {
    option(name, type, OPT_POLICIES, desc, ptr);
}
