#include "common.h"

#if OBJECT_ORIGIN_DETAIL

#include "object_origin.h"
#include "object.h"
#include "thread_safe_queue.h"
#include "tree.h"
#include "config.h"

#include <pthread.h>
#include <libgen.h>

use_tree(u64, md_object_origin);


static pthread_t                   origin_parsing_pthread;
pthread_cond_t                     origin_parsing_thread_cond  = PTHREAD_COND_INITIALIZER;
pthread_mutex_t                    origin_parsing_thread_mtx   = PTHREAD_MUTEX_INITIALIZER;
static md_ts_queue                 origin_parse_request_queue;
static tree(u64, md_object_origin) known_origins;


#define OBJ_ORIGIN_KEY(_pid, _addr) \
    ((((u64)(u16)(_pid)) << 48ULL) | (_addr))


void object_origin_get_async(struct md_object_t *obj) {
    obj->origin = NULL;

    if (obj->origin_inst_addr != 0) {
        thread_safe_queue_enqueue(origin_parse_request_queue, obj);

        pthread_mutex_lock(&origin_parsing_thread_mtx);
        pthread_cond_signal(&origin_parsing_thread_cond);
        pthread_mutex_unlock(&origin_parsing_thread_mtx);
    }
}

void object_origin_cancel_if_in_flight(struct md_object_t *obj) {
    struct md_object_t **it;
    int                  i;

    pthread_mutex_lock(&origin_parse_request_queue.mtx);

    i = 0;
    array_rtraverse(origin_parse_request_queue.q, it) {
        if (*it == obj) {
            array_delete(origin_parse_request_queue.q, i);
            goto out;
        }
        i += 1;
    }

out:;
    pthread_mutex_unlock(&origin_parse_request_queue.mtx);
}

static void parse_origin(int pid, u64 origin_inst_addr, md_object_origin *origin) {
    FILE *f;
    char  buff[4096];
    char *s;
    char *cur;
    u64   cur_start;
    u64   start;
    u64   end;
    char  cur_copy[1024];
    FILE *p;

    origin->func_name   = "???";
    origin->file_name   = "???";
    origin->line_number = 0;

    snprintf(buff, sizeof(buff), "/proc/%d/maps", pid);
    f = fopen(buff, "r");

    if (f == NULL) { return; }

    cur       = NULL;
    cur_start = 0;

    while (fgets(buff, sizeof(buff), f)) {
        if (buff[strlen(buff) - 1] == '\n') { buff[strlen(buff) - 1] = 0; }

        if (*buff == 0) { continue; }

        /* range */
        if ((s = strtok(buff, " ")) == NULL) { continue; }
        sscanf(s, "%lx-%lx", &start, &end);

        /* perms */
        if ((s = strtok(NULL, " ")) == NULL) { continue; }
        /* offset */
        if ((s = strtok(NULL, " ")) == NULL) { continue; }
        /* dev */
        if ((s = strtok(NULL, " ")) == NULL) { continue; }
        /* inode */
        if ((s = strtok(NULL, " ")) == NULL) { continue; }

        /* path */
        if ((s = strtok(NULL, " ")) == NULL) { continue; }
        if (*s != '/')                       { continue; }

        if (cur == NULL || strcmp(cur, s)) {
            if (cur != NULL) { free(cur); }
            cur       = strdup(s);
            cur_start = start;
        }

        if (origin_inst_addr >= cur_start
        &&  origin_inst_addr <  end) {

            snprintf(cur_copy, sizeof(cur_copy), "%s", cur);
            origin->file_name = strdup(basename(cur_copy));

            snprintf(buff, sizeof(buff),
                    "addr2line -C -f -e %s %lx", cur, origin_inst_addr - cur_start);

            p = popen(buff, "r");

            if (p != NULL) {
                fread(buff, 1, sizeof(buff), p);

                s = buff;

                while (*s && *s != '\n') { s += 1; }

                if (*s) {
                    *s  = 0;
                    s  += 1;
                }

                origin->func_name = strdup(buff);

                if (*s != '?') {
                    if (s[strlen(s) - 1] == '\n') {
                        s[strlen(s) - 1] = 0;
                    }
                    if ((s = strtok(s, ":")) != NULL) {
                        free(origin->file_name);
                        origin->file_name = strdup(basename(s));

                        if ((s = strtok(NULL, ":")) != NULL) {
                            sscanf(s, "%lu", &origin->line_number);
                        }
                    }
                }

                pclose(p);
            }

            break;
        }
    }

    if (cur != NULL) { free(cur); }

    fclose(f);
}

static void *origin_parsing_thread(void *arg) {
    md_object                      *obj;
    int                             pid;
    u64                             origin_inst_addr;
    md_object_origin                new_origin;
    u64                             key;
    tree_it(u64, md_object_origin)  it;
    md_object_origin               *o;
    md_object                      *top;

    for (;;) {
        /* Wait until we know that there's at least one request on the queue. */
        pthread_mutex_lock(&origin_parsing_thread_mtx);
        pthread_cond_wait(&origin_parsing_thread_cond, &origin_parsing_thread_mtx);
        pthread_mutex_unlock(&origin_parsing_thread_mtx);

        for (;;) {
            obj = NULL;
            if (!thread_safe_queue_peek_keep_locked(origin_parse_request_queue, &obj)) {
                pthread_mutex_unlock(&origin_parse_request_queue.mtx);
                /*
                 * We've emptied the queue.
                 * Now we will wait on a mutex to signal that something's been added.
                 */
                break;
            }

            /* obj is at the top of the queue. */

            pid              = obj->pid;
            origin_inst_addr = obj->origin_inst_addr;

            pthread_mutex_unlock(&origin_parse_request_queue.mtx);

            key = OBJ_ORIGIN_KEY(pid, origin_inst_addr);
            it  = tree_lookup(known_origins, key);
            o   = tree_it_good(it) ? &tree_it_val(it) : NULL;

            if (o == NULL) {
                memset(&new_origin, 0, sizeof(new_origin));
                parse_origin(pid, origin_inst_addr, &new_origin);
                it = tree_insert(known_origins, key, new_origin);
                o  = &tree_it_val(it);
            }

            if (!thread_safe_queue_peek_keep_locked(origin_parse_request_queue, &top)) {
                pthread_mutex_unlock(&origin_parse_request_queue.mtx);
                /*
                 * The request was cancelled and now the queue is empty.
                 * Now we will wait on a mutex to signal that something's been added.
                 */
                break;
            }

            if (top == obj) {
                /*
                 * The object is still at the top of the queue, so it hasn't been cancelled.
                 * We'll set the origin and pop it off.
                 */
                obj->origin = o;

                pthread_mutex_unlock(&origin_parse_request_queue.mtx);
                thread_safe_queue_dequeue(origin_parse_request_queue, NULL);
            } else {
                /*
                 * The request must have been cancelled since it's not at the top
                 * of the queue any more.
                 */
                pthread_mutex_unlock(&origin_parse_request_queue.mtx);
            }
        }
    }

    return NULL;
}

int start_object_origin_parsing_thread(void) {
    int status;
    origin_parse_request_queue = thread_safe_queue_make(struct md_object_t*);
    known_origins              = tree_make(u64, md_object_origin);

    status = pthread_create(&origin_parsing_pthread, NULL, origin_parsing_thread, NULL);
    if (status != 0) {
        WARN("failed to create origin_parsing thread\n");
    }
    return status;
}

#endif
