#ifndef __OBJECT_H__
#define __OBJECT_H__

#include "tree.h"
#include "common.h"
#include "object_origin.h"
#include "profiling_primitives.h"

#include <stdint.h>
#include <sys/types.h>

typedef struct md_object_t {
    union {
        u64           start;
        u64           addr;
    };
    u64               end;
    u64               orig_end;
    u64               creation_time_ns;
    u64               last_access_time_ns;
    u64               last_migrate_time_ns;
    int               current_node;
    int               n_resident_pages;
    int               pid;
    int               mig_err;
    int               mig_count;
#if OBJECT_ORIGIN
    u64               origin_inst_addr;
#if OBJECT_ORIGIN_DETAIL
    md_object_origin *origin;
#endif
#endif
    u32               hid;
    u32               preassigned_pmm;

    u64               llc_misses[NR_NUMA_NODES];
    u64               llc_misses_running[NR_NUMA_NODES];
    float             llc_misses_scaled[NR_NUMA_NODES];
/*     u64               alloc_pages[NR_NUMA_NODES]; */
/*     u64               free_pages[NR_NUMA_NODES]; */
/*     u64               present_pages[NR_NUMA_NODES]; */
} md_object;

use_tree(u64, md_object);

typedef tree(u64, md_object)    md_object_map;
typedef tree_it(u64, md_object) md_object_map_it;
#define make_object_map() tree_make(u64, md_object)
#define free_object_map(_t) tree_free((_t))

md_object     *map_get_object(md_object_map map, u64 addr);
md_object_map  copy_object_map(md_object_map map);
int md_object_scaled_acc_cmp(const void *a, const void *b);
u64 get_object_capacity_estimate(const md_object *obj);

#endif
