#include "sig.h"
#include "common.h"
#include "cleanup.h"

#include <stdio.h>
#include <stdlib.h>

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef __USE_GNU
#define __USE_GNU
#endif
#include <signal.h>
#include <execinfo.h>
#include <ucontext.h>

#define MAX_BT_LEN (100)

#define TERM_BLUE  "\e[0;34m"
#define TERM_GREEN "\e[0;32m"
#define TERM_RED   "\e[0;31m"
#define TERM_RESET "\e[00m"

void print_backtrace(void *caller) {
    int    n;
    void  *bt_buff[MAX_BT_LEN];
    char **strs;
    int    i;

    n = backtrace(bt_buff, MAX_BT_LEN);

    if (caller != NULL) {
        bt_buff[1] = caller;
    }

    strs = backtrace_symbols(bt_buff, n);

    if (strs == NULL) {
        WARN("backtrace unavailable\n");
        return;
    }

    for (i = 3; i < n; i += 1) {
        printf("    %s\n", strs[i]);
    }

    free(strs);
}

static void print_fatal_signal_message_and_backtrace(char *sig_name, void *caller) {
    printf("\n" TERM_RED "mat-daemon has received a fatal signal (%s).\n", sig_name);
    printf("Here is a backtrace of its execution (most recent first):" TERM_RESET "\n\n");
    printf(TERM_BLUE);
    print_backtrace(caller);
    printf(TERM_RESET);
    printf("\n");
    printf(TERM_GREEN);
    printf("Please create an issue at https://gitlab.com/bkammerd/mat-daemon\n"
           "describing what happened.\n");
    printf(TERM_RESET);
    printf("\n");
}

static void *get_caller_address(ucontext_t *ucontext) {
    void *caller_address;

    caller_address = NULL;

    /* Get the address at the time the signal was raised */
#if defined(__i386__) // gcc specific
    caller_address = (void *) ucontext->uc_mcontext.gregs[REG_EIP]; // EIP: x86 specific
#elif defined(__x86_64__) // gcc specific
    caller_address = (void *) ucontext->uc_mcontext.gregs[REG_RIP]; // RIP: x86_64 specific
#else
#warning Unsupported architecture -- can't get caller address in backtrace.
#endif

    return caller_address;
}

static void sigsegv_handler(int sig, siginfo_t * info, void * ucontext) {
    struct sigaction  act;
    void             *caller_address;
    FILE             *p;
    char              buff[512];

    act.sa_handler = SIG_DFL;
    act.sa_flags = 0;
    sigemptyset (&act.sa_mask);
    sigaction(SIGSEGV, &act, NULL);

    cleanup();
    caller_address = get_caller_address(ucontext);
    snprintf(buff, sizeof(buff),
             "addr2line -e %s %p", argv0, caller_address);
    p = popen(buff, "r");
    if (p != NULL) {
        fread(buff, 1, sizeof(buff), p);
        pclose(p);
        ERR_NOEXIT("segfaulted on address %p at %s\n", info->si_addr, buff);
    } else {
        ERR_NOEXIT("segfaulted on address %p, PC: %p\n", info->si_addr, caller_address);
    }
    print_fatal_signal_message_and_backtrace("SIGSEGV", caller_address);

    /* Do the real signal */
    kill(0, SIGSEGV);
}

static void sigill_handler(int sig, siginfo_t * info, void * ucontext) {
    struct sigaction act;

    act.sa_handler = SIG_DFL;
    act.sa_flags = 0;
    sigemptyset (&act.sa_mask);
    sigaction(SIGILL, &act, NULL);

    cleanup();
    print_fatal_signal_message_and_backtrace("SIGILL", get_caller_address(ucontext));

    /* Do the real signal */
    kill(0, SIGILL);
}

static void sigfpe_handler(int sig, siginfo_t * info, void * ucontext) {
    struct sigaction act;

    act.sa_handler = SIG_DFL;
    act.sa_flags = 0;
    sigemptyset (&act.sa_mask);
    sigaction(SIGFPE, &act, NULL);

    cleanup();
    print_fatal_signal_message_and_backtrace("SIGFPE", get_caller_address(ucontext));

    /* Do the real signal */
    kill(0, SIGFPE);
}

static void sigbus_handler(int sig, siginfo_t * info, void * ucontext) {
    struct sigaction act;

    act.sa_handler = SIG_DFL;
    act.sa_flags = 0;
    sigemptyset (&act.sa_mask);
    sigaction(SIGBUS, &act, NULL);

    cleanup();
    print_fatal_signal_message_and_backtrace("SIGBUS", get_caller_address(ucontext));

    /* Do the real signal */
    kill(0, SIGBUS);
}

int init_sig(void) {
    struct sigaction sa;

    sigemptyset(&sa.sa_mask);
    sa.sa_flags     = SA_SIGINFO;
    sa.sa_sigaction = sigsegv_handler;
    if (sigaction(SIGSEGV, &sa, NULL) == -1) {
        WARN("sigaction failed for SIGSEGV\n");
        return 1;
    }

    sigemptyset(&sa.sa_mask);
    sa.sa_flags     = SA_SIGINFO;
    sa.sa_sigaction = sigill_handler;
    if (sigaction(SIGILL, &sa, NULL) == -1) {
        WARN("sigaction failed for SIGILL\n");
        return 1;
    }

    sigemptyset(&sa.sa_mask);
    sa.sa_flags     = SA_SIGINFO;
    sa.sa_sigaction = sigfpe_handler;
    if (sigaction(SIGFPE, &sa, NULL) == -1) {
        WARN("sigaction failed for SIGFPE\n");
        return 1;
    }

    sigemptyset(&sa.sa_mask);
    sa.sa_flags     = SA_SIGINFO;
    sa.sa_sigaction = sigfpe_handler;
    if (sigaction(SIGFPE, &sa, NULL) == -1) {
        WARN("sigaction failed for SIGFPE\n");
        return 1;
    }

    sigemptyset(&sa.sa_mask);
    sa.sa_flags     = SA_SIGINFO;
    sa.sa_sigaction = sigbus_handler;
    if (sigaction(SIGBUS, &sa, NULL) == -1) {
        WARN("sigaction failed for SIGBUS\n");
        return 1;
    }

    return 0;
}
