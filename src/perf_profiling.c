#include "perf_profiling.h"
#include "common.h"
#include "profiling.h"
#include "config.h"
#include "process.h"
#include "tree.h"
#include "array.h"

static void perf_get_event_counter(const char *event_string, perf_event_info *info) {
    int err;

    info->pe.type           = PERF_TYPE_HARDWARE;
    info->pe.size           = sizeof(struct perf_event_attr);
    info->pe.disabled       = 1;
    info->pe.exclude_kernel = 1;
    info->pe.exclude_hv     = 1;

    info->pfm.size = sizeof(pfm_perf_encode_arg_t);
    info->pfm.attr = &info->pe;
    err            = pfm_get_os_event_encoding(event_string,
                                               PFM_PLM2 | PFM_PLM3,
                                               PFM_OS_PERF_EVENT,
                                               &info->pfm);

    if (err != PFM_SUCCESS) {
        ERR("Couldn't find an appropriate event to use. (error %d) Aborting.\n", err);
    }
}

static void perf_init_event_counter_for_pid(int pid, const char *event_string, perf_event_info *info) {
    int fd;

    /* Use libpfm to fill the pe structs */
    fd = 0;

    perf_get_event_counter(event_string, info);

    /* Open the perf file descriptor */
    fd = syscall(__NR_perf_event_open, &info->pe, pid, -1, -1, 0);

    if (fd == -1) {
        if (errno == ESRCH) { return; } /* The process may have already exited. */

        WARN("attr = 0x%lx\n", (u64)(info->pe.config));
        ERR("Error opening perf event 0x%llx (%d -- %s).\n", info->pe.config, errno, strerror(errno));
    } else {
        info->fd = fd;

        /* Initialize */
        ioctl(info->fd, PERF_EVENT_IOC_RESET, 0);
        ioctl(info->fd, PERF_EVENT_IOC_ENABLE, 0);
    }
}

void setup_perf_profiling_for_process(int pid, perf_process_profile_info *info) {
    perf_init_event_counter_for_pid(pid, config.perf_event_string_1, &info->e1);
    perf_init_event_counter_for_pid(pid, config.perf_event_string_2, &info->e2);
}

void stop_perf_profiling_for_process(int pid, perf_process_profile_info *info) {
    close(info->e1.fd);
    close(info->e2.fd);
}

static void update_ratio_for_process(md_proc *proc) {
    long long  e1_count;
    long long  e2_count;
    long long  e1_diff;
    long long  e2_diff;

    read(proc->profile_info->perf.e1.fd, &e1_count, sizeof(e1_count));
    read(proc->profile_info->perf.e2.fd, &e2_count, sizeof(e2_count));

    /* Shift the window. */
    memmove(proc->profile_info->perf.ratio_window,
            proc->profile_info->perf.ratio_window + 1,
            sizeof(proc->profile_info->perf.ratio_window) - sizeof(proc->profile_info->perf.ratio_window[0]));

    proc->profile_info->perf.prev_ratio = proc->profile_info->perf.cur_ratio;


    e1_diff = safe_diff(e1_count, proc->profile_info->perf.c1);
    e2_diff = safe_diff(e2_count, proc->profile_info->perf.c2);

    proc->profile_info->perf.c1 = e1_count;
    proc->profile_info->perf.c2 = e2_count;

    if (e2_diff == 0) {
        proc->profile_info->perf.cur_ratio = 0.0;
    } else {
        proc->profile_info->perf.cur_ratio =   (float)e1_diff
                                             / (float)e2_diff;
    }

    proc->profile_info->perf.ratio_window[RATIO_WINDOW_SIZE - 1] = proc->profile_info->perf.prev_ratio;
}

void perf_profile_interval(void) {
    md_proc *proc;

    PROCESS_FOR(proc, {
        lock_process(proc);
        update_ratio_for_process(proc);
        unlock_process(proc);
    });
}
