#include "mdtop.h"
#include "common.h"
#include "config.h"
#include "array.h"
#include "process.h"
#include "object.h"
#include "profiling.h"

#include <termios.h>
#include <math.h>

#define TERM_ALT_SCREEN() append_to_output_buff("\e[?1049h")
#define TERM_STD_SCREEN() append_to_output_buff("\e[?1049l")
#define CLEAR_SCREEN()    append_to_output_buff("\e[2J")
#define CURSOR_HOME()     append_to_output_buff("\e[H")
#define CURSOR_HIDE()     append_to_output_buff("\e[?25l")
#define CURSOR_SHOW()     append_to_output_buff("\e[?25h")
#define CLEAR_LINE()      append_to_output_buff("\e[K")
#define UP_LINE()         append_to_output_buff("\e[1A")
#define DOWN_LINE()       append_to_output_buff("\e[1B")
#define BEG_LINE()        append_to_output_buff("\r");
#define INVERT            "\e[7m"
#define BOLD              "\e[1m"
#define COLOR1            "\e[48;5;17m"
#define COLOR2            "\e[48;5;19m"
#define COLOR3            "\e[48;5;55m"
#define COLOR4            "\e[48;5;57m"
#define COLOR5            "\e[48;5;23m"
#define COLOR6            "\e[48;5;25m"
#define RED               "\e[38;5;172m"
#define ORANGE_BG         "\e[48;5;172m"
#define GREY              "\e[38;5;243m"
#define GREY_BG           "\e[48;5;243m"
#define RESET             "\e[0m"

static const char *shades[] = {
    "\e[48;5;16m",
    "\e[48;5;17m",
    "\e[48;5;18m",
    "\e[48;5;19m",
    "\e[48;5;20m",
    "\e[48;5;21m",
    "\e[48;5;57m",
    "\e[48;5;93m",
    "\e[48;5;129m",
    "\e[48;5;165m",
    "\e[48;5;201m",
    "\e[48;5;200m",
    "\e[48;5;199m",
    "\e[48;5;198m",
    "\e[48;5;197m",
    "\e[48;5;196m",
};

#define N_SHADES (sizeof(shades) / sizeof(shades[0]))

#define OBJ_PER_PAGE (20)

static int     mdtop_initialized;
static int     paused;
static int     show_objects;
static int     selected_proc;
static char    scratch_buff[1024];
static char    col_fmt[128];
static array_t output_buff;
static int     selected_page;
static int     col_selected = 0;
static int     reverse_order = 1;
static int     obj_col_selected = 0;
static int     reverse_obj_order = 1;
static md_proc*                 gproc;
static int     show_hid;
use_tree(u32, array_t);
use_tree(u32, float);
tree(u32, float) heat;
typedef char * (*proc_print_fn_t)(md_proc*);
typedef char * (*obj_print_fn_t)(md_proc*, md_object*);
typedef char * (*hid_print_fn_t)(u32, array_t);


#define TOP_PROC_COLS                                                                   \
    /* name,        width,  header left,  header right,         string function */      \
    X( pid,         9,      "PID",        "",                   print_proc_pid        ) \
    X( cmd,         15,     "CMD",        "",                   print_proc_cmd        ) \
    X( numobjects,  7,      "OBJ",        "",                   print_proc_numobjects ) \
    X( mem,         27,     "MEM",        "rss, dev, tracked",  print_proc_mem        ) \
    X( acc,         24,     "ACC",        "cnt, evt, tracked",  print_proc_acc        ) \
    X( ratio,       24,     "RATIO",      "",                   print_proc_ratio      )

#define TOP_OBJ_COLS                                                                    \
    /* name,        width,  header left,  header right,         string function */      \
    X( hid,         16,     "HID",        "",                   print_obj_hid         ) \
    X( addr,        16,     "ADDR",       "",                   print_obj_range       ) \
    X( size,        12,     "SIZE",       "",                   print_obj_size        ) \
    X( mem,         15,     "MEM",        "%res, dev",          print_obj_mem         ) \
    X( acc,         19,     "ACC",        "cnt, evt",           print_obj_acc         ) \
    X( heat,        6,      "HEAT",       "",                   print_obj_heat        ) \
    X( active,      8,      "ACTIVE",     "",                   print_obj_active      ) \
    X( age,         5,      "AGE",        "",                   print_obj_age         ) \
    X( origin,      60,     "ORIGIN",     "",                   print_obj_origin      )

#define TOP_HID_COLS                                                                    \
    /* name,        width,  header left,  header right,         string function */      \
    X( hid,         16,     "HID",        "",                   print_hid_hid         ) \
    X( number,      7,      "OBJS",       "",                   print_hid_objs        ) \
    X( size,        12,     "SIZE",       "",                   print_hid_size        ) \
    X( mem,         10,     "MEM",        "%res",               print_hid_mem         ) \
    X( acc,         19,     "ACC",        "cnt, evt",           print_hid_acc         ) \
    X( heat,        6,      "HEAT",       "",                   print_hid_heat        )


#define X(nm, wd, hl, hr, fn) PROC_COL_##nm,
enum { TOP_PROC_COLS N_PROC_COLS };
#undef X

static int proc_widths[] = {
#define X(nm, wd, hl, hr, fn) wd,
    TOP_PROC_COLS
#undef X
};

static const char *proc_headers_left[] = {
#define X(nm, wd, hl, hr, fn) hl,
    TOP_PROC_COLS
#undef X
};

static const char *proc_headers_right[] = {
#define X(nm, wd, hl, hr, fn) hr,
    TOP_PROC_COLS
#undef X
};

#define X(nm, wd, hl, hr, fn) OBJ_COL_##nm,
enum { TOP_OBJ_COLS N_OBJ_COLS };
#undef X

static int obj_widths[] = {
#define X(nm, wd, hl, hr, fn) wd,
    TOP_OBJ_COLS
#undef X
};

static const char *obj_headers_left[] = {
#define X(nm, wd, hl, hr, fn) hl,
    TOP_OBJ_COLS
#undef X
};

static const char *obj_headers_right[] = {
#define X(nm, wd, hl, hr, fn) hr,
    TOP_OBJ_COLS
#undef X
};

#define X(nm, wd, hl, hr, fn) HID_COL_##nm,
enum { TOP_HID_COLS N_HID_COLS };
#undef X

static int hid_widths[] = {
#define X(nm, wd, hl, hr, fn) wd,
    TOP_HID_COLS
#undef X
};

static const char *hid_headers_left[] = {
#define X(nm, wd, hl, hr, fn) hl,
    TOP_HID_COLS
#undef X
};

static const char *hid_headers_right[] = {
#define X(nm, wd, hl, hr, fn) hr,
    TOP_HID_COLS
#undef X
};

static char *print_proc_pid(md_proc *proc) {
    sprintf(scratch_buff, " %7d ", proc->pid);
    return scratch_buff;
}

static char *print_proc_cmd(md_proc *proc) {
    int   max;
    int   len;
    char *dots;
    int   print_len;
    char *s;

    max = proc_widths[PROC_COL_cmd] - 1;
    len = strlen(proc->cmd_buff);

    if (len <= max) {
        dots = "";
        s    = proc->cmd_buff;
    } else {
        dots = "...";
        s    = proc->cmd_buff + len - (max - strlen(dots));
    }

    print_len = max - strlen(dots);

    sprintf(scratch_buff, " %s%-*s", dots, print_len, s);

    return scratch_buff;
}

static char *print_proc_numobjects(md_proc *proc) {
    sprintf(scratch_buff, " %5lu ", tree_len(proc->object_map));
    return scratch_buff;
}

static char *print_proc_mem(md_proc *proc) {
    u64               total_pages;
    u64               total_pages_ddr;
    u64               total_pages_pmm;
    u64               total_pages_unknown;
    md_object_map_it  obj_it;
    md_object        *obj;
    u64               obj_pages;
    u64               total_tracked_pages;
    float             perc_ddr_pages;

    total_pages         =
    total_pages_ddr     =
    total_pages_pmm     =
    total_pages_unknown = 0;

    tree_traverse(proc->object_map, obj_it) {
        obj = &tree_it_val(obj_it);

        obj_pages = get_object_capacity_estimate(obj) / PAGE_SIZE;

        total_pages += obj_pages;

        if (obj->current_node == DDR_NODE) {
            total_pages_ddr += obj_pages;
        } else if (obj->current_node == PMM_NODE) {
            total_pages_pmm += obj_pages;
        } else {
            total_pages_unknown += obj_pages;
        }
    }

    total_tracked_pages = total_pages_ddr + total_pages_pmm;
    perc_ddr_pages      = total_tracked_pages > 0
                            ? 100.0 * ((float)total_pages_ddr / (float)total_tracked_pages)
                            : 0.0;

    sprintf(scratch_buff, "%8.1fMB, (%3d/%-3d), %3d%%",
            pages_to_MB(total_pages),
            (int)perc_ddr_pages,
            total_tracked_pages > 0 ? 100 - (int)perc_ddr_pages : 0,
            total_pages > 0 ? (int)(100.0 * ((float)(total_pages - total_pages_unknown)/(float)(total_pages))): 0);

    return scratch_buff;
}

static char *print_proc_acc(md_proc *proc) {
    md_object_map_it  obj_it;
    md_object        *obj;
    u64               total_accesses[NR_NUMA_NODES];
    u64               total_acc;
    u64               total_untracked_acc;
    float             perc_ddr_acc;

    total_accesses[DDR_NODE] = total_accesses[PMM_NODE] = 0;

    tree_traverse(proc->object_map, obj_it) {
        obj = &tree_it_val(obj_it);

        total_accesses[DDR_NODE] += obj->llc_misses[DDR_NODE];
        total_accesses[PMM_NODE] += obj->llc_misses[PMM_NODE];
    }

    total_acc             = total_accesses[DDR_NODE]             + total_accesses[PMM_NODE];
    total_untracked_acc   = proc->untracked_llc_misses[DDR_NODE] + proc->untracked_llc_misses[PMM_NODE];
    perc_ddr_acc          = total_acc > 0
                                ? 100.0 * ((float)total_accesses[DDR_NODE] / (float)total_acc)
                                : 100.0;

    sprintf(scratch_buff, "%6.1fK, (%3d/%-3d), %3d%%",
            ((float)total_acc)/1000.0,
            total_acc > 0 ? (int)perc_ddr_acc : 0,
            total_acc > 0 ? 100 - (int)perc_ddr_acc : 0,
            total_acc > 0 ? (int)(100.0 * ((float)total_acc/(float)(total_acc + total_untracked_acc))): 0);

    return scratch_buff;
}

static char *print_proc_ratio(md_proc *proc) {
    float ratio;

    ratio = get_proc_event_ratio(proc);

    sprintf(scratch_buff, "%4.8f", ratio);

    return scratch_buff;
}

static char *print_obj_hid(md_proc *proc, md_object *obj) {
    tree_it(u32, md_string) it;
    it = tree_lookup(proc->hid_string, obj->hid);
    if(tree_it_good(it)){
        sprintf(scratch_buff, " %s ", tree_it_val(it));
    }
    else{
        sprintf(scratch_buff, " %d ", obj->hid);
    }
    return scratch_buff;
}

static char *print_obj_range(md_proc *proc, md_object *obj) {
    sprintf(scratch_buff, " 0x%lx ", obj->start);
    return scratch_buff;
}

static char *print_obj_size(md_proc *proc, md_object *obj) {
    sprintf(scratch_buff, " %6.2fMB ", pages_to_MB((obj->end - obj->start) / PAGE_SIZE));
    return scratch_buff;
}

static char *print_obj_mem(md_proc *proc, md_object *obj) {
    u64   total_alloc[NR_NUMA_NODES];
    u64   total_free[NR_NUMA_NODES];
    u64   total_pages_ddr;
    u64   total_pages_pmm;
    u64   total_pages;
    float perc_resident_pages;

    total_alloc[DDR_NODE] = 0; // obj->alloc_pages[DDR_NODE];
    total_free[DDR_NODE]  = 0; // obj->free_pages[DDR_NODE];
    total_alloc[PMM_NODE] = 0; // obj->alloc_pages[PMM_NODE];
    total_free[PMM_NODE]  = 0; // obj->free_pages[PMM_NODE];
    total_pages_ddr       = 0; // obj->present_pages[DDR_NODE];
    total_pages_pmm       = 0; // obj->present_pages[PMM_NODE];

    total_pages           = total_pages_ddr + total_pages_pmm;
    perc_resident_pages   = total_pages > 0
                                ? 100.0 * ((float)total_pages / (float)((obj->end - obj->start) / PAGE_SIZE))
                                : 0.0;
    perc_resident_pages   = MIN(100.0, perc_resident_pages);

    sprintf(scratch_buff, "%3.2f%%, %d ",
            perc_resident_pages,
            obj->current_node);

    return scratch_buff;
}

static char *print_obj_acc(md_proc *proc, md_object *obj) {
    u64   total_accesses[NR_NUMA_NODES];
    u64   total_acc;
    float perc_ddr_acc;

    total_accesses[DDR_NODE] = obj->llc_misses[DDR_NODE];
    total_accesses[PMM_NODE] = obj->llc_misses[PMM_NODE];

    total_acc             = total_accesses[DDR_NODE]             + total_accesses[PMM_NODE];
    perc_ddr_acc          = total_acc > 0
                                ? 100.0 * ((float)total_accesses[DDR_NODE] / (float)total_acc)
                                : 100.0;

    sprintf(scratch_buff, "%6.1fK, (%3d/%-3d) ",
            ((float)total_acc)/1000.0,
            total_acc > 0 ? (int)perc_ddr_acc : 0,
            total_acc > 0 ? 100 - (int)perc_ddr_acc : 0);

    return scratch_buff;
}

static char *print_obj_heat(md_proc *proc, md_object *obj) {
    float             rel_heat;
    enum numa_node_id node;
    u32               shade_idx;

    rel_heat = 0.0;

    if (proc->mdtop_max_scaled_acc > 0.0) {
        for (node = 0; node < NR_NUMA_NODES; node++) {
            rel_heat += obj->llc_misses_scaled[node];
        }

        rel_heat /= proc->mdtop_max_scaled_acc;
    }

    rel_heat = MIN(1.0, rel_heat);

    shade_idx = MIN(N_SHADES - 1, (u32)(rel_heat * (N_SHADES)));

    sprintf(col_fmt, "%s", shades[shade_idx]);

    sprintf(scratch_buff, "%1.2f ", rel_heat);

    return scratch_buff;
}

static char *print_obj_active(md_proc *proc, md_object *obj) {
    float rel_active;
    u32   shade_idx;

    rel_active = 0.0;
    if (proc->mdtop_max_stale_tm_ns > 0) {
        rel_active =   1.0
                    - ((float)(proc->mdtop_timestamp_ns - obj->last_access_time_ns)
                       / (float)1000000000ULL);
    }

    rel_active = MIN(1.0, rel_active);
    rel_active = MAX(0.0, rel_active);

    shade_idx = MIN(N_SHADES - 1, (u32)(rel_active * (N_SHADES)));

    sprintf(col_fmt, "%s", shades[shade_idx]);

    sprintf(scratch_buff, " %1.2f ", rel_active);

    return scratch_buff;
}

static char *print_obj_age(md_proc *proc, md_object *obj) {
    float rel_age;
    u32   shade_idx;

    rel_age = 0.0;
    if (proc->mdtop_max_stale_tm_ns > 0) {
        rel_age =   (float)(proc->mdtop_timestamp_ns - obj->creation_time_ns)
                  / (float)proc->mdtop_max_age_ns;
    }

    rel_age = MIN(1.0, rel_age);
    rel_age = MAX(0.0, rel_age);

    shade_idx = MIN(N_SHADES - 1, (u32)(rel_age * (N_SHADES)));

    sprintf(col_fmt, "%s", shades[shade_idx]);

    sprintf(scratch_buff, " %1.2f", rel_age);

    return scratch_buff;
}

static char *print_obj_origin(md_proc *proc, md_object *obj) {
#if OBJECT_ORIGIN_DETAIL
    if (obj->origin == NULL) {
        return "<unknown>";
    }

    if (obj->origin->line_number > 0) {
        sprintf(scratch_buff,
                " %s() at %s:%lu ",
                obj->origin->func_name,
                obj->origin->file_name,
                obj->origin->line_number);
    } else {
        sprintf(scratch_buff,
                " %s() in %s ",
                obj->origin->func_name,
                obj->origin->file_name);
    }
    return scratch_buff;
#elif OBJECT_ORIGIN
    sprintf(scratch_buff,
            " 0x%p ",
            (void*)obj->origin_inst_addr);
    return scratch_buff;
#else
    return "<unknown>";
#endif
}

static char *print_hid_hid(u32 hid, array_t arr){
    tree_it(u32, md_string) it;
    it = tree_lookup(gproc->hid_string, hid);
    if(tree_it_good(it)){
        sprintf(scratch_buff, " %s ", tree_it_val(it));
    }
    else{
        sprintf(scratch_buff, " %d ", hid);
    }
    return scratch_buff;
}

static char *print_hid_objs(u32 hid, array_t arr){
    sprintf(scratch_buff, " %d ", array_len(arr));
    return scratch_buff;
}

static char *print_hid_size(u32 hid, array_t arr){
    u64 total_num_pages = 0;
    md_object** object_it;
    array_traverse(arr, object_it){
        total_num_pages += ((*object_it)->end - (*object_it)->start) / PAGE_SIZE;
    }
    sprintf(scratch_buff, " %6.2fMB ", pages_to_MB(total_num_pages));
    return scratch_buff;
}

static char *print_hid_mem(u32 hid, array_t arr){
    u64 total_pages_ddr;
    u64 total_pages_pmm;
    u64 total_pages;
    u64 ranges = 0;
    float perc_resident_pages;

    total_pages_ddr = 0;
    total_pages_pmm = 0;
    total_pages = 0;

    md_object** object_it;
    array_traverse(arr, object_it){
        total_pages_ddr = 0; // (*object_it)->present_pages[DDR_NODE];
        total_pages_pmm = 0; // (*object_it)->present_pages[PMM_NODE];
        ranges += ((*object_it)->end - (*object_it)->start) / PAGE_SIZE;
        total_pages += total_pages_ddr + total_pages_pmm;
    }
    perc_resident_pages   = total_pages > 0
                                ? 100.0 * ((float)total_pages / (float)ranges)
                                : 0.0;
    perc_resident_pages   = MIN(100.0, perc_resident_pages);

    sprintf(scratch_buff, "%3.2f%%",
            perc_resident_pages
            );
    return scratch_buff;
}

static char *print_hid_acc(u32 hid, array_t arr){
    u64 total_accesses[NR_NUMA_NODES];
    u64 total_acc = 0;
    float perc_ddr_acc;

    total_accesses[DDR_NODE] =
    total_accesses[PMM_NODE] = 0;

    md_object** object_it;
    array_traverse(arr, object_it){
        total_accesses[DDR_NODE] += (*object_it)->llc_misses[DDR_NODE];
        total_accesses[PMM_NODE] += (*object_it)->llc_misses[PMM_NODE];

    }

    total_acc = total_accesses[DDR_NODE] + total_accesses[PMM_NODE];
    perc_ddr_acc          = total_acc > 0
                                ? 100.0 * ((float)total_accesses[DDR_NODE] / (float)total_acc)
                                : 100.0;

    sprintf(scratch_buff, "%6.1fK, (%3d/%-3d) ",
            ((float)total_acc)/1000.0,
            total_acc > 0 ? (int)perc_ddr_acc : 0,
            total_acc > 0 ? 100 - (int)perc_ddr_acc : 0);

    return scratch_buff;
}


void fill_heat_tree(tree(u32, array_t) t){
    tree_it(u32, array_t) it;
    float rel_heat = 0.0;
    md_object** object_it;
    tree_traverse(t, it){
        array_traverse(tree_it_val(it), object_it){
            rel_heat += (*object_it)->llc_misses_scaled[DDR_NODE];
            rel_heat += (*object_it)->llc_misses_scaled[PMM_NODE];
        }
        tree_insert(heat, tree_it_key(it), rel_heat);
    }
}

static char *print_hid_heat(u32 hid, array_t arr){
    float rel_heat;
    u32 shade_idx;
    float max_heat;
    max_heat = 0.0;
    tree_it(u32, float) it = tree_lookup(heat, hid);
    rel_heat = tree_it_val(it);
    tree_it(u32, float) heat_it;
    tree_traverse(heat, heat_it){
        if(tree_it_val(heat_it) > max_heat){
            max_heat = tree_it_val(heat_it);
        }
    }

    rel_heat /= max_heat;

    rel_heat = MIN(1.0, rel_heat);

    shade_idx = MIN(N_SHADES - 1, (u32)(rel_heat * (N_SHADES)));

    sprintf(col_fmt, "%s", shades[shade_idx]);

    sprintf(scratch_buff, "%1.2f ", rel_heat);

    return scratch_buff;

}

static const proc_print_fn_t proc_print_fns[] = {
#define X(nm, wd, hl, hr, fn) fn,
    TOP_PROC_COLS
#undef X
};

static const obj_print_fn_t obj_print_fns[] = {
#define X(nm, wd, hl, hr, fn) fn,
    TOP_OBJ_COLS
#undef X
};

static const hid_print_fn_t hid_print_fns[] = {
#define X(nm, wd, hl, hr, fn) fn,
    TOP_HID_COLS
#undef X
};


static void append_to_output_buff(const char *s) {
    int len;

    len = strlen(s);

    array_push_n(output_buff, (char*)s, len);
}

static void flush_output_buff(void) {
    int n_wr;

    n_wr = 0;

    while ((n_wr += write(1, array_data(output_buff) + n_wr, array_len(output_buff) - n_wr))
            < array_len(output_buff)) {}

    array_clear(output_buff);
}



static struct termios save_term;
static struct termios new_term;

static void init_mdtop(void) {
    if (!mdtop_initialized) {
        fflush(stdout);
        heat = tree_make(u32, float);
        tcgetattr(0, &save_term);
        new_term = save_term;

        /* control modes - set 8 bit chars */
        new_term.c_cflag |= (CS8);
        /* local modes - choing off, canonical off, no extended functions */
        new_term.c_lflag &= ~(ECHO | ICANON | IEXTEN);

        /* Return each byte, or zero for timeout. */
        new_term.c_cc[VMIN] = 0;
        /* 300 ms timeout (unit is tens of second). */
        new_term.c_cc[VTIME] = 1;

        tcsetattr(0, TCSAFLUSH, &new_term);

        output_buff = array_make(char);

        TERM_ALT_SCREEN();
        CURSOR_HIDE();
        CLEAR_SCREEN();
        CURSOR_HOME();

        flush_output_buff();

        mdtop_initialized = 1;
    }
}

void mdtop_cleanup(void) {
    if (mdtop_initialized) {
        mdtop_initialized = 0;

        TERM_STD_SCREEN();
        CURSOR_SHOW();
        flush_output_buff();

        tcsetattr(0, TCSAFLUSH, &save_term);
        INFO("Restored terminal settings.\n");
    }
}


static void print_proc_header(void) {
    int         col;
    const char *color;
    int         width;
    int         max_len;
    char        buff[128];
    int         i;
    int         j;

    j = 0;
    while(j != col_selected){
        for(i = 0; i < proc_widths[j]; i++){
            append_to_output_buff(" ");
        }
        j++;
    }
    append_to_output_buff("   V \n");
    append_to_output_buff("  ");

    for (col = 0; col < N_PROC_COLS; col += 1) {
        color   = col & 1 ? COLOR2 : COLOR1;
        width   = proc_widths[col];
        max_len = proc_widths[col]
                    + strlen(color)
                    + strlen(BOLD)
                    + strlen(RESET)
                    + strlen(color)
                    + strlen(RESET);

        snprintf(buff, max_len,
                 "%s " BOLD "%s" RESET "%s %*s" RESET,
                 color,
                 proc_headers_left[col],
                 color,
                 (int)MAX(0, width - strlen(proc_headers_left[col]) - 2),
                 proc_headers_right[col]);

        append_to_output_buff(buff);
    }

    append_to_output_buff(RESET "\n");

    append_to_output_buff("  ");

    for (col = 0; col < N_PROC_COLS; col += 1) {
        width = proc_widths[col];
        color = col & 1 ? COLOR2 : COLOR1;
        append_to_output_buff(color);
        for (i = 0; i < width; i += 1) {
            append_to_output_buff("─");
        }
        append_to_output_buff(RESET);
    }

    append_to_output_buff(RESET "\n");
}

static void print_proc_row(md_proc *proc, int selected) {
    const char *c1;
    const char *c2;
    const char *inactive;
    int         col;
    const char *color;
    int         width;
    int         max_len;
    const char *str;
    char        buff[128];

    c1       = proc->mdtop_did_migrate ? COLOR3 : COLOR1;
    c2       = proc->mdtop_did_migrate ? COLOR4 : COLOR2;
    inactive = proc->active            ? ""     : GREY;

    if (selected) {
        append_to_output_buff("> ");
    } else {
        append_to_output_buff("  ");
    }

    for (col = 0; col < N_PROC_COLS; col += 1) {
        color   = col & 1 ? c2 : c1;
        width   = proc_widths[col];
        max_len = proc_widths[col]
                    + strlen(color)
                    + strlen(inactive)
                    + strlen(RESET);

        str = proc_print_fns[col](proc);

        snprintf(buff, max_len,
                 "%s%s%*s" RESET,
                 color,
                 inactive,
                 width,
                 str);

        append_to_output_buff(buff);
    }

    append_to_output_buff(RESET "\n");
}

static void print_obj_header(void) {
    int         col;
    const char *color;
    int         width;
    int         max_len;
    char        buff[128];
    int         i;
    int         j;

    j = 0;
    while(j != obj_col_selected){
        for(i = 0; i < obj_widths[j]; i++){
            append_to_output_buff(" ");
        }
        j++;
    }
    append_to_output_buff("      V \n");

    append_to_output_buff("    " COLOR5 " ");

    for (col = 0; col < N_OBJ_COLS; col += 1) {
        color   = col & 1 ? COLOR6 : COLOR5;
        width   = obj_widths[col];
        max_len = obj_widths[col]
                    + strlen(color)
                    + strlen(BOLD)
                    + strlen(RESET)
                    + strlen(color)
                    + strlen(RESET);

        snprintf(buff, max_len,
                 "%s " BOLD "%s" RESET "%s %*s" RESET,
                 color,
                 obj_headers_left[col],
                 color,
                 (int)MAX(0, width - strlen(obj_headers_left[col]) - 2),
                 obj_headers_right[col]);

        append_to_output_buff(buff);
    }

    append_to_output_buff(RESET "\n");

    append_to_output_buff("    " COLOR5 " ");

    for (col = 0; col < N_OBJ_COLS; col += 1) {
        color = col & 1 ? COLOR6 : COLOR5;
        width = obj_widths[col];
        append_to_output_buff(color);
        for (i = 0; i < width; i += 1) {
            append_to_output_buff("─");
        }
        append_to_output_buff(RESET);
    }

    append_to_output_buff(RESET "\n");
}

static void print_obj_row(md_proc *proc, md_object *obj, int draw_scroll) {
    const char *inactive;
    int         col;
    const char *color;
    int         width;
    int         max_len;
    const char *str;
    char        buff[128];

    inactive = proc->active ? "" : GREY;

    if (draw_scroll) {
        append_to_output_buff("    " ORANGE_BG " ");
    } else {
        append_to_output_buff("    " GREY_BG " ");
    }

    for (col = 0; col < N_OBJ_COLS; col += 1) {
        color   = col & 1 ? COLOR6 : COLOR5;
        width   = obj_widths[col];
        max_len = obj_widths[col]
                    + strlen(color)
                    + strlen(inactive)
                    + strlen(RESET);

        col_fmt[0] = 0;

        str = obj_print_fns[col](proc, obj);

        max_len += strlen(col_fmt);

        snprintf(buff, max_len,
                 "%s%s%s%*s" RESET,
                 color,
                 col_fmt,
                 inactive,
                 width,
                 str);

        append_to_output_buff(buff);
    }

    append_to_output_buff(RESET "\n");
}

static void print_empty_obj_row(int draw_scroll) {
    int         col;
    const char *color;
    int         width;
    int         i;

    if (draw_scroll) {
        append_to_output_buff("    " ORANGE_BG " ");
    } else {
        append_to_output_buff("    " GREY_BG " ");
    }

    for (col = 0; col < N_OBJ_COLS; col += 1) {
        color = col & 1 ? COLOR6 : COLOR5;
        width = obj_widths[col];
        append_to_output_buff(color);
        for (i = 0; i < width; i += 1) {
            append_to_output_buff(" ");
        }
        append_to_output_buff(RESET);
    }

    append_to_output_buff(RESET "\n");
}

static void print_hid_header(void) {
    int         col;
    const char *color;
    int         width;
    int         max_len;
    char        buff[128];
    int         i;
/*     int         j; */

    append_to_output_buff("    " COLOR5 " ");

    for (col = 0; col < N_HID_COLS; col += 1) {
        color   = col & 1 ? COLOR6 : COLOR5;
        width   = hid_widths[col];
        max_len = hid_widths[col]
                    + strlen(color)
                    + strlen(BOLD)
                    + strlen(RESET)
                    + strlen(color)
                    + strlen(RESET);

        snprintf(buff, max_len,
                 "%s " BOLD "%s" RESET "%s %*s" RESET,
                 color,
                 hid_headers_left[col],
                 color,
                 (int)MAX(0, width - strlen(hid_headers_left[col]) - 2),
                 hid_headers_right[col]);

        append_to_output_buff(buff);
    }

    append_to_output_buff(RESET "\n");

    append_to_output_buff("    " COLOR5 " ");

    for (col = 0; col < N_HID_COLS; col += 1) {
        color = col & 1 ? COLOR6 : COLOR5;
        width = hid_widths[col];
        append_to_output_buff(color);
        for (i = 0; i < width; i += 1) {
            append_to_output_buff("─");
        }
        append_to_output_buff(RESET);
    }

    append_to_output_buff(RESET "\n");
}

static void print_hid_row(u32 hid, array_t arr, int draw_scroll) {
    const char *inactive;
    int         col;
    const char *color;
    int         width;
    int         max_len;
    const char *str;
    char        buff[128];

    inactive = gproc->active ? "" : GREY;

    if (draw_scroll) {
        append_to_output_buff("    " ORANGE_BG " ");
    } else {
        append_to_output_buff("    " GREY_BG " ");
    }

    for (col = 0; col < N_HID_COLS; col += 1) {
        color   = col & 1 ? COLOR6 : COLOR5;
        width   = hid_widths[col];
        max_len = hid_widths[col]
                    + strlen(color)
                    + strlen(inactive)
                    + strlen(RESET);

        col_fmt[0] = 0;

        str = hid_print_fns[col](hid, arr);

        max_len += strlen(col_fmt);

        snprintf(buff, max_len,
                 "%s%s%s%*s" RESET,
                 color,
                 col_fmt,
                 inactive,
                 width,
                 str);

        append_to_output_buff(buff);
    }

    append_to_output_buff(RESET "\n");
}
static void print_empty_hid_row(int draw_scroll) {
    int         col;
    const char *color;
    int         width;
    int         i;

    if (draw_scroll) {
        append_to_output_buff("    " ORANGE_BG " ");
    } else {
        append_to_output_buff("    " GREY_BG " ");
    }

    for (col = 0; col < N_HID_COLS; col += 1) {
        color = col & 1 ? COLOR6 : COLOR5;
        width = hid_widths[col];
        append_to_output_buff(color);
        for (i = 0; i < width; i += 1) {
            append_to_output_buff(" ");
        }
        append_to_output_buff(RESET);
    }

    append_to_output_buff(RESET "\n");
}



int pidcmp(const void* a, const void* b){
    md_proc* val1 = *(md_proc**)a;
    md_proc* val2 = *(md_proc**)b;

    return (int)val1->pid - (int)val2->pid;
}

int memorycmp(const void* a, const void* b){
/*     md_proc* val1 = *(md_proc**)a; */
/*     md_proc* val2 = *(md_proc**)b; */

    u64 total_pages1;
    u64 total_pages2;
    u64 total_pages_ddr;
    u64 total_pages_pmm;
/*     md_object_map_it obj_it; */
/*     md_object *obj; */

    total_pages_ddr =
    total_pages_pmm = 0;

/*     tree_traverse(val1->object_map, obj_it) { */
/*         obj = &tree_it_val(obj_it); */
/*         total_pages_ddr       += obj->present_pages[DDR_NODE]; */
/*         total_pages_pmm       += obj->present_pages[PMM_NODE]; */
/*     } */

    total_pages1 = total_pages_ddr + total_pages_pmm;

    total_pages_ddr =
    total_pages_pmm = 0;
/*     tree_traverse(val2->object_map, obj_it) { */
/*         obj = &tree_it_val(obj_it); */
/*         total_pages_ddr       += obj->present_pages[DDR_NODE]; */
/*         total_pages_pmm       += obj->present_pages[PMM_NODE]; */
/*     } */

    total_pages2 = total_pages_ddr + total_pages_pmm;

    if(total_pages1 < total_pages2){
        return -1;
    }
    else if(total_pages1 == total_pages2){
        return 0;
    }
    else{
        return 1;
    }
}

int cmdcmp(const void* a, const void* b){

    md_proc* val1 = *(md_proc**)a;
    md_proc* val2 = *(md_proc**)b;

    return strcmp(val1->cmd_buff, val2->cmd_buff);
}

int objectcmp(const void* a, const void* b){
    md_proc* val1 = *(md_proc**)a;
    md_proc* val2 = *(md_proc**)b;

    u64 num_objs1 = tree_len(val1->object_map);
    u64 num_objs2 = tree_len(val2->object_map);

    if(num_objs1 < num_objs2){
        return -1;
    }
    else if(num_objs1 == num_objs2){
        return 0;
    }
    else{
        return 1;
    }

}

int accesscmp(const void* a, const void* b){
    md_proc* val1 = *(md_proc**)a;
    md_proc* val2 = *(md_proc**)b;

    u64 total_acc1 = 0;
    u64 total_acc2 = 0;
    u64 total_accesses[NR_NUMA_NODES];
    md_object_map_it obj_it;
    md_object *obj;

    total_accesses[DDR_NODE] =
    total_accesses[PMM_NODE] = 0;

    tree_traverse(val1->object_map, obj_it) {
        obj = &tree_it_val(obj_it);
        total_accesses[DDR_NODE]       += obj->llc_misses[DDR_NODE];
        total_accesses[PMM_NODE]       += obj->llc_misses[PMM_NODE];
    }

    total_acc1 = total_accesses[DDR_NODE] + total_accesses[PMM_NODE];

    total_accesses[DDR_NODE] =
    total_accesses[PMM_NODE] = 0;
    tree_traverse(val2->object_map, obj_it) {
        obj = &tree_it_val(obj_it);
        total_accesses[DDR_NODE]       += obj->llc_misses[DDR_NODE];
        total_accesses[PMM_NODE]       += obj->llc_misses[PMM_NODE];
    }

    total_acc1 = total_accesses[DDR_NODE] + total_accesses[PMM_NODE];

    if(total_acc1 < total_acc2){
        return -1;
    }
    else if(total_acc1 == total_acc2){
        return 0;
    }
    else{
        return 1;
    }

}
int r_pidcmp(const void* a, const void* b){
    md_proc* val1 = *(md_proc**)a;
    md_proc* val2 = *(md_proc**)b;

    return (int)val2->pid - (int)val1->pid;
}

int r_memorycmp(const void* a, const void* b){
/*     md_proc* val1 = *(md_proc**)a; */
/*     md_proc* val2 = *(md_proc**)b; */

    u64 total_pages1;
    u64 total_pages2;
    u64 total_pages_ddr;
    u64 total_pages_pmm;
/*     md_object_map_it obj_it; */
/*     md_object *obj; */

    total_pages_ddr =
    total_pages_pmm = 0;

/*     tree_traverse(val1->object_map, obj_it) { */
/*         obj = &tree_it_val(obj_it); */
/*         total_pages_ddr       += obj->present_pages[DDR_NODE]; */
/*         total_pages_pmm       += obj->present_pages[PMM_NODE]; */
/*     } */

    total_pages1 = total_pages_ddr + total_pages_pmm;

    total_pages_ddr =
    total_pages_pmm = 0;
/*     tree_traverse(val2->object_map, obj_it) { */
/*         obj = &tree_it_val(obj_it); */
/*         total_pages_ddr       += obj->present_pages[DDR_NODE]; */
/*         total_pages_pmm       += obj->present_pages[PMM_NODE]; */
/*     } */

    total_pages2 = total_pages_ddr + total_pages_pmm;

    if(total_pages1 < total_pages2){
        return 1;
    }
    else if(total_pages1 == total_pages2){
        return 0;
    }
    else{
        return -1;
    }
}

int r_cmdcmp(const void* a, const void* b){

    md_proc* val1 = *(md_proc**)a;
    md_proc* val2 = *(md_proc**)b;

    return strcmp(val2->cmd_buff, val1->cmd_buff);
}

int r_objectcmp(const void* a, const void* b){
    md_proc* val1 = *(md_proc**)a;
    md_proc* val2 = *(md_proc**)b;

    u64 num_objs1 = tree_len(val1->object_map);
    u64 num_objs2 = tree_len(val2->object_map);

    if(num_objs1 < num_objs2){
        return 1;
    }
    else if(num_objs1 == num_objs2){
        return 0;
    }
    else{
        return -1;
    }

}

int r_accesscmp(const void* a, const void* b){
    md_proc* val1 = *(md_proc**)a;
    md_proc* val2 = *(md_proc**)b;

    u64 total_acc1 = 0;
    u64 total_acc2 = 0;
    u64 total_accesses[NR_NUMA_NODES];
    md_object_map_it obj_it;
    md_object *obj;

    total_accesses[DDR_NODE] =
    total_accesses[PMM_NODE] = 0;

    tree_traverse(val1->object_map, obj_it) {
        obj = &tree_it_val(obj_it);
        total_accesses[DDR_NODE]       += obj->llc_misses[DDR_NODE];
        total_accesses[PMM_NODE]       += obj->llc_misses[PMM_NODE];
    }

    total_acc1 = total_accesses[DDR_NODE] + total_accesses[PMM_NODE];

    total_accesses[DDR_NODE] =
    total_accesses[PMM_NODE] = 0;
    tree_traverse(val2->object_map, obj_it) {
        obj = &tree_it_val(obj_it);
        total_accesses[DDR_NODE]       += obj->llc_misses[DDR_NODE];
        total_accesses[PMM_NODE]       += obj->llc_misses[PMM_NODE];
    }

    total_acc1 = total_accesses[DDR_NODE] + total_accesses[PMM_NODE];

    if(total_acc1 < total_acc2){
        return 1;
    }
    else if(total_acc1 == total_acc2){
        return 0;
    }
    else{
        return -1;
    }

}

int hidcmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    return val1->hid - val2->hid;
}

int r_hidcmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    return val2->hid - val1->hid;
}


int startcmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    if(val1->start < val2->start){
        return -1;
    }
    else if(val1->start == val2->start){
        return 0;
    }
    else{
        return 1;
    }
}

int sizecmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    if(((val1->end - val1->start) / PAGE_SIZE) < ((val2->end - val2->start) / PAGE_SIZE)){
        return -1;
    }
    if(((val1->end - val1->start) / PAGE_SIZE) == ((val2->end - val2->start) / PAGE_SIZE)){
        return 0;
    }
    else{
        return 1;
    }
}

int objmemcmp(const void* a, const void* b){
/*     md_object* val1 = *(md_object**)a; */
/*     md_object* val2 = *(md_object**)b; */

    u64 total_pages1;
    u64 total_pages2;

    total_pages1 =
    total_pages2 = 0;
    total_pages1 = 0; // val1->present_pages[DDR_NODE] + val1->present_pages[PMM_NODE];
    total_pages2 = 0; //val2->present_pages[DDR_NODE] + val2->present_pages[PMM_NODE];

    if (total_pages1 < total_pages2){
        return -1;
    }
    else if(total_pages1 == total_pages2){
        return 0;
    }
    else{
        return 1;
    }

}

int objacccmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    u64 total_acc1;
    u64 total_acc2;

    total_acc1 = val1->llc_misses[DDR_NODE] + val1->llc_misses[PMM_NODE];
    total_acc2 = val2->llc_misses[DDR_NODE] + val2->llc_misses[PMM_NODE];

    if (total_acc1 < total_acc2){
        return -1;
    }
    else if(total_acc1 == total_acc2){
        return 0;
    }
    else{
        return 1;
    }
}

int heatcmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    float rel_heat1;
    float rel_heat2;

    rel_heat1 = val1->llc_misses_scaled[DDR_NODE] + val1->llc_misses_scaled[PMM_NODE];
    rel_heat2 = val2->llc_misses_scaled[DDR_NODE] + val2->llc_misses_scaled[PMM_NODE];

    if (rel_heat1 < rel_heat2){
        return -1;
    }
    else if(rel_heat1 == rel_heat2){
        return 0;
    }
    else{
        return 1;
    }
}

int activecmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;
    float rel_active1;
    float rel_active2;

    rel_active1 =
    rel_active2 = 0;


    if (gproc->mdtop_max_stale_tm_ns > 0) {
        rel_active1 =   1.0
                    - ((float)(gproc->mdtop_timestamp_ns - val1->last_access_time_ns)
                       / (float)1000000000ULL);
        rel_active2 =   1.0
                    - ((float)(gproc->mdtop_timestamp_ns - val2->last_access_time_ns)
                       / (float)1000000000ULL);
    }
    rel_active1 = MIN(1.0, rel_active1);
    rel_active1 = MAX(0.0, rel_active1);

    rel_active2 = MIN(1.0, rel_active2);
    rel_active2 = MAX(0.0, rel_active2);

    if(rel_active1 < rel_active2){
        return -1;
    }
    else if(rel_active1 == rel_active2){
        return 0;
    }
    else{
        return 1;
    }
}
int agecmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    float rel_age1;
    float rel_age2;

    rel_age1 =
    rel_age2 = 0;

    if (gproc->mdtop_max_stale_tm_ns > 0) {
        rel_age1 =   (float)(gproc->mdtop_timestamp_ns - val1->creation_time_ns)
                  / (float)gproc->mdtop_max_age_ns;
        rel_age2 =   (float)(gproc->mdtop_timestamp_ns - val2->creation_time_ns)
                  / (float)gproc->mdtop_max_age_ns;
    }

    rel_age1 = MIN(1.0, rel_age1);
    rel_age1 = MAX(0.0, rel_age1);

    rel_age2 = MIN(1.0, rel_age2);
    rel_age2 = MAX(0.0, rel_age2);

    if(rel_age1 < rel_age2){
        return -1;
    }
    else if(rel_age1 == rel_age2){
        return 0;
    }
    else{
        return 1;
    }
}

int r_startcmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    if(val1->start < val2->start){
        return 1;
    }
    else if(val1->start == val2->start){
        return 0;
    }
    else{
        return -1;
    }
}

int r_sizecmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    if(((val1->end - val1->start) / PAGE_SIZE) < ((val2->end - val2->start) / PAGE_SIZE)){
        return 1;
    }
    if(((val1->end - val1->start) / PAGE_SIZE) == ((val2->end - val2->start) / PAGE_SIZE)){
        return 0;
    }
    else{
        return -1;
    }
}

int r_objmemcmp(const void* a, const void* b){
/*     md_object* val1 = *(md_object**)a; */
/*     md_object* val2 = *(md_object**)b; */

    u64 total_pages1;
    u64 total_pages2;

    total_pages1 =
    total_pages2 = 0;
    total_pages1 = 0; // val1->present_pages[DDR_NODE] + val1->present_pages[PMM_NODE];
    total_pages2 = 0; // val2->present_pages[DDR_NODE] + val2->present_pages[PMM_NODE];

    if (total_pages1 < total_pages2){
        return 1;
    }
    else if(total_pages1 == total_pages2){
        return 0;
    }
    else{
        return -1;
    }

}

int r_objacccmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    u64 total_acc1;
    u64 total_acc2;

    total_acc1 = val1->llc_misses[DDR_NODE] + val1->llc_misses[PMM_NODE];
    total_acc2 = val2->llc_misses[DDR_NODE] + val2->llc_misses[PMM_NODE];

    if (total_acc1 < total_acc2){
        return 1;
    }
    else if(total_acc1 == total_acc2){
        return 0;
    }
    else{
        return -1;
    }
}

int r_heatcmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    float rel_heat1;
    float rel_heat2;

    rel_heat1 = val1->llc_misses_scaled[DDR_NODE] + val1->llc_misses_scaled[PMM_NODE];
    rel_heat2 = val2->llc_misses_scaled[DDR_NODE] + val2->llc_misses_scaled[PMM_NODE];

    if (rel_heat1 < rel_heat2){
        return 1;
    }
    else if(rel_heat1 == rel_heat2){
        return 0;
    }
    else{
        return -1;
    }
}

int r_activecmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;
    float rel_active1;
    float rel_active2;

    rel_active1 =
    rel_active2 = 0;


    if (gproc->mdtop_max_stale_tm_ns > 0) {
        rel_active1 =   1.0
                    - ((float)(gproc->mdtop_timestamp_ns - val1->last_access_time_ns)
                       / (float)1000000000ULL);
        rel_active2 =   1.0
                    - ((float)(gproc->mdtop_timestamp_ns - val2->last_access_time_ns)
                       / (float)1000000000ULL);
    }
    rel_active1 = MIN(1.0, rel_active1);
    rel_active1 = MAX(0.0, rel_active1);

    rel_active2 = MIN(1.0, rel_active2);
    rel_active2 = MAX(0.0, rel_active2);

    if(rel_active1 < rel_active2){
        return 1;
    }
    else if(rel_active1 == rel_active2){
        return 0;
    }
    else{
        return -1;
    }
}
int r_agecmp(const void* a, const void* b){
    md_object* val1 = *(md_object**)a;
    md_object* val2 = *(md_object**)b;

    float rel_age1;
    float rel_age2;

    rel_age1 =
    rel_age2 = 0;

    if (gproc->mdtop_max_stale_tm_ns > 0) {
        rel_age1 =   (float)(gproc->mdtop_timestamp_ns - val1->creation_time_ns)
                  / (float)gproc->mdtop_max_age_ns;
        rel_age2 =   (float)(gproc->mdtop_timestamp_ns - val2->creation_time_ns)
                  / (float)gproc->mdtop_max_age_ns;
    }

    rel_age1 = MIN(1.0, rel_age1);
    rel_age1 = MAX(0.0, rel_age1);

    rel_age2 = MIN(1.0, rel_age2);
    rel_age2 = MAX(0.0, rel_age2);

    if(rel_age1 < rel_age2){
        return 1;
    }
    else if(rel_age1 == rel_age2){
        return 0;
    }
    else{
        return -1;
    }
}

void mdtop_update(void) {
    u64               start;
    int               i;
    char              c;
    int               p;
    md_proc          *proc;
    int               n_obj;
    int               n_pages;
    int               scroll_bar_len;
    int               obj_idx;
    int               obj_row;
/*     md_object_map_it  it; */
    int               page_idx;
    int               scroll_bar_row;
    int               rows_left;
    u64               elapsed;

    (void)print_obj_origin;

    start = measure_time_now_ms();

    init_mdtop();

    if (paused) {
        while (read(1, &c, 1) && c) {
            if (((char)c) == ' ' ) {
                paused = !paused;
            }
        }
        return;
    }

    CLEAR_SCREEN();
    CURSOR_HOME();
    BEG_LINE();


    if (hash_table_len(process_table) > 0) {
        append_to_output_buff("\n");
        append_to_output_buff("    ENTER:  toggle object view\n");
        append_to_output_buff("    j, k:   move up, down\n");
        append_to_output_buff("    h, l:   change sorting property\n");
        append_to_output_buff("    /:      toggle sorting order\n");
        append_to_output_buff("    SPACE:  pause/resume\n");
        append_to_output_buff("\n");
    }

    print_proc_header();

    if (hash_table_len(process_table) == 0) {
        append_to_output_buff("\n    Waiting for processes...\n");
    }

    while (read(1, &c, 1) && c) {
        switch (c) {
            case 10:
                if(!show_objects && !show_hid){
                    show_objects = !show_objects;
                }
                else if(show_objects && !show_hid){
                    show_objects = !show_objects;
                    show_hid = !show_hid;
                }
                else{
                    show_hid = !show_hid;
                }
                break;
            case 'j':
                if (show_objects) {
                    selected_page += 1;
                } else {
                    selected_proc += 1;
                }
                break;
            case 'k':
                if (show_objects) {
                    selected_page -= 1;
                    selected_page  = MAX(0, selected_page);
                } else {
                    selected_proc -= 1;
                    selected_proc  = MAX(0, selected_proc);
                }
                break;
            case ' ':
                paused = !paused;
                break;
            case 'h':
                if (show_objects) {
                    if(obj_col_selected == 0){
                        break;
                    }
                    obj_col_selected -= 1;
                }
                else {
                    if(col_selected == 0){
                        break;
                    }
                    col_selected -= 1;
                }
                break;
            case 'l':
                if (show_objects){
                    if(obj_col_selected == N_OBJ_COLS - 1){
                        break;
                    }
                    obj_col_selected += 1;
                }
                else{
                    if(col_selected == N_PROC_COLS - 1){
                        break;
                    }
                    col_selected += 1;
                }
                break;
            case '/':
                if (show_objects){
                    reverse_obj_order = !reverse_obj_order;
                }
                else{
                    reverse_order = !reverse_order;
                }
        }
    }

    RLOCK();


    array_t proc_arr = array_make(md_proc*);
    PROCESS_FOR(proc, {
        array_push(proc_arr, proc);
    });


    switch(col_selected){
        case 0:
            if(reverse_order){
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), pidcmp);
            }
            else{
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), r_pidcmp);
            }
            break;
        case 1:
            if(reverse_order){
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), cmdcmp);
            }
            else{
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), r_cmdcmp);
            }
            break;
        case 2:
            if(reverse_order){
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), objectcmp);
            }
            else{
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), r_objectcmp);
            }
            break;
        case 3:
            if(reverse_order){
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), memorycmp);
            }
            else{
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), r_memorycmp);
            }
            break;
        case 4:
            if(reverse_order){
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), accesscmp);
            }
            else{
                qsort(array_data(proc_arr), array_len(proc_arr), sizeof(md_proc*), r_accesscmp);
            }
            break;
    }



    md_proc** proc_it;
    p = 0;
    array_traverse(proc_arr, proc_it) {
        selected_proc = MIN(selected_proc, hash_table_len(process_table) - 1);
        proc = *proc_it;
        gproc = *proc_it;
        lock_process(proc);
        print_proc_row(*proc_it, p == selected_proc);

        if (p == selected_proc) {
            if (show_objects) {
                append_to_output_buff("\n");


                print_obj_header();

                array_t obj_arr = array_make(md_object*);
                md_object_map_it obj_it;
                tree_traverse(proc->object_map, obj_it){
                    md_object *obj = &tree_it_val(obj_it);
                    array_push(obj_arr, obj);
                }

                n_obj          = tree_len(proc->object_map);
                n_pages        = MAX(1, roundf(((float)n_obj / (float)OBJ_PER_PAGE) + 0.5));
                scroll_bar_len = MAX(1, roundf((float)OBJ_PER_PAGE / (float)n_pages));
                selected_page  = MIN(selected_page, n_pages - 1);
                obj_idx        = 0;
                obj_row        = 1;

                switch(obj_col_selected){
                case 0:
                    if (reverse_obj_order){
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), hidcmp);
                    }
                    else{
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), r_hidcmp);
                    }
                    break;
                case 1:
                    if (reverse_obj_order){
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), startcmp);
                    }
                    else{
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), r_startcmp);
                    }
                    break;
                case 2:
                    if (reverse_obj_order){
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), sizecmp);
                    }
                    else{
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), r_sizecmp);
                    }
                    break;
                case 3:
                    if (reverse_obj_order){
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), objmemcmp);
                    }
                    else{
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), r_objmemcmp);
                    }
                    break;
                case 4:
                    if (reverse_obj_order){
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), objacccmp);
                    }
                    else{
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), r_objacccmp);
                    }
                    break;
                case 5:
                    if (reverse_obj_order){
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), heatcmp);
                    }
                    else{
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), r_heatcmp);
                    }
                    break;
                case 6:
                    if (reverse_obj_order){
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), activecmp);
                    }
                    else{
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), r_activecmp);
                    }
                    break;
                case 7:
                    if (reverse_obj_order){
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), agecmp);
                    }
                    else{
                        qsort(array_data(obj_arr), array_len(obj_arr), sizeof(md_object*), r_agecmp);
                    }
                    break;
                }
                md_object** object_it;
                array_traverse(obj_arr, object_it) {
                    if (obj_row <= OBJ_PER_PAGE) {
                        page_idx = obj_idx / OBJ_PER_PAGE;
                        if (page_idx == selected_page) {
                            if (page_idx >= n_pages - 1) {
                                scroll_bar_row = obj_row > (((page_idx - 1) * scroll_bar_len) + 1);
                            } else {
                                scroll_bar_row =    obj_row > (page_idx * scroll_bar_len)
                                                 && obj_row <= (page_idx * scroll_bar_len) + scroll_bar_len;
                            }

                            if (page_idx == n_pages - 1) {
                                scroll_bar_row |= obj_row > OBJ_PER_PAGE - scroll_bar_len;
                            }

                            print_obj_row(proc, *object_it, scroll_bar_row);
                            obj_row += 1;
                        }
                    }
                    obj_idx += 1;
                }
                array_free(obj_arr);
                rows_left = OBJ_PER_PAGE - obj_row;

                if (rows_left) {
                    for (i = 0; i < rows_left + 1; i += 1) {
                            if (page_idx >= n_pages - 1) {
                                scroll_bar_row = obj_row > (((page_idx - 1) * scroll_bar_len) + 1);
                            } else {
                                scroll_bar_row =    obj_row > (page_idx * scroll_bar_len)
                                                 && obj_row <= (page_idx * scroll_bar_len) + scroll_bar_len;
                            }

                            print_empty_obj_row(scroll_bar_row);
                            obj_row += 1;
                    }
                }

                append_to_output_buff("\n");
            }
          else if(show_hid){
                append_to_output_buff("\n");

                print_hid_header();

                tree(u32, array_t) hidtree = tree_make(u32, array_t);
                md_object_map_it obj_it;
                tree_traverse(proc->object_map, obj_it){
                    md_object* obj = &tree_it_val(obj_it);
                    tree_it(u32, array_t) it2 = tree_lookup(hidtree, obj->hid);
                    if(!tree_it_good(it2)){
                        array_t a = array_make(md_object*);
                        it2 = tree_insert(hidtree, obj->hid, a);
                    }
                    array_push(tree_it_val(it2), obj);
                }

                fill_heat_tree(hidtree);


                n_obj          = tree_len(hidtree);
                n_pages        = MAX(1, roundf(((float)n_obj / (float)OBJ_PER_PAGE) + 0.5));
                scroll_bar_len = MAX(1, roundf((float)OBJ_PER_PAGE / (float)n_pages));
                selected_page  = MIN(selected_page, n_pages - 1);
                obj_idx        = 0;
                obj_row        = 1;

                tree_it(u32, array_t) hid_it;
                tree_traverse(hidtree, hid_it) {
                    if (obj_row <= OBJ_PER_PAGE) {
                        page_idx = obj_idx / OBJ_PER_PAGE;
                        if (page_idx == selected_page) {
                            if (page_idx >= n_pages - 1) {
                                scroll_bar_row = obj_row > (((page_idx - 1) * scroll_bar_len) + 1);
                            } else {
                                scroll_bar_row =    obj_row > (page_idx * scroll_bar_len)
                                                 && obj_row <= (page_idx * scroll_bar_len) + scroll_bar_len;
                            }

                            if (page_idx == n_pages - 1) {
                                scroll_bar_row |= obj_row > OBJ_PER_PAGE - scroll_bar_len;
                            }

                            print_hid_row(tree_it_key(hid_it), tree_it_val(hid_it), scroll_bar_row);
                            array_free(tree_it_val(hid_it));
                            obj_row += 1;
                        }

                    }
                    obj_idx += 1;
                }
                rows_left = OBJ_PER_PAGE - obj_row;
                tree_free(hidtree);
                if (rows_left) {
                    for (i = 0; i < rows_left + 1; i += 1) {
                            if (page_idx >= n_pages - 1) {
                                scroll_bar_row = obj_row > (((page_idx - 1) * scroll_bar_len) + 1);
                            } else {
                                scroll_bar_row =    obj_row > (page_idx * scroll_bar_len)
                                                 && obj_row <= (page_idx * scroll_bar_len) + scroll_bar_len;
                            }

                            print_empty_hid_row(scroll_bar_row);
                            obj_row += 1;
                    }
                }

                append_to_output_buff("\n");
          }
        }

        proc->mdtop_did_migrate = 0;

        unlock_process(proc);
        p += 1;
    }

    UNLOCK();
    flush_output_buff();
    array_free(proc_arr);
    elapsed = measure_time_now_ms() - start;
    if (elapsed < config.profile_period_ms) {
/*         usleep(1000ULL * (config.profile_period_ms - elapsed)); */
        usleep(1000ULL * (safe_diff(1000, elapsed)));
    }
}
