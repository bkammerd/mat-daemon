#include "cleanup.h"
#include "config.h"
#include "process.h"
#include "msg.h"
#include "mdtop.h"
#include "profiling.h"
#include "profile_response.h"

#include <signal.h>
#include <unistd.h>

static int cleaned_up;

#if OBJECT_ANALYSIS
Object_Analysis object_analysis;
#endif

static void do_cleanup_sig(int sig) {
    printf("\n");
    INFO("Caught SIGINT...\n");
    cleanup();
    printf("Goodbye.\n");
    exit(1);
}

void init_cleanup(void) {
    signal(SIGINT, do_cleanup_sig);
    atexit(cleanup);
}


#if OBJECT_ANALYSIS
static void print_rss_err(FILE *f, RSS_Error *err, u64 count) {
    fprintf(f, "  Min:  %f\n", err->min);
    fprintf(f, "  Max:  %f\n", err->max);
    fprintf(f, "  Avg:  %f\n", (double)err->sum  / (double)count);
    fprintf(f, "  Sign: %f\n", (double)err->sign / (double)count);
}
#endif

void cleanup(void) {
    if (cleaned_up) { return; }
    cleaned_up = 1;

    INFO("Cleaning up.\n");

    mdtop_cleanup();
    close_message_queue();


    fprintf(mig_stats, "MIG COUNT: %lu\n",    total_mig_count);
    fprintf(mig_stats, "MIG PAGES: %lu\n",    total_mig_pages);
    fprintf(mig_stats, "MIG TIME:  %lu ns\n", total_mig_time);

#if OBJECT_ANALYSIS
    object_analysis.t_end = measure_time_now_ms();

    u64 secs = (object_analysis.t_end - object_analysis.t_start) / 1000;

    FILE *f = fopen("object_analysis.txt", "w");
    if (f) {
        fprintf(f, "Objects per second:        %f\n", (double)object_analysis.sum_objects / (double)secs);
        fprintf(f, "MB per second:             %f\n", (double)(object_analysis.sum_bytes / 1000000) / (double)secs);
        fprintf(f, "Average lifetime(s):       %f\n", ((double)object_analysis.sum_lifetime_ms / (double)object_analysis.sum_objects) / 1000);
        fprintf(f, "Total file-backed objects: %lu\n", object_analysis.sum_file_objects);
        fprintf(f, "LLC tracked:               %f\n", (double)object_analysis.sum_tracked_llc / (double)(object_analysis.sum_tracked_llc + object_analysis.sum_untracked_llc));
        fprintf(f, "Pages tracked:             %f\n", (double)object_analysis.sum_tracked_pages / (double)(object_analysis.sum_tracked_pages + object_analysis.sum_untracked_pages));
        fprintf(f, "%%RSS is file-backed:       %f\n", object_analysis.sum_file_ratio / (double)object_analysis.count);
        fprintf(f, "RSS Error (faults):\n");
        print_rss_err(f, &object_analysis.faults_err, object_analysis.count);
        fprintf(f, "RSS Error (mapped):\n");
        print_rss_err(f, &object_analysis.mapped_err, object_analysis.count);
        fprintf(f, "RSS Error (mapped+file_faults):\n");
        print_rss_err(f, &object_analysis.mapped_file_err, object_analysis.count);

        fclose(f);
    }
#endif
}
