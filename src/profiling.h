#ifndef __PROFILING_H__
#define __PROFILING_H__

#include "process.h"
#include "perf_profiling.h"
#include "bpf/bpf_profiling.h"

extern u64 profile_interval_counter;
extern u64 alloc_trigger_interval;
extern u64 profile_interval_start_time_us;

typedef struct process_profile_info_t {
    perf_process_profile_info perf;
    u64                       profiled_intervals;
} process_profile_info;

int   init_profiling(void);
int   start_profiling_thread(void);
int   start_untracked_faults_thread(void);
void  setup_profiling_for_process(int pid);
void  stop_profiling_for_process(int pid, md_proc *proc, int needs_lock);
float get_proc_event_ratio(md_proc *proc);

#endif
