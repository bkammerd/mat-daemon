#ifndef __PERF_PROFILING_H__
#define __PERF_PROFILING_H__

#include "array.h"

#include <asm/unistd.h>
#include <linux/perf_event.h>
#include <perfmon/pfmlib_perf_event.h>
#include <asm/perf_regs.h>

#define RATIO_WINDOW_SIZE (10)


typedef struct __attribute__ ((__packed__)) {
    u32 pid;
    u32 tid;
    u64 time;
    u64 addr;
} perf_sample;

typedef struct {
    struct perf_event_attr       pe;
    int                          fd;
    pfm_perf_encode_arg_t        pfm;
    struct perf_event_mmap_page *metadata;
} perf_event_info;

typedef struct {
    perf_event_info e1;
    perf_event_info e2;
    long long       c1;
    long long       c2;

    float           ratio_window[RATIO_WINDOW_SIZE];
    float           cur_ratio;
    float           prev_ratio;
    int             in_steady_state;
    int             spiking;
} perf_process_profile_info;


int init_perf_profiling(void);
void setup_perf_profiling_for_process(int pid, perf_process_profile_info *info);
void stop_perf_profiling_for_process(int pid, perf_process_profile_info *info);
void perf_profile_interval(void);

#endif
