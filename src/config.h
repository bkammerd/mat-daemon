#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "simple_config.h"
#include "array.h"

#include <stdio.h>
#include <errno.h>

#define LIST_MODES \
    X(object)      \
    X(region)

#define X(mode) MODE_##mode,
enum md_mode { LIST_MODES N_MODES };
#undef X

enum {
    OPT_BOOL,
    OPT_INT,
    OPT_FLOAT,
    OPT_STRING,
};

typedef struct {
    struct scfg *_scfg;

    enum md_mode      mode;
    const char       *mode_string;
    int               region_shift;
    int               split_large_objects;

    /* system */
    int               tier_1_node;
    int               tier_2_node;
    float             tier_1_capacity;
    float             tier_2_capacity;

    /* object filtering */
    int               migration_min_age_ms;

    /* profiling */
    int               large_object_threshold;
    int               profile_period_ms;
    int               profile_overflow_thresh;
    int               profile_print_interval_period;
    const char       *profile_ddr_event_string;
    const char       *profile_pmm_event_string;
    int               profile_perf_max_sample_pages;
    const char       *profile_output_file_string;
    float             profile_history_weight;
    int               bpf_event_poll_period_ms;
    char             *perf_event_string_1;
    char             *perf_event_string_2;

    /* policy */
    const char       *load_trigger_policy;
    const char       *load_ranking_policy;
    const char       *load_packing_policy;
    const char       *load_combined_policy;

    /* misc */
    int               auto_purge_inactive_procs;
    int               auto_purge_inactive_time_ms;
    int               mdtop;
    int               weird_perf_issue_detect;
    int               weird_perf_issue_interval_limit;
    int               stop_app_during_migrations;
    int               print_migrate_stats;
    int               increase_num_fd_rlimit;
    int               advise_initial_placement;
    float             advice_cold_threshold;

    /* set according to other config options. */
    FILE             *profile_output_file;
} md_config;

extern md_config config;

int  init_config(int argc, const char **argv);
void print_config(void);
void show_config(void);
void add_policy_option(const char *name, int type, const char *desc, void *ptr);

static inline void require(const char *name)                                      { scfg_require(config._scfg, name);             }
static inline void default_bool(const char *name, int val)                        { scfg_default_bool(config._scfg, name, val);   }
static inline void default_int(const char *name, int val)                         { scfg_default_int(config._scfg, name, val);    }
static inline void default_float(const char *name, float val)                     { scfg_default_float(config._scfg, name, val);  }
static inline void default_string(const char *name, const char *val)              { scfg_default_string(config._scfg, name, val); }
static inline void validate_bool(const char *name, scfg_validate_bool_fn_t v)     { scfg_validate_bool(config._scfg, name, v);    }
static inline void validate_int(const char *name, scfg_validate_int_fn_t v)       { scfg_validate_int(config._scfg, name, v);     }
static inline void validate_float(const char *name, scfg_validate_float_fn_t v)   { scfg_validate_float(config._scfg, name, v);   }
static inline void validate_string(const char *name, scfg_validate_string_fn_t v) { scfg_validate_string(config._scfg, name, v);  }


static inline int is_float_between_zero_and_one(struct scfg *cfg, float f) {
    if (f < 0.0 || f > 1.0) {
        scfg_validate_set_err(cfg, "value must be in the range [0.0, 1.0]");
        return 0;
    }

    return 1;
}

static inline int is_float_percentage(struct scfg *cfg, float f) {
    if (f < 0.0 || f > 100.0) {
        scfg_validate_set_err(cfg, "value must be in the range [0.0, 100.0]");
        return 0;
    }

    return 1;
}


extern FILE *mig_stats;

#endif
