#ifndef __COMMON_H__
#define __COMMON_H__


#define OBJECT_ANALYSIS      (0)
#define OBJECT_ORIGIN        (1)
#define OBJECT_ORIGIN_DETAIL (OBJECT_ORIGIN && 0)

#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>
#include <sys/mman.h>
#include <math.h>
#include <dlfcn.h>

extern const char *argv0;


#define MAX_PROFILE_EVENTS 2

#define _XSTR(x) #x
#define XSTR(x) _XSTR(x)

#define PLINE(...) \
    (printf(XSTR(__FILE__) ":" XSTR(__LINE__) ":\n" __VA_ARGS__), fflush(stdout))

#define TIMED_BEGIN { u64 __start = measure_time_now_ms();
#define TIMED_END(name) PLINE(XSTR(name) ": %lums\n", measure_time_now_ms() - __start); }

#define likely(x)   (__builtin_expect(!!(x), 1))
#define unlikely(x) (__builtin_expect(!!(x), 0))

#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#define MAX(a, b) ((a) >= (b) ? (a) : (b))

#define ALIGN(x, align)         ((__typeof(x))((((u64)(x)) + (((u64)align) - 1ULL)) & ~(((u64)align) - 1ULL)))
#define IS_ALIGNED(x, align)    (!(((u64)(x)) & (((u64)align) - 1ULL)))
#define IS_ALIGNED_PP(x, align) (!((x) & ((align) - 1ULL)))
#define IS_POWER_OF_TWO(x)      ((x) != 0 && IS_ALIGNED((x), (x)))
#define IS_POWER_OF_TWO_PP(x)   ((x) != 0 && IS_ALIGNED_PP((x), (x)))

#define UINT(w) uint##w##_t
#define SINT(w) int##w##_t

#define u8  UINT(8 )
#define u16 UINT(16)
#define u32 UINT(32)
#define u64 UINT(64)

#define i8  SINT(8 )
#define i16 SINT(16)
#define i32 SINT(32)
#define i64 SINT(64)

#define KB(x)    (x*(1ull<<10))
#define MB(x)    (x*(1ull<<20))
#define GB(x)    (x*(1ull<<30))
#define TB(x)    (x*(1ull<<40))
#define TO_KB(x) (x/(1ull<<10))
#define TO_MB(x) (x/(1ull<<20))
#define TO_GB(x) (x/(1ull<<30))
#define TO_TB(x) (x/(1ull<<40))

#define PAGE_SHIFT (12ULL)
#define PAGE_SIZE  (1ULL << PAGE_SHIFT)

typedef void *addr_t;

#define UTIL_FN_PRELUDE static __inline__

UTIL_FN_PRELUDE
u64 next_power_of_2(u64 x) {
    if (x == 0) {
        return 2;
    }

    x--;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    x |= x >> 32;
    x++;

    return x;
}

UTIL_FN_PRELUDE
u64 measure_time_now_ns(void) {
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);

    return (1000000000ULL * ts.tv_sec) + ts.tv_nsec;
}

UTIL_FN_PRELUDE
u64 measure_time_now_us(void) {
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);

    return (1000000ULL * ts.tv_sec) + (ts.tv_nsec / 1000ULL);
}

UTIL_FN_PRELUDE
u64 measure_time_now_ms(void) {
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);

    return (1000ULL * ts.tv_sec) + (ts.tv_nsec / 1000000ULL);
}

UTIL_FN_PRELUDE
u64 sum_u64_array(u64 *x, int len) {
    int i;
    u64 sum;

    sum = 0;
    for ( i = 0; i < len; i++) {
        sum += x[i];
    }
    return sum;
}

UTIL_FN_PRELUDE
float sum_float_array(float *x, int len) {
    int i;
    float sum;

    sum = 0.0;
    for ( i = 0; i < len; i++) {
        sum += x[i];
    }
    return sum;
}

void cleanup(void);
void mdtop_cleanup(void);

#define ERR(_fmt, ...)                                    \
do {                                                      \
    int _save_errno = errno;                              \
    mdtop_cleanup();                                      \
    fprintf(stderr, "%sERROR\e[00m: " _fmt,               \
            isatty(2) ? "\e[0;31m" : "", ##__VA_ARGS__);  \
    if (_save_errno) { exit(_save_errno); }               \
    cleanup();                                            \
    exit(1);                                              \
} while (0)

#define ERR_NOEXIT(_fmt, ...)                             \
do {                                                      \
    fprintf(stderr, "%sERROR\e[00m: " _fmt,               \
            isatty(2) ? "\e[0;31m" : "", ##__VA_ARGS__);  \
} while (0)

#define WARN(_fmt, ...)                                   \
do {                                                      \
    fprintf(stderr, "%sWARN\e[00m:  " _fmt,               \
            isatty(2) ? "\e[0;33m" : "", ##__VA_ARGS__);  \
} while (0)

#define INFO(_fmt, ...)                                   \
do {                                                      \
    fprintf(stderr, "%sINFO\e[00m:  " _fmt,               \
            isatty(2) ? "\e[0;36m" : "", ##__VA_ARGS__);  \
} while (0)


UTIL_FN_PRELUDE
u64 safe_diff(u64 x, u64 y) {
    return ((x > y) ? (x - y) : ((u64)0));
}

UTIL_FN_PRELUDE
float pages_to_MB(u64 pages) {
    return ( (float)(pages << PAGE_SHIFT) / (1024.0*1024.0) );
}

UTIL_FN_PRELUDE
int bump_nofile_rlimit(void) {
    int status;
	struct rlimit rlim_new = {
		.rlim_cur	= 16384,
		.rlim_max	= 16384,
	};

    status = setrlimit(RLIMIT_NOFILE, &rlim_new);

    if (status != 0) {
        WARN("failed to increase file limit (errno = %d)\n", errno);
    }

    return status;
}

UTIL_FN_PRELUDE
char * pretty_bytes(uint64_t n_bytes) {
    uint64_t    s;
    double      count;
    char       *r;
    const char *suffixes[]
        = { " B", " KB", " MB", " GB", " TB", " PB", " EB" };

    s     = 0;
    count = (double)n_bytes;

    while (count >= 1024 && s < 7) {
        s     += 1;
        count /= 1024;
    }

    r = calloc(64, 1);

    if (count - floor(count) == 0.0) {
        sprintf(r, "%d", (int)count);
    } else {
        sprintf(r, "%.2f", count);
    }

    strcat(r, suffixes[s]);

    return r;
}

#if OBJECT_ANALYSIS
typedef struct {
    double sum;
    double min;
    double max;
    i64    sign;
} RSS_Error;

typedef struct {
    u64       t_start;
    u64       t_end;
    u64       sum_objects;
    u64       sum_file_objects;
    u64       sum_bytes;
    u64       sum_lifetime_ms;
    u64       sum_tracked_llc;
    u64       sum_untracked_llc;
    u64       sum_tracked_pages;
    u64       sum_untracked_pages;
    double    sum_file_ratio;
    RSS_Error faults_err;
    RSS_Error mapped_err;
    RSS_Error mapped_file_err;
    u64       count;
} Object_Analysis;

extern Object_Analysis object_analysis;
#endif

#endif
