#ifndef __PROCESS_H__
#define __PROCESS_H__

#include "hash_table.h"
#include "object.h"
#include "array.h"
#include "profiling_primitives.h"
#define MDADV_SENDER
#include "mdadv.h"

#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>
#include <linux/limits.h>
#include <pthread.h>

typedef char *md_string;
use_tree(u32, md_string);
typedef tree(u32, md_string)   md_string_tree;

typedef struct md_empty_t {}   md_empty;
use_tree(u64, md_empty);
typedef tree(u64, md_empty)    md_page_map;
typedef tree_it(u64, md_empty) md_page_map_it;

typedef struct {
    u64 size;
    int node;
} md_retired_object;

use_tree(u64, md_retired_object);
typedef tree(u64, md_retired_object)    md_retired_map;
typedef tree_it(u64, md_retired_object) md_retired_map_it;

#define make_page_map() tree_make(u64, md_empty)
#define free_page_map(_t) tree_free((_t))
#define make_retired_map() tree_make(u64, md_retired_object)
#define free_retired_map(_t) tree_free((_t))

struct process_profile_info_t;

typedef struct {
    int                            alive;
    pid_t                          pid;
    md_object_map                  object_map;
    md_retired_map                 retired_objects;
    pthread_mutex_t                mtx;
    char                           cmd_buff[1024];
    u64                            untracked_llc_misses_running[NR_NUMA_NODES];
    u64                            untracked_llc_misses[NR_NUMA_NODES];
    u64                            untracked_present_pages[NR_NUMA_NODES];

    int                            active;
    u32                            inactive_ms;
    int                            has_gotten_events[NR_NUMA_NODES];
    int                            weird_perf_issue_counter[NR_NUMA_NODES];

    int                            mdtop_did_migrate;
    float                          mdtop_max_scaled_acc;
    u64                            mdtop_timestamp_ns;
    u64                            mdtop_max_age_ns;
    u64                            mdtop_max_stale_tm_ns;

    md_string_tree                 hid_string;

    struct process_profile_info_t *profile_info;

    u64                            obj_allocs_since_last_migration;
    u32                            obj_allocs_this_interval;
    u32                            obj_frees_this_interval;
    u64                            ignored_bytes_total;

    mdadv_sender                   advice_queue;
} md_proc;


use_hash_table(pid_t, md_proc);

extern hash_table(pid_t, md_proc) process_table;
extern pthread_rwlock_t           process_table_rwlock;

#define RLOCK()  (pthread_rwlock_rdlock(&process_table_rwlock))
#define WLOCK()  (pthread_rwlock_wrlock(&process_table_rwlock))
#define UNLOCK() (pthread_rwlock_unlock(&process_table_rwlock))

int      init_process_table(void);

void     add_process(pid_t pid);
void     del_process(pid_t pid, int needs_lock);
void     add_object_to_process(pid_t pid, md_object *obj);
void     del_object_from_process(pid_t pid, u64 addr);
void     update_object_hid(pid_t pid, u64 addr, u32 hid);
void     insert_string_for_hid(pid_t pid, u32 hid, md_string s);
void     create_regions_in_process_for_range(pid_t pid, u64 start, u64 end, u64 ts_ns);

void     add_object_to_process_no_lock(pid_t pid, md_object *obj);
void     del_object_from_process_no_lock(pid_t pid, u64 addr);
void     update_object_hid_no_lock(pid_t pid, u64 addr, u32 hid);
void     insert_string_for_hid_no_lock(pid_t pid, u32 hid, md_string s);
void     create_regions_in_process_for_range_no_lock(pid_t pid, u64 start, u64 end, u64 ts_ns);

md_proc *acquire_process(pid_t pid);
void     release_process(md_proc *proc);
void     lock_process(md_proc *proc);
void     unlock_process(md_proc *proc);

#define PROCESS_FOR(proc, ...)                                       \
do {                                                                 \
    pid_t _pid;                                                      \
    (void)_pid;                                                      \
    RLOCK();                                                         \
        hash_table_traverse(process_table, _pid, (proc)) __VA_ARGS__ \
    UNLOCK();                                                        \
} while (0)

#endif
